+++
title = "Nuevos servicios a la vista"
author = ["drymer"]
date = 2017-03-28T08:30:00+02:00
tags = ["servicios", "git", "gitea", "prosody", "xmpp"]
draft = false
+++

Por si alguien no se ha dado cuenta, esta web está en un dominio nuevo, daemons.it. No tiene nada de especial el que sea de Italia, simplemente era de los más baratos y no está la cosa para gastar tontamente. El tema es que por fin tengo un dominio de verdad y no corro el peligro de que me lo quiten por que si, como puede pasar con los dominios gratuitos `.cf` y `.ga`.

Lo mejor de todo es que me veo con la suficiente estabilidad para dejar que otras personas usen algunos de los servicios que tengo instalados. Estos son Gitea, que permite gestionar repositorios Git  y Prosody, para XMPP. Aunque su uso y disfrute es gratuito, hay varias cosas a tener en cuenta:

**Respecto a la creación de usuarios**:

-   En Prosody se pueden registrar cuentas usando el registro IN-BAND. Clientes como Gaim o Psi+ lo soportan. Todas las cuentas creadas tendrán en marcadores la sala de Bad Daemons: **daemons@salas.daemons.it**. Para limitar el posible abuso por parte de bots, es necesario que en un plazo de 48 horas desde la creación de la cuenta se entre en esa sala y se salude, para poder tener en cuenta que la cuenta es humana. Si no se hace, podré borrar la cuenta sin previo aviso.

-   En Gitea se pueden registrar cuentas desde [esta página](https://git.daemons.it/user/signup). Pero si en un plazo de 48 horas no se ha creado algún repositorio, podré borrar la cuenta sin previo aviso.

**En general**

-   No hay garantía ninguna de que estos servicios estén funcionando siempre.

-   No hay garantía de que no se pierdan datos.

-   El acceso está abierto en ambos servicios, pero esto puede cambiar sin previo aviso.

-   Gitea no limita el tamaño del contenido del repositorio. Esto puede cambiar y puedo decidir borrar repositorios demasiado grandes sin previo aviso.

-   El servidor Prosody soporta el XEP HTTP Upload. Ahora mismo el límite es de 4GB por archivo, algo totalmente exagerado, pero lo tengo así para cubrir las necesidades de las pasarelas XMPP - Telegram de la FairCoop. Si se abusa de ello, tendré que cambiarlo.

El resumen de lo anterior viene a ser que son mis servidores y me los follo como quiero. Dicho esto, no tengo intención de limitar nada siempre y cuando el uso que se de esto sea razonable. Me parece tontería tener un par de servidores con tan poco uso, prefiero compartirlo. No se a cuantas cuentas podré dar acceso, ya que los dos servidores que tengo son más bien sustitos, pero intentaré alargarlo lo máximo posible. Hasta aquí estaban las condiciones de uso, ahora veamos que es lo que ambos servicios ofrecen (y que límites tendrán los servidores):

**Prosody**

Los XEP que soporta son:

-   Smacks

-   CSI

-   MAM: Se guarda registro durante una semana.

-   Carbons

-   HTTP Upload: Como ya he mencionado, el límite de tamaño de los ficheros es de 4GB. Espero que nadie abuse de ello o tendré que cambiarlo. Los ficheros se guardan durante una semana.

-   Blocking

-   Salas: En salas.daemons.it.

Si se quiere más información sobre lo que hace cada XEP exactamente, recomiendo la lectura de [este articulo](/posts/xmpp-en-moviles-mentiras-y-como-mejorar-tu-servidor/).

Los logs están desactivados por defecto (exceptuando el de errores), pero esto va cambiando según si hay problemas o no y necesito debuguear. Lo recomendable, en cualquier caso, es usar Tor.

**Gitea**

Soporta Organizaciones, Pull Request, Hooks, Webhooks, factor de doble autenticación. Esto de momento, recordemos que Gitea es un proyecto oven. Para la siguiente versión, tienen integración con Github mediante oauth2, integración con OpenID, lo cual permitirá loguearse desde cuentas GNU social y soporte para firmar los commits con GPG. Para ver mejor que ofrece, se puede probar [aquí](https://try.gitea.io/user/login), aunque hay que tener en cuenta que algunas características son propias de rama de desarrollo.

**VPS**

Tengo dos, en uno está el servidor Prosody, unto a esta web y algunas pasarelas y en el otro está el pajarraco, el planet de emacs y Gitea. Ambos servidores tienen 512MB de ram y otros tantos de swap, 1 núcleo de CPU de unos 0.70 Ghz y 20GB de disco duro. Como se puede ver, sobretodo de espacio va justito.

Para cualquier problema o sugerencia con los servicios, [se me puede contactar](https://daemons.it/) como se prefiera.

Esta página se ha actualizado por última vez el día 28-03-2017.
