+++
title = "Como loguear todo lo que lees y por qué"
author = ["drymer"]
date = 2018-09-04T22:55:00+02:00
tags = ["productividad"]
draft = false
+++

Desde hace un tiempo intento que toda la lectura de articulos que hago, pase por un sitio, que es [wallabag](https://elbinario.net/2014/06/20/wallabag-sustituto-libre-de-pocket-o-instapaper/). Para quien no lo conozca, es una alternativa libre de Instapaper o Pocket. El objetivo de este tipo de aplicaciones es guardar el contenido de una web para consumirlo más tarde. Pero además de ello, tiene otras ventajas:

- Etiquetas
- Búsquedas
- Visionado offline
- Visionado sin anuncios
- Una interfaz limpia y clara
- Escoger temática según contexto
- Cacheo: Si el origen se borra, lo sigues teniendo en tu "read it later"

El flujo que suelo seguir, tanto cuando miro el TL del fediverso y leo mis feeds en el transporte público como cuando llego al trabajo y reviso IRC, XMPP, slack, hangouts y correo es el mismo. Me pongo en modo "captura" sin leer ningún articulo, solo me pongo a ver el contexto de todo ello y si decido que parece interesante, lo guardo en wallabag. Cuando he procesado todos los flujos de entrada de datos, es cuando empiezo a leer.

Entonces viene una pequeña criba. Algo que en su momento me parecía interesante por que la cabeza no me daba para pensar o por el motivo que sea, unas horas o días mas tarde me parece una chorrada y lo borro sin más de la aplicación. De este modo, evito perder el tiempo en articulos que en realidad no me interesa.

Me di cuenta hace poco de que hacia todo esto sin haber pensado mucho en ello cuando leí el artículo que sale como referencia al final de todo. Recomiendo leerlo para ver más en profundidad las ventajas de seguir este flujo de lectura, con algún ejemplo.

Una forma de obligarme a tener limpio el wallabag con todo lo que leo es publicarlo. Llevo categorizando todo lo que leo desde hace casi un año, por lo que ya hay mucho log hecho. Espero ir publicando todas las semanas. Para no generar ruido, tendrá una sección a parte, y si por algún motivo alguien quiere subscribir-se al feed, puede hacerlo [aquí](/log/index.xml). También tiene su sección en el menú, por lo que no deberia ser difícil encontrarlo.

Referencias:

- [The Secret Power of ‘Read It Later’ Apps](https://praxis.fortelabs.co/the-secret-power-of-read-it-later-apps-6c75cc37ef42/)
