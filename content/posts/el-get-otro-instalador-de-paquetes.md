+++
title = "el-get: Otro instalador de paquetes"
author = ["drymer"]
date = 2016-10-24T11:50:00+02:00
tags = ["emacs"]
draft = false
+++

Alguien se podría preguntar, para que queremos otro, si ya tenemos `package.el`, que además viene por defecto en emacs? Pues es simple, no todos los paquetes están en melpa, elpa o marmalade. O igual si que están pero queremos usar una rama concreta del repositorio.

Hasta ahora yo maneaba esto instalándolo desde terminal con git y ale, a volar. Pero también tenia algún paquete que habia copiado directamente de [la wiki de emacs](https://www.emacswiki.org), con el tiempo se me podría haber olvidado de donde lo he sacado. Pero ya no, por que lo tengo controlado con el-get, que permite usar muchos instaladores. Yo solo he probado el de git y el de la wiki de emacs, pero por poder se puede tirar de `apt-get`, `brew`, `elpa`, `dpkg`, etc. Así que al lío:

Se instala el-get desde elpa o usando su instalador. `M-x package-install RET el-get RET` para lo primero, o para asegurarse de que siempre esté disponible:

```lisp
;; Añade la ruta de el-get
(add-to-list 'load-path "~/.emacs.d/el-get/el-get/")

;; Comprueba si el-get está instalado, sino lo instala desde un gist
(unless (require 'el-get nil 'noerror)
;; Comprobar si existe el fichero a descargar, si existe no lo descarga
(if (not (file-exists-p "/tmp/el-get-install.el"))
(url-copy-file "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el" "/tmp/el-get-install.el"))
(load-file "/tmp/el-get-install.el")
)

(use-package el-get)
```

Y de paso aprendemos un poquito de elisp. `unless` se traduce en "a menos que". Por lo tanto, la primera linea relevante viene a ser "a menos que cargue el paquete `el-get`, descarga el fichero `el-get-install.el` en /tmp/". Tanto si lo descarga como si no, ejecuta el instalador que clonará el repositorio git en `~/.emacs.d/el-get/el-get/`. Las lineas se pueden poner tal cual en el archivo de configuración, así nos aseguramos de que siempre está.

En el repositorio git hay un montón de recetas para muchísimos paquetes, tanto de los que están en elpa como en distintos repositorios git. Evidentemente luego se puede definir tus propias recetas, que es lo que veremos a continuación. Pondré las referentes a mi configuración, por poner unos ejemplos.

```lisp
(add-to-list 'el-get-sources '(:name xlicense-github
                                 :type github
                                 :pkgname "timberman/xlicense-el"
                                 ))
```

<div class="src-block-caption">
  <span class="src-block-number">Listado de programa 1</span>
  xlicense
</div>

```lisp
(add-to-list 'el-get-sources   '(:name org2nikola
                                   :website "https://github.com/redguardtoo/org2nikola.git"
                                   :description "Export org into HTML used by static blog generator nikola."
                                   :type git
                                   :url "https://github.com/redguardtoo/org2nikola.git"
                                   :load-path ("./")
                                   ))
```

<div class="src-block-caption">
  <span class="src-block-number">Listado de programa 2</span>
  org2nikola
</div>

```lisp
(add-to-list 'el-get-sources '(:name org-mode-maint
                                 :website "http://orgmode.org/"
                                 :description "Org-mode is for keeping notes, maintaining ToDo lists, doing project planning, and authoring with a fast and effective plain-text system."
                                 :type git
                                 :url "git://orgmode.org/org-mode.git"
                                 :branch "maint"
                                 :load-path ("." "lisp/")
                                 ))
```

<div class="src-block-caption">
  <span class="src-block-number">Listado de programa 3</span>
  org-mode rama maint
</div>

```lisp
(add-to-list 'el-get-sources '(:name gnu-social-mode
                               :description "gnu-social client"
                               :type github
                               :pkgname "bashrc/gnu-social-mode"
                               )
         )
```

<div class="src-block-caption">
  <span class="src-block-number">Listado de programa 4</span>
  gnu-social-mode
</div>

Cómo se puede ver, hay bastantes variables. Los tipos de paquetes git y Github son muy similares, la única diferencia es que si se usa Github, no hay que concretar la url pero si el nombre del paquete. También se puede ver, en el caso del paquete de org-mode, que podemos concretar las ordenes a usar para construir el paquete.

De momento hemos añadido las recetas, aún queda instalar los paquetes. Podemos ejecutar en el buffer &lowast;Scratch&lowast; lo siguiente:

```lisp
(el-get 'sync 'xlicense-github)
```

Y se presiona `C-`. Esto se puede hacer con todos los paquetes que se quieran instalar. Eso o `M-x el-get-install RET xlicense-github`. Y ya podemos usarlo. No hace falta añadir la ruta de cada paquete a la variable `load-path`, de eso se encarga `el-get`, por eso es importante cargarlo. Para actualizar, `M-x el-get-update RET xlicense-github`.
