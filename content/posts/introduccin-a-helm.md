+++
title = "Introducción a Helm"
author = ["drymer"]
date = 2016-05-15T18:33:00+02:00
tags = ["emacs", "helm"]
draft = false
+++

`Helm` es un paquete cuya descripción tiene cojones. Yo no soy capaz de traducirlo literalmente. Lo más cercano que puedo decir es que `helm` es un framework de completado y reducción de selección. Ole. Otra descripción que parece medio humana: `Helm` es un frontend para otras funciones que modifica su uso e incluso une varias. Técnicamente, los paquetes de `helm` pueden ser usados por otros frameworks cómo [`ivy`](http://oremacs.com/2015/04/16/ivy-mode/) o [`ido`](https://www.emacswiki.org/emacs/InteractivelyDoThings), siempre que o unos o otros se adapten.

Cómo es complicado explicar mejor qué es, pondré algún gif en este articulo para que sea más fácil de entender.


## Instalación y configuración {#instalación-y-configuración}

Empecemos por el principio. Para instalarlo, `M-x package-install RET helm RET`. La configuración que yo uso es la siguiente:

```emacs-lisp
(require 'helm)
(require 'helm-config)

(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z")  'helm-select-action)

(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x b") 'helm-mini)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "M-x") 'helm-M-x)

(helm-mode 1)

(require 'helm-descbinds)
(helm-descbinds-mode)
```


## Cómo coño se usa {#cómo-coño-se-usa}

Y podréis pensar: "Pos muy bien, instalado y configurado. Ahora lo jodido, cómo se usa?". A lo que yo respondo que del mismo modo que usabais `emacs` antes. Al menos en lo que a combinaciones de teclas (y uso básico) se refiere. Desgranaré partes de la configuración anterior y mostraré cómo se ve en la práctica, que al final es lo importante. Básicamente comentaré los keybindings, que cómo algún alma audaz habrá visto, son combinaciones que ya se usan de antes.

A continuación saldrán un buen número de gifs para mostrar lo comentado anteriormente. El formato vendrá a ser **Combinación de teclas** | **Función ejecutada**. Debajo de todo eso, una explicación de por que la función que sustituye a la que viene por defecto mola más. Espero que sea útil. Una última cosa, os fiaréis en que la primera y la segunda imagen se parecen mucho y difieren bastante de lo que tenéis. Esto es debido a que en ambas imágenes está ya **instalado** helm. La diferencia es que en las primeras se usa un simple wrapper que apenas añade funcionalidad y en las segundas se usa las propias. De ahí a que haya que rebindear las funciones tal cómo sale en la configuración.

{{< figure src="/img/m-x-pordefecto.gif" caption="Figura 1: M-x" >}}

{{< figure src="/img/helm-m-x.gif" caption="Figura 2: M-x | helm-M-x" >}}

**Por que mola más?**: No tienes por que saber cómo se llama exactamente la función que quieres ejecutar. En el ejemplo, se escribe `M-x list package` y se puede ver todo lo que tenga que ver con la instalación de paquetes. Además, si la función tiene alguna combinación de teclas para ejecutarla la muestra al lado. Acepta expresiones regulares.

{{< figure src="/img/yank-pordefecto.gif" caption="Figura 3: M-y | toggle-kill-ring" >}}

{{< figure src="/img/helm-show-kill-ring.gif" caption="Figura 4: M-y | helm-show-kill-ring" >}}

**Por que mola más?**: Por defecto, `M-y` no hace nada más que rotar las marcas del `kill-ring`. En cambio, con esta función de `helm`, no sólo rotará sino que nos mostrará las opciones que hay. Acepta expresiones regulares para buscar.

{{< figure src="/img/helm-mode-switch-buffer.gif" caption="Figura 5: C-x b | helm-mode-switch-to-buffer" >}}

{{< figure src="/img/helm-mini.gif" caption="Figura 6: C-x b | helm-mini" >}}

**Por que mola más?**: No sólo muestra los buffers que hay abiertos, sino que también muestra los buffers que han sido abiertos recientemente (tirando de `recentf`) y crear buffers nuevos.

{{< figure src="/img/find-files.gif" caption="Figura 7: C-x C-f | Find-Files" >}}

**Por que mola más?**: Varios motivos cuya cabida escapan al articulo de hoy, saldrán más adelante. Pero un ejemplo es que integra la función `ffap`, cuyo nombre tan sugestivo significa **Find File At Point**, que viene a ser abrir un archivo al situarse en una ruta escrita en el buffer, cómo se ve en el ejemplo. Sólo sale una imagen que es la de `Find-Files`, ya que es muy similar a la función que trae por defecto emacs, excepto las mencionadas funciones extra.

Y con esto ya se ha visto lo básico. Ahora a usarlo.

[Fuente principal](http://tuhdo.github.io/helm-intro.html)

[La sección](https://daemons.cf/stories/mi-configuracin-de-emacs/#Helm) pertinente en mi configuración.
