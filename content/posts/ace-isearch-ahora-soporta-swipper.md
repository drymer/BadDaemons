+++
title = "ace-isearch ahora soporta swipper"
author = ["drymer"]
date = 2017-05-07T15:01:00+02:00
draft = false
+++

Hace un tiempo [escribí sobre ace-isearch](https://daemons.it/posts/cosillas-de-emacs-buscar-en-el-buffer/), una manera cuanto menos curiosa de moverse por el buffer. Para quien no lo sepa, es una manera simple de usar a la vez `avy`, `isearch` y `helm-swoop`.

Como se pudo ver en el articulo de como [migrar de Helm a Ivy](https://daemons.it/posts/migrar-de-helm-a-ivy/), yo ahora uso Ivy, por lo que abandoné el uso de ace-isearch. Prefiero no mezclar el uso de ambos frameworks, que luego vienen los líos y los bugs raros. Pero hace poco caí en que swipper y helm-swoop eran muy similares y de hecho hasta tenían la misma pinta cuando los ejecutas, así que pensé en añadirle soporte a swipper a ace-isearch. Dicho y hecho, esta semana anterior me aceptaron el Pull Request en Github (buuuu), por lo que ace-isearch permite escoger entre avy y ace y entre helm-swoop y swipper. Para más información, se puede ver la sección correspondiente de mi archivo de configuración de emacs:

```emacs-lisp
(use-package ace-isearch
  :ensure t
  :init
  ;; Dependencias
  ;; (use-package helm-swoop :ensure t)
  (use-package avy
    :ensure t
    :config
    (setq avy-all-windows t))
  ;; (use-package ace-jump-mode :ensure t)
  :config
  (setq avy-background t)
  (setq ace-isearch-function 'avy-goto-char)
  (setq ace-isearch-input-length 5)
  (setq ace-isearch-jump-delay 0.30)
  (setq ace-isearch-use-jump 'printing-char)
  (setq ace-isearch-function-from-isearch 'ace-isearch-swiper-from-isearch)
  (global-ace-isearch-mode +1)
  (global-ace-isearch-mode -1)
  (global-ace-isearch-mode +1)
  :bind (:map isearch-mode-map
	      ("C-'" . avy-isearch))
  :diminish ace-isearch-mode)
```
