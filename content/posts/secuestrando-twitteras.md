+++
title = "Secuestrando twitteras"
author = ["drymer"]
date = 2015-08-24T15:53:00+02:00
tags = ["python"]
draft = false
+++

[Aviso]
Quien sólo quiera usar el servicio y pasar de todo, que clique [aquí](http://daemon4jidu2oig6.onion/twitter2rss/).

**Actualización**

Desde que escribí este articulo, he mejorado mucho [gnusrss](https://daemons.cf/cgit/gnusrss/about/#orgheadline21). Recomiendo ignorar todo lo que haga referencia a ese programa de este articulo y mirar el README que he enlazado arriba.

Se dice que [mahoma no va a la montaña](http://elbinario.net/2015/07/08/gnusocial-si-mahoma-no-va-a-la-montana/), así que no queda más remedio que traer cojones la montaña. Twitter es la red social privativa mejor considerada en cuanto a libertades y privacidad, lo cual no dice mucho ya que en esas comparaciones sólo hay redes privativas. Es cómo decir que la Modelo de Madrid es lo mejor del mundo por que Guantanamo está muy feo en esta temporada.

Así que bueno, quien quiera abandonar esa red social pero no quiera dejar de leer a cierta gente, puede usar [twitter2rss](http://daemon4jidu2oig6.onion/cgit/twitter2rss/about/). Este programa lo que hace es una petición a <http://twitter.com/lamarea_com>, por ejemplo, coge la información de sus tweets y la pasa a formato RSS, un archivo XML. No es muy legible en crudo, por falta de retornos de carro, pero eso sólo se ve si se mira el código fuente cómo tal. Y bueno, eso hace.

La cosa interesante es que se puede usar cómo servicio, quien quiera ofrecerlo, gracias a la interfaz web en php que ha hecho [@frangor](https://frangor.info).

Hay un disclaimer importante, y es que este script usa la web de twitter basándose en ciertos tags HTML. Sólo tienen que cambiarlo para que el invento se oda. Esto no tiene solución ninguna, solamente puedo estar atento y arreglarlo cuando pase. Siempre se agradecerá feedback, claro.

Para instalarlo y usarlo hay que tener un servidor web con php y clonar lo siguiente a la raíz de este.

```sh
torify git clone https://daemons.it/drymer/twitter2rss
# O si no quieres clonar a través de tor
git clone https://daemons.it/drymer/twitter2rss
```

Y ya. Sólo queda retocar el CSS si se quiere. Recomiendo usar el autoindex en _feeds_, para que la gente pueda ver los feeds que hay. La interfaz se ve así.

{{< figure src="/img/2015-07-24-205421_1362x727_scrot.png" >}}

Esto seria si queremos ofrecerlo cómo servicio, si sólo queremos usarlo de manera personal y lo tenemos en algún ordenador que esté enchufado todo el día tendríamos que modificar el archivo llamado <span class="underline">feed\_list.txt</span>. Con un editor cualquiera vale, y tendríamos que poner una lista tal que así.

```text
eldiarioes
el_diagonal
kaosenlarednet
lamarea_com
lahaineinfo
noticiasalb
nodo50
EsperanzAguirre
la_directa
```

Y ya tendremos un feed RSS que cualquier lector puede leer. Ya no dependemos de la red privativa de twitter, biba! Pero podemos ir más allá. Hasta ahora es un secuestro pequeñito, sólo llevamos la información hasta nuestro lector de feeds. Podemos hacerlo más grande. Podemos combinarlo con otra herramienta, llamada [gnusrss](http://daemon4jidu2oig6.onion/posts/gnusrss--publicando-feeds-en-gnu-social/). El proceso de combinar ambas seria sencillo, lo resumiré a continuación:

```sh
# Preparamos el entorno
mkdir /var/www/twitter2rss
cd /var/www/twitter2rss

# Instalamos dependencias
su -c "apt-get install python3-pip"
# Esto en realidad no es una buena práctica, debería instalarse los paquetes con virtualenv. Quien sepa hacerlo, debería hacerlo
su -c "pip3 install pyrss2gen pycurl PySocks"

su www-data -c "git clone https://daemons.it/drymer/twitter2rss"

# Si tenemos el webserver funcionando, ya podemos visitar la página web. Si no vamos a usarla, podemos usarlo tal que así
echo "lamarea_com" >> feed_list.txt
python3 twitter2rss.py
# Ya tendremos el primer RSS en feeds/lamarea_com.py

# Ahora, preparamos gnusrss
mkdir ~/instalados; cd ~/instalados
git clone https://daemons.it/drymer/gnusrss
cd gnusrss

nano example.py # pondremos cómo hacer con la cuenta de elbinario y el RSS de elbinario, sólo habría que tocar las tres primeras variables.
```

Ahora pondremos lo siguiente en este archivo:

```python
#!/bin/env python3
# -*- coding: utf-8 -*-

import gnusrss
import os.path

if "__main__":

  feedurl = 'http://elbinario.net/feed'

  #GNU Social credentials
  username = 'elbinario'
  password = 'contraseña'
  api_url  = 'https://gnusocial.net/api/statuses/update.xml'

  tor = "no"
  txt = '.elbinario.txt'
  txt_tmp = '.elbinario_tmp.txt'

  if os.path.exists(txt):
     gnusrss.rss(feedurl, txt_tmp, tor)
     new = gnusrss.compare(txt, txt_tmp)
     gnusrss.post(new, api_url, username, password, tor)
  else:
      gnusrss.rss(feedurl, txt, tor)
```

Y ejecutamos lo siguiente:

```sh
# Guardamos el archivo y ya podremos ejecutarlo
python3 example.py
# Si vemos contenido, es que ha funcionado. No publica nada por que aún no tiene nada que publicar
cat .elbinario.txt
# Para comprobar que publique en GNU Social, ejecutamos lo siguiente
sed -i '1d' .elbinario.txt
# Ejecutamos de nuevo y si todo va bien, debería publicarse una noticia en GNU Social

```
