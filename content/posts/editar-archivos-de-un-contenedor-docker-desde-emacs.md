+++
title = "Editar archivos de un contenedor docker desde emacs"
author = ["drymer"]
date = 2016-06-19T23:12:00+02:00
tags = ["docker", "emacs"]
draft = false
+++

Fiate tu la cosa más tonta que es, y yo iba tirando de `ssh` y `tramp`, lo que es muy incoḿodo. Pero de casualidad, buscando otra cosa en la [wiki de emacs](http://wikiemacs.org), encontré un [pequeño snippet](https://www.emacswiki.org/emacs/TrampAndDocker) que facilita las cosas.

```emacs-lisp
(push
 (cons
"docker"
'((tramp-login-program "docker")
  (tramp-login-args (("exec" "-it") ("%h") ("/bin/sh")))
  (tramp-remote-shell "/bin/sh")
  (tramp-remote-shell-args ("-i") ("-c"))))
 tramp-methods)

(defadvice tramp-completion-handle-file-name-all-completions
(around dotemacs-completion-docker activate)
"(tramp-completion-handle-file-name-all-completions \"\" \"/docker:\" returns
  a list of active Docker container names, followed by colons."
(if (equal (ad-get-arg 1) "/docker:")
    (let* ((dockernames-raw (shell-command-to-string "docker ps | awk '$NF != \"NAMES\" { print $NF \":\" }'"))
           (dockernames (cl-remove-if-not
                         #'(lambda (dockerline) (string-match ":$" dockerline))
                         (split-string dockernames-raw "\n"))))
      (setq ad-return-value dockernames))
  ad-do-it))
```

Como podéis observar, se añade un tercer modo a `tramp` que no es `ssh` ni `scp`. Lo que hace es conectarse usando la propia shell de docker. Muy cómodo, más que estarse preocupando de si el `ssh` del contenedor está en marcha o no. La segunda función, además, nos mostrará los nombres de los contenedores en funcionamiento, teniendo sólo que escoger el que se quiera editar. Se evalua el anterior snippet y se usa del siguiente modo: `C-x C-f /docker:$nombreDelContenedor:$rutaDelArchivo`. Y a ser felices.
