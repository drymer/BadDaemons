+++
title = "Forzar el uso de atajos de teclado para moverse en el buffer"
author = ["drymer"]
date = 2017-02-07T08:30:00+01:00
tags = ["productividad", "emacs"]
draft = false
+++

Hay unas cuantas combinaciones de teclas que funcionan en casi todos los editores de texto para moverse por el fichero (una excepción es vi, como no). Estos atajos están pensados para no tener que separar las manos de la parte del teclado en la que debería estar, que es el centro. Pero se tiene cierta tendencia a ir a lo fácil y usar las flechas y otras teclas específicas.

Es habitual querer coger la costumbre de usar los atajos del centrar del teclado para moverse y no usar las teclas específicas, por ello `hardcore-mode` resulta tan útil. Este modo te obliga a usar las combinaciones alternativas al desactivar las que estás habituado a usar. Dicho de otro modo, lo que hace es desactivar las teclas RET, BACKSPACE, flechas y cada vez que se presionen nos dirá la alternativa a usar en el minibuffer. "Solo" desactiva el uso de esas teclas, pero como no me parece suficiente "hardcore", he añadido la desactivación de más teclas. Quedaría como se ve en la siguiente tabla:

| Activo   | Inactivo                       |
|----------|--------------------------------|
| C-f      | Flecha derecha                 |
| C-b      | Flecha izquierda               |
| C-p      | Flecha arriba                  |
| C-n      | Flecha abajo                   |
| C-ñ      | Backspace (la tecla de borrar) |
| C-m / C- | RET (la tecla de enter )       |
| M-f      | C-flecha derecha               |
| M-b      | C-flecha izquierda             |
| M-p      | C-flecha arriba                |
| M-n      | C-flecha abajo                 |
| C-h      | DEL (tecla de borrar)          |
| F1       | El prefijo de ayuda            |
| C-v      | Av. Pág                        |
| M-v      | Re. Pág                        |
| C-a      | Inicio                         |
| C-e      | Fin                            |
| M-<      | Inicio del buffer (C-Inicio)   |
| M->      | Final del buffer (C-Fin)       |

Y la configuración sería así:

```emacs-lisp
(use-package hardcore-mode
  :ensure t
  :config
  (global-set-key "\M-p" 'backward-paragraph)
  (global-set-key "\M-n" 'forward-paragraph)
  (define-key key-translation-map [?\C-h] [?\C-?])
  (global-set-key (kbd "<f1>") 'help-command)
  (define-key hardcore-mode-map
    (kbd "<C-up>") (lambda ()
		       (interactive)
		       (message "This key is disabled. Use M-p instead.")))
  (define-key hardcore-mode-map
    (kbd "<C-down>") (lambda ()
		       (interactive)
		       (message "This key is disabled. Use M-n instead.")))
  (define-key hardcore-mode-map
    (kbd "<C-left>") (lambda ()
		       (interactive)
		       (message "This key is disabled. Use M-b instead.")))
  (define-key hardcore-mode-map
    (kbd "<C-right>") (lambda ()
		       (interactive)
		       (message "This key is disabled. Use M-f instead.")))
  (define-key hardcore-mode-map
    (kbd "<prior>") (lambda ()
			(interactive)
			(message "This key is disabled. Use M-v instead.")))
  (define-key hardcore-mode-map
    (kbd "<next>") (lambda ()
			(interactive)
			(message "This key is disabled. Use C-v instead.")))
  (define-key hardcore-mode-map
    (kbd "<home>") (lambda ()
			(interactive)
			(message "This key is disabled. Use C-a instead.")))
  (define-key hardcore-mode-map
    (kbd "<C-home>") (lambda ()
			(interactive)
			(message "This key is disabled. Use M-< instead.")))
  (define-key hardcore-mode-map
    (kbd "<end>") (lambda ()
			(interactive)
			(message "This key is disabled. Use C-e instead.")))
  (define-key hardcore-mode-map
    (kbd "<C-end>") (lambda ()
			(interactive)
			(message "This key is disabled. Use M-> instead.")))
  (global-hardcore-mode))
```

Si solo se quiere ir a lo básico, hay que quitar todos los `define-key` de la sección `:config`. Lo único "malo" que tengo que decir de este modo es que la tecla de borrado pasa a ser C-h, que en la terminal es el estándar, pero en emacs ese prefijo es el de ayuda. Se remapea a F1, pero bueno, puede resultarle molesto a alguien.
