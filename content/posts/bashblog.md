+++
title = "Bashblog - Un blog com bash"
author = ["drymer"]
date = 2015-06-26T19:15:00+02:00
draft = false
tags = ["bash", "elbinario"]
+++
Cuando se trata de hacer blogs o incluso webs sencillas, Wordpress es el CMS más usado. Pero no es la unica alternativa, ni mucho menos. Sobretodo si buscas al sencillo. Wordpress está bien si quieres tener cierta moduralidad y funcionalidades. Pero si buscas algo más sencillo, Bash Blog es la solución.

Podemos instalarlo y empezar a usarlo tal que así:

```bash
git clone https://github.com/cfenollosa/bashblog.git
cd bashblog
bash bb.sh
```

Puede que al ejecutarlo nos pida que definamos la variable $EDITOR. En el prompt debemos poner **EDITOR=$editor", siendo $editor el editor que queramos, tal cómo vim, emacs, nano, etc. También podremos crear los articulos con markdown o con html. Por defecto, busca Markdwon.pl y sinó, fuerza el html. Dado que la única necesidad real del programa que use markdown es que acepte el formato \*markdown in.md > out.html**, podemos usar cualquiera que acepte este formato. Lo único que tenemos que hacer es cambiar la línea 146 en la que se define la variable **markdown\_bin**. Con poner el nombre del binario valdrá. En mi caso, queda así.

```bash
markdown_bin="$(which markdown || which markdown)"
```

Ahora, podemos empezar con la edición. Lo haremos pasándole el argumento **post** al ejecutar el script. Y nos saldrá un texto cómo este:

```text
Title on this line

<p>The rest of the text file is an <b>html</b> blog post. The process will continue as soon
as you exit your editor.</p>

<p>Tags: keep-this-tag-format, tags-are-optional, example</p>
```

Quien sepa un poquito de inglés, no tendrá problema. No tiene mucho misterio. Instertamos título, cuerpo del artículo y por último tags, de tenerlos. Este articulo se veria [así](http://elbinario.net/bashblog/index.html), si usásemos ese programa. Y poco más hay que añadir. La gracia de esto es precisamente el minimalismo y la facilidad que ofrece.
