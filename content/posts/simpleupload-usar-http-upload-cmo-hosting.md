+++
title = "SimpleUpload: Usar HTTP Upload cómo hosting"
author = ["drymer"]
date = 2016-09-29T08:30:00+02:00
tags = ["python", "xmpp"]
draft = false
+++

HTTP Upload es ese XEP de los servidores XMPP que permite subir archivos al servidor y compartir la dirección en una conversación, ya sea en grupo o una conversación privada. Algo muy útil y que recientemente he implantado en [Jabbergram](https://daemons.cf/cgit/jabbergram/about/#orgheadline12), aunque aún está en fase de testeo.

Pero el soporte en **Jabbegram** vino después de hacer **SimpleUpload**, que es el nombre del programa que sube archivos desde la terminal. Esto es muy cómodo por que podemos compartir cualquier tipo de archivo y se verá correctamente en el navegador, desde un archivo de texto plano a una imagen pasando por audio o vídeo. El único límite del programa es el tamaño máximo que se imponga desde el servidor XMPP. Tanto Prosody como  Ejabberd tienen un tamaño máximo de 1 MB por defecto, pero imagino que la mayoría lo cambiaran a al menos dos o tres, por que sino es bastante triste.. Luego veremos como saber que limite tiene vuestro servidor.

Licencia GPL3 como siempre. He intentado simplificar al máximo su uso, por lo que he explicado cómo hace un envoltorio para ejecutarlo más cómodamente y sin que se líen las dependencias. Lo único que hay que hacer es meter id y contraseña en el archivo de configuración. Y, si el servidor web de Prosody que se usa, tiene un certificado inválido, hay que poner `False` en el valor `verify_ssl`, pero en general nadie tendrá problema con esto. Al lío, primero de todo con la instalación:

Hay que instalar `virtualenv` para gestionar el entorno:

```sh
su -c "aptitude install python3-virtualenv"
```

Crear un entorno virtual y instalar los paquetes necesarios:

```sh
mkdir ~/.env/
virtualenv ~/.env/SimpleUpload/
source ~/.env/SimpleUpload/bin/activate
git clone https://daemons.it/drymer/SimpleUpload/
cd SimpleUpload
pip3 install -r requirements.txt
deactivate
```

Ahora crearemos un archivo que permita usar el entorno virtual sin tener que activarlo a mano. Creamos el archivo `/usr/local/bin/upload` con el siguiente contenido:

```sh
#!/bin/bash

source ~/.env/SimpleUpload/bin/activate
python3 /ruta/al/repositorio/SimpleUpload/SimpleUpload.py $@
deactivate
```

Le damos permiso de ejecución con `sudo chmod +x /usr/local/bin/upload` y ya se podrá usar upload con normalidad, después de editar el fichero de configuración.

Hay que editar el archivo `config.ini` en el directorio de git con las credenciales de la cuenta XMPP:

```sh
cd /ruta/al/repositorio/SimpleUpload/
cp example.ini config.ini
nano config.ini
```

Los valores `id` y `password` son nuestro usuario en formato **usuario@servidor.com** y la contraseña sin más. El valor `verify_ssl` solo se debe establecer en **False** si el servidor web de que proporciona el módulo HTTP Upload usa un certificado inválido. El resto de valores no hace falta tocarlos, se establecerán solos en el primer uso.

Ahora sólo queda ejecutar el programa para subir un archivo. Para hacer una prueba y ver el tamaño máximo de ficheros que permite el servidor, recomiendo crear un fichero pequeño y subirlo:

```sh
echo "Funciona!" > hola.txt
upload hola.txt
```

La primera vez que se ejecute irá un poco más lento que las posteriores. Si se quiere ver el tamaño máximo de fichero permitido por el servidor, hay que mirar en la variable `max_size` del fichero `config.ini`. El valor está en bytes, hay que dividirlo por 1024 dos veces para ver el valor en megabytes.
