+++
title = "Ver documentación usando Ivy"
author = ["drymer"]
date = 2016-12-20T08:30:00+01:00
tags = ["ivy", "emacs"]
draft = false
url = "/posts/ver-documentacion-usando-ivy/"
+++

No es raro necesitar consultar documentación mientras se está programando. Como siempre, es mejor buscarla y verla desde emacs. Hay un modo de hacerlo usando los archivos de [Dash](https://kapeli.com/dash), que es un programa para buscar documentación offline.

El modo que veremos a continuación es `counsel-dash`, que de hecho está basado en `helm-dash`, un programa muy molón. La diferencia que hay entre `counsel-dash` y `helm-dash` es la obvia, la primera usa `counsel` y la segunda `helm`. El problema de `counsel-dash` es que aún no está muy refactorizado y sigue necesitando que se instale `helm`, pero no es muy terrible ya que no se usa y además hay intención de quitar la dependencia.

Antes que nada hay que instalar `sqlite` en la distribución que se use. En Debian el paquete se llama `sqlite` o `sqlite3`. Luego, se puede instalar mediante el-get o mediante elpa. Yo he usado el primer método. La configuración es la siguiente:

```emacs-lisp
(add-to-list 'el-get-sources '(:name counsel-dash
				     :description "counsel-dash"
				     :type github
				     :pkgname "nathankot/counsel-dash"))

(if (not (el-get-package-installed-p 'counsel-dash))
  (progn
    (package-install 'helm)
    (package-install 'dash-functional)
    (package-install 'helm-dash)
    (el-get 'sync 'counsel-dash)))

(use-package counsel-dash
  :load-path "el-get/counsel-dash"
  :init
  (setq counsel-dash-docsets-path "~/.emacs.d/docsets/")
  (setq helm-dash-docsets-path counsel-dash-docsets-path)
  (setq counsel-dash-common-docsets '("Bash" "Emacs Lisp" "HTML" "Markdown" "Nginx" "Python 3" "Docker" "Font_Awesome" "LaTeX" "MySQL" "Python 2" "SQLite" "Ansible" "Apache_HTTP_Server" "HTML" "Nginx"))
  (setq counsel-dash-browser-func 'eww))
```

La variable `counsel-dash-docsets-path` define la ruta en la que estarán los docsets (veremos luego como instalarlos), `counsel-dash-common-docsets` son los docsets que se quieren usar y la última linea establece que para ver la documentación se usará `eww`, el navegador de Emacs.

Ahora veremos como instalar los docsets. Para ver una lista, se puede ir a [esta dirección](https://kapeli.com/dash#docsets) o se puede simplemente ejecutar `counsel-dash-install-docset`. Como a mi me gusta que se auto-instalen las cosas que no tengo instaladas, he escrito el siguiente código de elisp para que instale los docsets que me interesan:

```emacs-lisp
(if (not (concat counsel-dash-docsets-path "Bash.docset"))
    (let ((local_docsets counsel-dash-common-docsets)
	  (docs))
      (while local_docsets
	(setq docs (car local_docsets))
	(if (string-match "Python" docs)
	    (setq docs (replace-regexp-in-string " " "_" docs)))
	(if (string-match "Emacs Lisp" docs)
	    (setq docs (replace-regexp-in-string " " "_" docs)))
	(if (not (file-exists-p (concat counsel-dash-docsets-path "/" (car local_docsets) ".docset")))
	    (progn
	      (counsel-dash-install-docset docs)
	      (setq local_docsets (cdr local_docsets)))
	  (setq local_docsets (cdr local_docsets))))))

```

No tengo ni idea si el código es muy "correcto", pero dado que funciona y solo debería usarse una vez cada mucho, valdrá. Lo que hace es un bucle pasando por todas las cadenas de texto de la lista `counsel-dash-common-docsets`, comprueba si existe el docset en la ruta especificada en `counsel-dash-docsets-path` y si no está instalado, lo instala.

A continuación una captura del `print` de Python.

{{< figure src="/img/counsel-dash.png" >}}
