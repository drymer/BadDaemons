+++
title = "Cómo montar un planet"
author = ["drymer"]
date = 2016-07-14T22:26:00+02:00
tags = ["sysadmin"]
draft = false
+++

Primero de todo, que es un planet? La definición aproximada que nos da **Planet Venus**, que es el programa que usaremos, es un increíble rio de noticias. En sinsillo, lo que hace este programa es coger una cantidad indefinida de feeds y crear uno único. También se le podría llamar agregador de noticias. La cosa es que, en este caso, se suele usar con blogs o webs con una temática concreta, pero bueno, eso ya queda a elección de quien lo use. Un ejemplo es [http://planet.emacslife.com](http://planet.emacses.org), [http://planet.emacs-es.org/](http://planet.emacs-es.org/) o [http://planetlibre.es](http://planetlibre.es).

Así que al lío. Primero de todo, hay que clonar el repositorio de Venus. Lo suyo seria hacerlo directamente en lo que queremos que sea la raíz del servidor web, por ejemplo en `/var/www/planet/`:

```sh
su www-data
cd /var/www/
git clone https://github.com/rubys/venus planet
cd planet
```

Una vez hecho, se tiene que crear un archivo de configuración. Para ello disponemos del directorio `examples/`. Para no liarnos, copiamos uno de esos archivos a la raíz con `cp examples/planet-schmanet.ini $nombreDePlanet.ini` y editamos el archivo llamado que hayamos copiado. Se puede observar que hay dos secciones, que son `[Planet]` y `[DEFAULT]`. Todas las secciones son bastante obvias, así que simplemente pegaré el de `emacs-es.ini`.

```ini
[Planet]
name = Planet Emacses
link = http://planet.emacs-es.org
owner_name = Nobody
owner_email = contacto@emacs-es.org
cache_directory = /var/www/emacs-es.org/cache
log_level = DEBUG
feed_timeout = 20
output_theme = emacsen
output_dir = /var/www/emacs-es.org/output/
items_per_page = 60
bill_of_materials:
images/#{face}
activity_threshold = 90
new_feed_items = 5
locale = "es_ES"

[DEFAULT]
[http://daemons.it/tags/emacs/]
name = Bad Daemons

[http://www.maxxcan.com/category/emacs/feed/]
name = Maxxcan's Site
```

Todo queda bastante claro, hay que adaptar rutas, RSS y el tema. Sólo un par de cosas a comentar. En la sección `[DEFAULT]` van los feeds que queremos que salgan en la web y el parámetro `output_theme` es el tema del planet. Hay unos pocos y son muy feos, por eso decidimos copiar el de [http://planet.emacslife.com](http://planet.emacslife.com). A título de curiosidad diré que me pasé dos o tres horas copiando el tema de la web del planet, examinando el css, entendiendo cómo funcionan las plantillas, adaptándolo y todo eso, y resulta que tienen tanto la plantilla cómo los css en su puto repositorio. Me reí tanto que escupí sangre. Pero bueno, al menos modifiqué un par de cosillas que no me gustaban del que tenían y aprendí como funcionan las plantillas, que diría que cuadra bastante con el [Modelo Vista Controlador (MVC)](https://es.wikipedia.org/wiki/Modelo–vista–controlador), aunque yo de eso no se mucho.

Pues nada, ya tenemos un planet en funcionamiento. Para ejecutarlo, hay que ejecutar en el directorio `python2 planet.py emacs-es.ini` y ya esta (lo suyo seria ponerlo en el `crontab`). En el servidor web que se use, siguiendo el ejemplo, se tiene que poner la raíz en `/var/www/emacs-es.org/output/`. Con esto, ya se tiene lo básico. Para más información, se puede mirar en la [documentación de Venus](http://intertwingly.net/code/venus/docs/index.html) y se puede ver el repositorio de nuestro planet.

PD: El repositorio de la web está en [mi git](http://daemons.cf/cgit/planet.emacs-es.org), incluido el css de la web.
