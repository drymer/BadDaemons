+++
title = "Haciendo ver que tienes mierda de Google"
author = ["drymer"]
date = 2017-03-07T08:30:00+01:00
tags = ["android", "movil", "privacidad"]
draft = false
+++

En un articulo anterior, ya vimos como [limpiar de mierda](https://daemons.cf/posts/limpiando-mierda-en-android/) privativa de Google nuestro móvil Android. Esto tiene sus consecuencias, claro. Nos hemos quedado sin Google Play y sin Google Maps. Algo obvio y que no debería dar problemas, hasta que te encuentras aplicaciones que <span class="underline">requieren</span> las librerías que esas dos aplicaciones proveen. En general, ningún tipo de aplicaciones que necesite estas librerías es recomendable, ya que en general son aplicaciones privativas. Hay alguna rara excepción, como Signal hasta hace poco (sigue siendo mejor XMPP), pero suelen ser aplicaciones como la de Whatsapp o la de Bicing (que por cierto, está llena de publicidad de una empresa).

La respuesta obvia es simple, no uses estas aplicaciones y sigue con tu vida. En el caso de Bicing, se puede funcionar perfectamente sin ella, ya que en F-Droid hay una aplicación llamada [OpenBikeSharing](https://f-droid.org/repository/browse/?fdfilter=bike&fdid=be.brunoparmentier.openbikesharing.app), que soporta Bicing entre otros. Pero cuando la gente se encabezona en usar Whatsapp, es complicado sacarles de ahí, por mucho mejor que sea jabber. Una vez más, lo evidente y lo más recomendable es mandar a esa gente a la mierda. Pero hay veces que no queda más remedio que tragar. Ya sea por que te lo exigen en el trabajo o por que tu abuelo de 80 años no puede o no quiere más que usar el "wasa" ese y no quieres dear de comunicarte con el. Este articulo va para esta gente que se ve obligada a tener este tipo de aplicaciones que requieren que tengas Google Play instalado.

Primero de todo, no es recomendable que hagáis esto si no tenéis un mínimo de idea de lo que hacéis. Si hace falta preguntad, pero no hagáis cosas al tuntun, por que os podéis cargar vuestro móvil. Además, explicaré el proceso en mi móvil, un Huawei Honor 4x con Android 5.0, pero debido a la fragmentación provocada tanto por las versiones de Android como por los fabricantes, de un móvil a otro puede variar el proceso. Por lo tanto, no se trata tanto de seguir al pie de la letra sino de entender que se hace en cada momento y como adaptarlo al móvil de cada una.

Lo que vamos a usar, podemos usarlo gracias al proyecto [MicroG](https://microg.org/). Concretamente, veremos como se instala [GmsCore](https://github.com/microg/android_packages_apps_GmsCore/wiki), que es el paquete encargado de fingir que tienes los servicios de Google Play.

Que aporta exactamente?

-   Hace ver que tienes las GAPPS instaladas

-   Permite usar backends libres para la localización (útil para aplicaciones como OsmAnd o OpenBikeSharing)

-   Poco uso de batería

-   Es software libre \o/

-   Funciona en aparatos reales, emulados y virtuales

Quien necesita esto? Por lo que he ido viendo al preguntar, esto lo necesita la gente que usa Android de stock sin las GAPPS o la gente que usa CyanogenMod o LineageOS también sin las GAPPS.

Los pre-requisitos son simples, el más obvio es tener el móvil rooteado. Si no lo tienes rooteado, ve al foro [XDA Developers](https://www.xda-developers.com/) y busca tu cacharro. Después, hay que tener <span class="underline">desinstaladas</span> las GAPPS y hay que tener SignatureSpoofing instalado. Ahora veremos más sobre esto.

En el anterior articulo deshabilitamos las aplicaciones de Google, pero siguen instaladas. Ahora tendremos que borrarlas, después de hacer un backup, que nunca se sabe.  SignatureSpoofing es un módulo de XPosed Framework. Este framework sirve, básicamente, para hacer que los móviles hagan cosas para las que no están pensados para hacer. Por ejemplo, **SignatureSpojofing**, para quien tenga su inglés oxidado, significa "Falsificación de Firmas". Para el caso que ocupa, haremos que el programa de MicroG finja ser el de Google, para que aplicaciones como la de Bicing, Whatsapp o Signal no sospechen que están siendo engañadas.

Al lío. Hay que tener el móvil enchufado al ordenador y con USB debugging activo. Si no sabes como hacer esto, consulta [el primer articulo](https://daemons.cf/posts/limpiando-mierda-en-android/), en el tercer párrafo. Una vez hecho, instalamos Xposed Framework mediante la aplicación Xposed Installer. Hay dos formas de instalar Xposed Installer.

-   Xposed Installer en f-droid

-   O ejecutando en la terminal:

```bash
wget http://dl.xposed.info/latest.apk -O /tmp/xposed.apk
adb install /tmp/xposed.apk
```

Lo recomendado es el primer método, ya que así se está al día de las actualizaciones, pero a mi no me funcionó.

Una vez instalado, hay que abrir la aplicación y presionar en **Framework**. Sale un aviso de no tocar si no se sabe lo que se hace. Se presiona en **Instalar/actualizar**. Tarda un poco, cuando esté se le da a reiniciar. Si al encender el móvil, al presionar en la aplicación y luego en **Framework**, aparece que hay un problema con **Resources subclass**, lo que hay que hacer es ir a **Ajustes** y **Desactivar ganchos de fuente** y se reinicia otra vez. Para más información de por que pasa esto, se puede presionar el texto en rojo que menciona el error y llevará al hilo de XDA que lo explica.

Una vez hecho lo anterior, volvemos a abrir la aplicación y se presiona en **Descarga**, escribimos FakeGapps e instalamos la última version. Cuando esté, reiniciar ojo otra vez. Al reiniciar a mi se me quedó la pantalla congelada con una cenefa de rallas verdes. Esperé unos 10 segundos y reinicié a botonazo. Ha arrancado correctamente y el modulo está instalado.

Ahora ya tendremos uno de los dos requisitos. Falta desinstalar todo lo que sea de Google. Podríamos hacerlo del tirón, pero antes necesitamos la ruta de los paquetes para poder hacer la copia de seguridad. El siguiente script remonta la partición `/system/` en modo escritura, hace una copia en `~/apks_google/` y luego la desinstala. Si no funcionase del tirón, se puede reintentar quitando el bucle y repetir su contenido por cada paquete que haya, cambiando la variable `$apk` por el paquete que toque.

```bash
#!/bin/bash

apks=("com.google.android.gms" "com.google.android.backuptransport" "com.google.android.feedback" "com.google.android.gsf" "com.google.android.gsf.login" "com.google.android.onetimeinitializer" "com.google.android.partnersetup" "com.android.vending" "com.google.android.setupwizard")

mkdir -p ~/apks_google/
adb shell "su -c 'mount -o remount,rw /system/'"
for apk in ${apks[@]}
do
    ruta=$(adb shell "pm path $apk")
    adb pull $ruta ~/apks_google/
    adb uninstall $apk
done

adb shell "su -c 'mount -o remount,r /system/"

```

Sea como sea, hay que recordar remontar en modo lectura la partición `/system/`, que sino podemos romper muchas cosas. Lo importante es que ya cumplimos los requisitos. Bibah! Y solo llevamos 1019 palabras para ello. Lo gracioso es que lleva más tiempo explicar los requisitos que la instalación y configuración.

Ya solo queda añadir el repositorio de MicroG a f-droid para instalar el servicio falso de Gugel. Se puede usar este [código QR](https://microg.org/download.html).

Una vez añadido el y actualizado el repositorio, solo queda instalar las aplicaciones que nos interesan. Estas son `microG Services Core` y `microG Services Framework Proxy`. Si por lo que sea, alguna no aparece en F-Droid, se puede descargar directamente de su [página oficial](https://microg.org/download.html). Una vez instalados, deberíamos poder lanzar una aplicación nueva, llamada **Ajustes de microG**. Al abrirla, hay que presionar **Self-Check**, para comprobar que todos los requisitos se están cumpliendo. Ahí saldrá una lista de estos requisitos. Si todo está activo, que debería, ya hemos terminado.

Sino, deberíais repasar el articulo de nuevo. Una vez hecho, podéis preguntar por aquí. Ahora ya seremos capaces de usar aplicaciones como Signal, Whatsapp o Bicing, aunque no es para nada recomendable hacerlo. Recordad, solo hay que hacer esto si de verdad lo consideráis necesario.

En otro articulo hablaremos de como usar el GPS usando otras fuentes que no sean las de Google.
