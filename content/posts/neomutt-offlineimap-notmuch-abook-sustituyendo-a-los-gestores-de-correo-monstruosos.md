+++
title = "Neomutt + offlineimap + notmuch + abook: Sustituyendo a los gestores de correo monstruosos"
author = ["drymer"]
date = 2016-09-09T11:32:00+02:00
tags = ["i3wm", "mutt"]
draft = false
+++

Creo que ya lo comenté en algún otro articulo, pero se me odió la torre hace unas pocas semanas y desde entonces estoy funcionando con un **Pentium M con 512 MB de RAM**. Evidentemente he tenido que cambiar un poco mi flujo de trabajo, no puedo usar algunos programas que antes usaba. Por suerte hace ya más de un año estoy usando [i3-wm](https://daemons.cf/categories/i3wm), por lo que ya había muy poquita cosa que usase con interfaz gráfica o fuese demasiado pesado para este ordenador. Sólo hay dos programas en concreto de este tipo, de los que seguramente cuesta más desprenderse. Una es el navegador y otra el gestor de correo. Hoy hablaré del gestor de correo, el navegador lo dejaré para otro articulo.

No soy muy de enviar correos, pero si que suelo recibir ya me gusta estar en varias listas de correos. Para ello tanto thunderbird cómo la mayoría de gestores son una brutalidad en términos de consumo de recursos, aunque desde luego thunderbird es de los más pesados (y de los más usados). Por ello, me decidí a usar el cliente de correo `mutt`, que llevaba mucho tiempo queriendo usar pero sin haberme lanzado del todo.

Como he dicho, en un principio quería usar Mutt, pero me di cuenta de que aunque es perfectamente funcional, le faltan muchas opciones. Buscando vi que con el tiempo han ido saliendo parches que lo mejoran. Hace relativamente poco, varias personas decidieron unir estos parches y continuar el desarrollo activo de Mutt, surgiendo así Neomutt.

Antes de seguir, vayamos por partes. En el título hay cuatro programas, **Neomutt**, del que ya he hablado, **Offlineimap**, que descarga al PC los correos de un buzón MAP, **Abook** que gestiona la agenda y **Notmuch**, que indexa los estos correos. Neomutt no tiene la opción de descargar los correos, por eso hace falta Offlineimap. Tampoco tiene la opción de crear una agenda en función de los correos recibidos, cómo suelen tener todos los gestores, para ello usaremos Abook. El primero que configuraremos será Offlineimap.

Antes que nada, deberíais clonar el repositorio de mis archivos de configuración:

```sh
git clone https://daemons.it/drymer/dotfiles
cd dotfiles
bash instala.sh mutt
```

Esto copiara dos archivos y un directorio: `~/.mutt`, `~/.muttrc` y `~/.offlineimaprc`. Estos son los lugares en los que los programas buscan los ficheros de configuración.

En el próximo articulo explicaré cómo lo tengo montado para tener varias cuentas de forma cómoda en un contenedor cifrado con encfs. A parte de querer tener los correos en un sitio seguro, es interesante por que no nos vemos obligadas a tener los ficheros con las contraseñas en texto plano sin más, que siempre está feo.

Al lío.

## Offlineimap {#offlineimap}

Se puede instalar desde los repositorios de la distribución que se use o mediante `pip`, como más guste:

```sh
aptitude install offlineimap
# o
sbopkg -i offlineimap
# o
pip install offlineimap
```

Una vez instalado, editaremos el fichero en `~/.offlineimaprc` que tiene un contenido similar al siguiente:

```ini
[general]
ui = ttyui
accounts = autistici

[Account autistici]
localrepository = autistici-local
remoterepository = autistici-remote

[Repository autistici-local]
type = Maildir
localfolders = ~/mail/

[Repository autistici-remote]
type = IMAP
remotehost = mail.autistici.org
remoteuser = drymer@autistici.org
remotepass = password_molona
realdelete = yes
maxconnections = 3
ssl = yes
sslcacertfile = /etc/ssl/certs/ca-certificates.crt

```

Se pueden añadir tantas cuentas como se quieran en la variable `accounts` de la sección `General`. Por cada una de ellas, habrá que añadir las tres secciones posteriores (`Account`, `Repository (local)` y `Repository (remoto))`. Las variables que hay en ellas son bastante obvias, así que no entraré en más detalle. La variable `sslcacertfile` parece obligatoria desde la versión 7.0.5.

Ahora ya lo tenemos listo para sincronizar los correos. Ejecutando simplemente `offlineimap` se sincronizarán.

## Notmuch {#notmuch}

Se instala el paquete `notmuch`:

```sh
aptitude install notmuch
# o
sbopkg -i notmuch
```

Una vez instalado, se puede ejecutar `notmuch setup`, que creará el fichero `~/.notmuch-config` o se puede crear directamente:

```ini
[database]
path=/home/$user/mail

[user]
name=drymer
primary_email=cuenta@punto.com
other_email=alguien@cuenta.org

[new]
tags=new;unread;inbox
ignore=

[search]
exclude_tags=deleted;spam;

[maildir]
synchronize_flags=true

[crypto]
gpg_path=gpg
```

Sólo comentar que la variable `path` de la sección `database` debe apuntar al directorio en el que están los correos, que es dónde se creará la base de datos con el contenido indexado de los correos.

Una vez hecho, se ejecuta `notmuch new`, que según la cantidad de correos que se tengan puede tardar un poco. Luego, cada vez que se ejecute `offlineimap` deberá ejecutarse `notmuch new`, para que reindexe todos los correos nuevos. Podéis probar a buscar algún correo usando `notmuch search $parametroDeBusca`.

Notmuch gestiona las "flags" típicas del correo, **nuevo**, **enviado**, **borrado**, etc. Antes, si queríamos tener, una vista rápida de todos los correos de org-mode, por ejemplo, había que crear alguna regla para que moviese todos los correos de la lista de org-mode a un directorio en concreto. Pero con notmuch esto ya no es necesario. Se funciona con **etiquetas** en vez de con directorios, algo que parece más eficiente. Para ello yo uso el siguiente script:

```sh
#!/bin/bash

i=0

lista=("emacs-orgmode@gnu.org" "list_criptica@inventati.org" "@lists.torproject.org")
tags=("orgmode" "criptica" "tor")

for mail in ${lista[@]}
do
notmuch tag -inbox +${tags[$i]} --output=messages $mail tag:inbox
i=$((i+1))
done
```

El script se encuentra en `~/.mutt/scripts/filtros.sh`. Para usarlo hay que añadir un correo en la variable `lista` y el tag que se quiera que tengan esos correos en la variable `tags`. Hay que hacerlo siempre en orden. Luego veremos cómo automatizar su uso.

## Abook {#abook}

<a id="org60e1dcf"></a>

:ID: 83f3c957-75cf-418f-9aee-ccc480176422

Se instala el paquete `abook`:

```sh
aptitude install abook
# o
sbopkg -i abook
```

Este programa funciona del siguiente modo:

```sh
echo "From: unCorreo@punto.com" | abook --datafile ~/.abook --add-email-quiet
```

El `--datafile` solo hace falta si tenemos la agenda en un sitio distinto del habitual. El `--add-email-quiet` es lo que añade el correo a la agenda. Pero lo importante es que hay que pasarlo en ese formato, con el "From: " delante, lo cual es bastante incomodo. Por ello he hecho un script, que añade automáticamente todas las direcciones de los correos entrantes. Igual es un poco exagerado, pero dado que apenas consume recursos, tampoco importa mucho:

```sh
#!/bin/bash

MESSAGE=$(notmuch show tag:unread 2> /dev/null)
NEWALIAS=$(echo "$MESSAGE" | grep ^"From: ")

if [[ -n $NEWALIAS ]]
then
while read -r line
do
	busqueda="$(abook --datafile ~/.abook --mutt-query $(echo $line | cut -d'<' -f2 | cut -d'>' -f1 | cut -d':' -f2 | sed 's/^ //'))"

	# auto add sender to abook
	if [[ $busqueda = "Not found" ]]
	then
	    echo $line | abook --datafile ~/.abook --add-email-quiet > /dev/null
	fi

done < <(echo "$NEWALIAS")
fi
```

El script está en `~/.mutt/scripts/auto-process.sh` Lo que hace es coger los correos que no se han leído, comprobar si la dirección está añadida y si no lo está, añadirla. Más adelante veremos cómo se automatiza esto con otros scripts para que sea cómodo de usar.

Para usar la agenda, cuando se presiona **m** para enviar un nuevo mensaje en mutt, el primer campo a rellenar es la destinataria del correo, escribiendo las primeras letras y presionando el tabulador se auto completará.

## Neomutt {#neomutt}

Primero de todo la instalación. Hasta dónde se no hay paquetes en las distribuciones, pero se puede compilar. Lo único que vale la pena mencionar es que veréis que uso el flag `--disable-fmemopen`. Esto es debido a que, por algún motivo que aún no han sabido concretar, si mutt soporta **fmemopen** da un segfault cuando se abre mutt torificado. Dado que no supone ninguna mejora apreciable de rendimiento quitar ese soporte, recomiendo quitarlo sin más.

```sh
git clone https://github.com/neomutt/neomutt
cd neomutt ./prepare --prefix=/usr/local --sysconfdir=/etc --enable-debug --enable-notmuch \
 --enable-smtp --enable-hcache --enable-sidebar --with-ssl=/usr --with-curses=/usr \
 --with-regex --with-sasl --enable-imap --enable-pop --with-idn --enable-gpgme \
 --with-gpgme-prefix=/usr/ --disable-fmemopen
make
su -c "make install"
```

Ahora la configuración. En el directorio `~/.mutt/` hay los siguientes archivos:

```sh
ls ~/.mutt
account.example  colors  gpg.rc  keybinds  mailcap  muttrc.example  offlineimaprc.example  signature
```

Todos forman parte de la configuración de Neomutt. Para que sea más fácil leerlos, es recomendable tenerlos en archivos separados, ya que hay muchísimas opciones. Todos tienen nombres lógicos:

- `account.example`: Contiene los datos de la cuenta.
- `colors`: Los colores.
- `mailcap`: Contiene los comandos a usar según el tipo de formato de archivos recibidos. En mi caso sólo tengo para la visualización de correos HTML, pero se puede añadir para visualizar PDF, ODT, etc.
- `gpg.rc`: Contiene la configuración para el gpg.
- `keybinds`: Contiene los atajos de teclado.
- `signature`: Contiene una firma, que en mi caso sólo usa la cuenta de autistici.
- `muttrc.example`: Este ya ha sido copiado a `~/.muttrc`, por lo que lo ignoraremos.
- `offlineimaprc.example`: Este ya ha sido copiado a `~/.offlineimaprc`, por lo que lo ignoraremos.

No voy a explicar todos los parámetros que salen, ya que hay demasiadas, sólo las partes que considere más importantes. Para verlas en mayor profundidad, recomiendo visitar el manual de [neomutt](http://www.neomutt.org/manual/).

Empecemos por la más importante, que es la de `account.example`. Podéis cambiarle el nombre al archivo, pero si lo hacéis tendréis que cambiar el parámetro siguiente del `muttrc`:

```sh
source "~/.mutt/account.example"
```

Las variables más importantes a cambiar de `account.example` son las siguientes:

```text
set my_pass     = ""
set from        = ""
set server      = ""
set port        = ""
set realname    = ""
```

Estas hacen referencia a los datos de autenticación del servidor de correo. `realname` es el nombre que se quiera que aparezca a los demás cuando se envía un correo.

Comentaremos también la sección de **virtual-mailboxes** y la del **gpg**. La primera hace referencia a los mailboxes que tendremos. Habitualmente mutt tenia los típicos, el INBOX, Enviados, Basura, etc. La primera sección hace una búsqueda de los tags que le asignamos usando notmuch. Por lo tanto, hay que crear los virtual-mailboxes en función a los tags que hayamos creado en el script `filtros.sh`. De la sección de **gpg** solo mencionar que hay que cambiar la variable `gpg_sign_as` con el ID de nuestra clave.

El archivo `colors` sigue el esquema de colores que suelo usar, Azul, lila y gris. El archivo de `keybinds` contiene un par de atajos "emacsizados" y otros asignados a teclas más útiles. Las explicaciones de cada uno están al lado del bind. Para ver los binds que se usan actualmente, sólo hay que presionar **?** en mutt.

Y ya queda la última parte. Cómo automatizar la entrada de correos, añadirles tags y agregar los correos a la agenda. Yo hago esto con el siguiente script que meto en el cron:

```sh
#!/bin/bash

username=`whoami`

# commands
abook="/home/$username/.mutt/scripts/auto-process.sh"
autotag="/home/$username/.mutt/scripts/filtros.sh"

imapactive=`ps -ef | grep offlineimap | grep -v grep | wc -l`
online=`curl --silent http://gnu.org | wc -l`

case $imapactive in
'1')
    killall offlineimap && sleep 5
    ;;
esac

case $online in
'9')
    offlineimaprc="/home/$username/.offlineimaprc"
    mailsync="offlineimap -c $offlineimaprc -u quiet -q"
    echo "Ejecutando offlineimap..."
    $mailsync
    echo "Indexando emails..."
    notmuch new > /dev/null
    echo "Autoguardando contactos..."
    $abook
    echo "Autotageando correos..."
    $autotag
    ;;
esac

fi

```

Se puede crear este archivo en `/usr/local/bin/mailsync` y darle permisos con `chmod`. Recomiendo ejecutarlo desde la terminal para ver que funcione correctamente. Si funciona bien, lo añadimos al crontab ejecutando `crontab -e`:

```sh
*/5 * * * * /usr/local/bin/mailsync
```

De este modo, se ejecutará ese script cada cinco minutos. Y con esto, ya hemos terminado.

Que se me ocurran, quedarían las siguientes tareas para mejorarlo:

- Buscar correos usando notmuch

- Meter en el mailcap tipos de archivos más usados y asociarlos con los programas que use

Poco más hay que decir. Como ya he dicho más arriba, en el siguiente articulo explicaré cómo hacer mutt usable con dos o más cuentas y cómo meter todo en un contenedor cifrado y gestionarlo cómodamente. Neomutt se verá así.

{{< figure src="/img/2016-09-11-141804_1024x768_scrot.png" >}}
