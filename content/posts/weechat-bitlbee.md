+++
title = "Weechat + Bitlbee"
author = ["drymer"]
date = 2015-10-29T20:34:00+01:00
tags = ["irc", "xmpp"]
draft = false
+++

Cuando tengo mucho que hacer suelo encontrar chorradas con las que entretenerme de una manera mucho más fácil. Este es el caso de weechat.
Yo uso los clientes de irc y xmpp en terminal, pero hasta ahora habia usado sólo irssi y mcabber. mcabber lo malo que tiene, es que va a una ventana por cuenta, no permite tener un sólo mcabber corriendo con las cuentas **lol@lols.com** y otra **lolazo@lols.com** en esa ventana. Y bueno, aún con tmux/screen, es incómodo. Y de casualidad me encontré con bitlbee.
Bitlbee es un servidor que emula ser un servidor irc, pero en realidad es una pasarela a otras muchas redes. AOL, yahoo, gtalk,... Todas mierda privativa, las únicas interesantes, claro, son XMPP y GNU Social (statusnet). Empezaremos por este y luego ya nos ponemos con weechat en si.


## Instalación {#instalación}

```sh
$ su -c "aptitude install bitlbee weechat" # debian
$ su -c " sbopkg -i bitlbee weechat" # slackware
```


## Configuración básica de weechat {#configuración-básica-de-weechat}

Hecho esto, al turrón. Weechat tiene tres modos distintos de configuración. El plugin **isset.py** (que permite hacerlo de manera interactiva), desde el propio weechat ejecutando comandos con **/set** y editando los ficheros del ~/.weechat. Veremos el segundo modo. Pondré las opciones tal cual y las iré comentando. Lo siguiente es el uso básico para irc y el aspecto, sin tocar aún el tema de los plugins. Esto mostrará un poco el funcionamiento básico para aprender a modificar cosas, al final pondré mi configuración exacta.

```text
# Ayuda
/help
# Ver todas las opciones
/set *
/set *nick*
# Añadir un servidor y decirle que use ssl
/server add freenode holmes.freenode.net/6697
/set irc.server.freenode.ssl on
# Conectar al servidor
/connect freenode
# Entrar en una sala
/join #slackware-es
# Configurar la conexión automática al servidor y que entre en esa sala
/set irc.server.freenode.autoconnect true
/set irc.server.freenode.autojoin "#slackware-es,#debian-es"
# Guardar la configuración. Por defecto, se guarda al salir
/save
# Formato de tiempo sin segundos, que es muy feo
/set weechat.look.buffer_time_format "%H:%M:"
# Los nicks a la derecha
/set weechat.bar.nicklist.position right
# Tamaño de la barra de nicks
/set weechat.bar.nicklist.size 10
# Color de la barra de status gris
/set weechat.bar.status.color_bg 0
# Permitir mensajes de más de una linea
/set weechat.bar.input.size 0
/set weechat.bar.input.size_max 3
# Añadir filtro inteligente: Sólo muestra los join/part/quit de usuarios que hablaron recientemente
/set irc.look.smart_filter on
/filter add irc_smart * irc_smart_filter *
# Colorcitos para los nicks
/set weechat.color.chat_nick_colors red,green,brown,blue,magenta,cyan,white,lightred,lightgreen,yellow,lightblue,lightmagenta,lightcyan
# Colorcitos para los nicks ausentes
/set irc.server_default.away_check 5
/set irc.server_default.away_check_max_nicks 25
# Cuando una persona escriba más de una linea, en vez de mostrar el nick más de una vez, muestra este carácter: ↪
/set weechat.look.prefix_same_nick ↪
```

Scripts que he instalado:

```text
buffers.pl # Muestra una barra con todos los buffers
highmon.pl # Muestra un buffer con los avisos que se configuren
iset.pl # Configurar interactivamente
auto_away.py # Pues eso
autojoin_on_invite.py # Aham
autosort.py # Ordena los buffers de manera automática
bitlbee_completion.py # Autocompletado de comandos para bitlbee
go.py # Va a x buffer
screen_away.py # Cuando te sales del screen/tmux, pone el away
urlgrab.py # Muestra las url del buffer en el que se ejecuta en otro buffer
urlbar.py # Una barra arriba del todo que muestra la última url
```

Se pueden instalar/ver:

```text
/script install algo.py
/script
```

Ahora, veamos los colorcicos de la barra de los buffers y un poco de configuración básica:

```text
# Color de fondo (background = bg)
/set buffers.color.current_bg 0
# Color de las letras (foreground = fg)
/set buffers.color.current_fg ligthblue
# Colorcitos de los avisos
/set buffers.color.hotlist_message_bg default
/set buffers.color.hotlist_message_fg magenta
# Separamos el nombre del servidor del canal y lo indentamos
/set irc.look.server_buffer independent
/set buffers.look.indenting on
/set buffers.look.show_number off
```

Creo que ya se va entendiendo la dinámica de cómo se modifican los parámetros. Suele ser $obetoAlQueHaceReferencia.$atributo.$valor, más o menos. Ahora, vayamos con bitlbee.

Si tenéis bastantes buffers, lo de ir subiendo por ellos de uno en uno se hace muy lento. Por eso tenemos el plugin **go.py**. Al escribir **/go #emacs-es** nos lleva a esa sala. Pero aún más molón, es asignarle un keybind:

```text
/key bind /go meta-g
```


## Configuración de bitlbee {#configuración-de-bitlbee}

Editamos el **/etc/bitlbee/bitlbee.conf** y cambiamos los siguientes valores, siendo el segundo optativo (para torificar todas las conexiones):

```text
DaemonInterface = 127.0.0.1
# Torificamos todas las conexiones
Proxy = socks5://127.0.0.1:9050
# Reiniciamos
# service bitlbee restart # debian
# /etc/rc.d/rc.bitlbee restart # slackware
```

Ahora ya podemos añadir este servidor cómo si fuese uno de irc, desde weechat.

```text
/server add xmpp localhost/6667 -autoconnect
```

Veremos que debajo del buffer **xmpp**, hay un canal llamado **&bitlbee**. Este es un canal de control y manearemos todo desde aquí. A continuación registraremos la contraseña y pondremos que se loguee en bitlbee de manera automática.

```text
register $contraseña
/set irc.server.im.command "/msg &bitlbee identify $contraseña"
```

Añadimos una cuenta de jabber, nos unimos a una sala y le ponemos el autojoin.

```text
account add jabber drymer@autistici.org $contraseñaMaja
# Ejecutamos account list para ver el id de la cuenta
account list
# Al ser la primera, siempre será 0
chat add 0 redeslibres@salas.mijabber.es
# Ejecutamos channel list para ver el id del canal
channel list
# Al ser el primero, siempre será 2
channel 2 set auto_join true
/join #redeslibres
```

Con esto las salas. Veamos ahora el tema de los mensajes privados y el otr. Es bastante sencillo. Ejecutamos lo siguiente siempre en **&bitlbee**.

```text
otr connect becario@gnusocial.red
# Si sabemos que es su fpr...
otr trust becario@gnusocial.net 7714AB49 0C8FF5C2 5EBA0021 C34B75E6 EA819A5C
# Abrimos un mensaje privado
/query becario
```

Y creo que ya está lo básico. Evidentemente hay muchísimas más cosas, por eso mismo pondré aquí un link a mi configuración.

[weechat.conf](/files/weechat.conf)

Y aquí una captura.

{{< figure src="/img/weechat.png" >}}

Fuentes (o copias descaradas). Vale la pena echarles un ojo a todas esas webs, sobretodo la de Pascal Poitras:

-   [http://zanshin.net/2015/01/10/a-guide-for-setting-up-weechat-and-bitlbee/](http://zanshin.net/2015/01/10/a-guide-for-setting-up-weechat-and-bitlbee/)
-   [http://geekyschmidt.com/2011/01/02/bitlbee-and-otr-then-add-some-tor](http://geekyschmidt.com/2011/01/02/bitlbee-and-otr-then-add-some-tor)
-   [http://pascalpoitras.com/2013/08/09/weechat-highlight/](http://pascalpoitras.com/2013/08/09/weechat-highlight/)
-   <https://wiki.bitlbee.org/jabberGroupchats>
