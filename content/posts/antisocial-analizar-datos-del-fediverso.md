+++
title = "Antisocial - Analizar datos del fediverso"
author = ["drymer"]
lastmod = 2021-08-09T00:20:15+02:00
tags = ["antisocial", "fediverso"]
draft = false
+++

**Disclaimer**: Este artículo se ha escrito del tirón con cosas que tengo en mente desde hace mucho. Es posible que no sea del todo riguroso en algunos aspectos y que en alguno exagere. Intento simplificar cosas que son complejas de explicar, por lo que seguro que hay cosas que no son exactas.


## Tabla de contenido {#tabla-de-contenido}

-   [Prólogo](#prólogo)
-   [De quién nos protegemos?](#de-quién-nos-protegemos)
-   [Definiciones de seguridad y anónimato](#definiciones-de-seguridad-y-anónimato)
-   [Matriz para decidir como protegerte](#matriz-para-decidir-como-protegerte)
-   [Antisocial: Qué datos se pueden sacar de las redes sociales libres?](#antisocial-qué-datos-se-pueden-sacar-de-las-redes-sociales-libres)


## Prólogo {#prólogo}

Qué diferencia una red social privativa de una red social libre? Se podría decir que la principal diferencia es que detrás de las redes privativas hay una o varias empresas con ánimo de lucro. Normalmente esto se traduce en que quieren recopilar cuantos más datos de sus usuarias. Pero que datos tienen realmente las empresas de nosotras?

Para simplificar la pregunta, vamos a reducir solamente al uso de la propia red social, sin entrar en [movidas de cookies](https://anytech365.com/es/stop-facebook-spying-on-you-outside-of-facebook/) de terceros ni nada parecido.

Qué datos puede ver facebook o twitter cuando usamos sus páginas?:

-   El contenido de lo que públicamos, ya sea público o privado
-   Interacciones (favoritos, compartir)
-   Dónde ponemos el ratón y durante cuanto tiempo
-   Que páginas miramos (perfiles privados, grupos, ...)
-   Cuanto tiempo pasamos en cada página
-   Anuncios que vemos
-   Información física (IP desde la que te conectas)

Cuando lo comparamos con los datos que se pueden sacar del fediverso, vemos que en realidad de esa lista de seis, solo se cumplen dos. Contenido e interacciones. Desde luego es una mejora, motivo suficiente para no usar redes privativas. Está bien que no cojan tus datos y los usen para alimentar AIs malignas o venderlas a terceros.


## De quién nos protegemos? {#de-quién-nos-protegemos}

Pero que pasa con los datos que se comparten en las redes libres? Que daño podrían causar? Creo que para responder a esta pregunta correctamente, hay que hablar de algunos temas que a veces se mezclan:

-   Enemigos o actores malignos
-   Seguridad de la información
-   Anonimato

Qué actores malignos podrian existir en general?

-   **Un gobierno**: no son pocos los gobiernos que espian de forma masiva a sus ciudadanos con el fin de "protegerlos". Sobre este tema hay [información para aburrir](https://es.wikipedia.org/wiki/Revelaciones%5Fsobre%5Fla%5Fred%5Fde%5Fvigilancia%5Fmundial%5F(2013-2015)).
-   **Una empresa**: las empresas de las redes sociales privativas recogen los datos para sí pero además los venden a otros. [Ejemplo reciente](https://time.com/6083323/bishop-pillar-grindr-data/), un cura denunciado por ser gay y ir a bares gays. La fuente de los datos? Datos disponibles a la venta por parte de Grindr.
-   **Un particular**: normalmente esta persona no tiene interés en la recopilación de datos masiva, sinó en la recopilación de datos personales. Puede estar relacionado con ataques personales y doxxing. Un ejemplo, [Gamergate](http://www.mtv.com/news/2245633/gamer-gate-one-year-later/).


## Definiciones de seguridad y anónimato {#definiciones-de-seguridad-y-anónimato}

**Disclaimer**: Es un tochazo.

Una vez definidos los actores malignos, definamos las otras dos palabras, ya que a veces las mezclamos.


### Seguridad {#seguridad}

La [seguridad de la información](https://es.wikipedia.org/wiki/Seguridad%5Fde%5Fla%5Finformaci%C3%B3n), en el contexto del fediverso, está compuesta de las siguientes claves:

-   **Confidencialidad**: Que la información llegue a quien tenga que llegar. La información pública la tenemos clara, la publicamos y la ve cualquiera. Luego hay otras formas de limitar quien puede ver que. Esta funcionalidad la tienen varias implementaciones, como Mastodon o Pleroma, y está cogida con pinzas. El problema es que en la federación, si mi servidor dice que algo es privado y se lo manda a otro servidor, este otro servidor tiene que respetarlo. Ejemplo, el usuario a del nodo manda un mensaje privado al usuario b del nodo b. El nodo b podría decidir hacer público este mensaje.
-   **Integridad**: Que nadie pueda modificar nuestra información. El problema es similar al anterior, dentro de tu servidor controlas lo que quieras, pero cuando lo lanzas fuera hay que confiar. Ejemplo, el usuario a del nodo a lanza un mensaje al mundo y el nodo b modifica el mensaje para decir que odia las aceitunas.
-   **Disponibilidad**: Que la información se pueda consultar cuando se requiera. Hay diferentes acercamientos en el fediverso, algunas implementaciones copian los datos (con la consecuencia sobrecarga) y aunque el origen falle, pueden mostrarlo. Otras simplemente referencian al origen.
-   **Autenticación**: Que se pueda identificar a la persona que ha escrito la información. Esta parte es especialmente importante y se relaciona con el siguiente palabro, **anonimato**.


### Anonimato {#anonimato}

Qué significa **anonimato**? Incluso dentro del contexto de las redes libres, puede tener distintas connotaciones. Una parte del problema es que usamos la palabra **anónimo** cuando en realidad queremos decir **pseudoanónimo**. Una persona anónima es una persona que es de nombre desconocido o que se oculta, según la RAE. Pero entonces, como podemos garantizar la seguridad de la información? Una de las claves es la integridad y la autenticación. Como podemos saber que el mensaje es de quien dice ser, si no sabemos quien es?

Una primera diferenciación que tenemos que hacer es la de personas e identidades. Yo como persona soy lo que pone en mi DNI, pero tengo más identidades. Una es drymer, alguien que lleva en comunidades de software libre varios años y que tiene interéses y actividades x.

Pero no es la única identidad que tengo, tengo otros nicks y otras actividades. Estas identidades pueden ser o no **pseudoanonimas**. Digo pseudoanonimas por que necesitan un identificador. Por ejemplo, el identificador más viejo y fiable de drymer es mi correo eléctronico.

Más ejemplos de identidades pseudoanonimas. Un foro en la red de tor. Si yo me logue como paquito, yo ahí tendré la identidad de paquito. **Sabran** quien es mi identidad. Pero desde un punto de vista técnico, no sabran quien soy yo como persona, ya que Tor se asegura de ello.

Llevando la definición a las redes libres, seremos **anónimas** si usamos herramientas para tal fin (tor, vpns, 7 proxies, etc...). Pero tendremos una identidad identificable, valga la redundancia, que será la que usaremos para iniciar sesión en uno de los nodos del fediverso. Por ejemplo, mi identidad será drymer@barcelona.social.


## Matriz para decidir como protegerte {#matriz-para-decidir-como-protegerte}

Después de todo ese desvarío, que creo era necesario, vamos a ver como facilitarnos un poco la vida. He aquí una matriz para escoger que cosas podemos hacer según de quien nos queremos proteger:

-   RSP: Red social privativa.
-   RSL: Red social libre.
-   PH: Protección por herramientas, como tor, proxies, vpns,...
-   PC: Protección por contenido, no decir direcciones físicas, nombres, gustos personales, etc.
-   IC: Irse a vivir a una cueva, plantar zanahorias y tomates.

| De quien me protejo?       | RSP + PH | RSP + PC | RSP + PH + PC | RSL + PH | RSL + PC | RSL + PH + PC | Irse a vivir a una cueva |
|----------------------------|----------|----------|---------------|----------|----------|---------------|--------------------------|
| De un gobierno             |          |          | X             |          |          | X             | X                        |
| De las empresas            | X        | X        | X             |          | X        |               | X                        |
| De una persona / colectivo |          | X        |               |          | X        |               | X                        |
| Del mundo                  |          |          |               |          |          |               | X                        |

Hay bastante a desgranar:

-   **Gobierno**: Tenemos que usar herramientas de ocultación tanto física como mediante el contenido. Esta última parte suele ser en la que se falla. Si coges y pones en tu red social favorita del fediverso que te vas a tomar un helado a la plaza del Sol, ya puedes usar tor que te van a poder encontrar igual. Por ello, ambos métodos de ocultación son absolutamente necesarios para usar tanto redes socialas privativas como libres. Si una de los dos métodos de ocultación falla, estarás expuesta.
-   **Empresas**: En general, como individuos somos irrelevantes (salvo excepciones, como la del cura de grindr). No les interesa Paco Perez, les interesa su edad, sexo, género y gustos. Y todo ello se puede conseguir tanto en redes sociales como privadas (aunque en mayor medida en las privativas). Si compartes una foto de la compra en la tienda del barrio para hacerles promoción, les estás diciendo tus gustos. Aunque sea una red social libre, son datos accesibles públicos. Por ello, uses la red que uses, si quieres protegerte de una empresa debes enmascarar tus datos. Usar protección de herramientas puede ser útil, pero no es la parte gorda de la que sacan tu información.
-   **Persona / colectivo**: En este caso da igual si usas redes privadas o libres, por que no tienen acceso a los servidores (a menos que hablemos de juankers malosos), a diferencia de los dos anteriores. De lo que se alimentan estos actores es del contenido de tus publicaciones. Por ello, en este caso, es irrelevante si usas una red social privada o pública. Lo importante es qué publicas en ellas y si pueden llegar a encontrar tus otras identidades, ya sea la física u otras identidades virtuales. Por lo tanto, la principal protección es la de protección por contenido.
-   **Mundo**: Papel de aluminio en la cabeza y al monte, poco más hay que hacer.

Si lo pensamos, los actores se diferencian solamente en el grado de recursos y de cosas ilegales que están dispuestos a hacer. Un gobierno siempre tendrá más recursos que una persona. Aunque una persona o un grupo de personas con la suficiente motivación pueden tocar las narices igual.

PD: algo que se puede ver en la matriz es que la solución a todos los problemas es irse a vivir a una cueva...


## Antisocial: Qué datos se pueden sacar de las redes sociales libres? {#antisocial-qué-datos-se-pueden-sacar-de-las-redes-sociales-libres}

Todo este tocho y yo en verdad venia a hablar de mi libro. Hace tiempo que me preocupa que nos pensemos que las redes sociales libres son seguras y anónimas, por que no lo son.

Estan sujetas a leyes, por lo que en el mejor de los casos tenemos que confiar en que los colectivos que las mantienen **rompan** la ley si tienen algún requerimiento legal. Algo que es mucho más fácil de decir que hacer. Por no hablar del peor de los males, que somos nosotras mismas. Somos nosotras quienes damos más información en nuestras publicaciones. Es este tema en concreto en el que yo quiero profundizar.

Como prueba de concepto, he pedido a unas buenas gentes del fediverso que me dejen descargar sus datos públicos y meterlos en una aplicación que he desarrollado que los trata. Hace dos cosas, mostrar una nube de palabras con sus palabras más usadas y muestra un gráfico de relaciones. En este gráfico de relaciones se ve con quien han interactuado, ya sea con reposts, favoritos o menciones.

Quiero agradecer a todas las personas que han ofrecido sus datos. Al final he cogido solo algunas pocas, tanto por facilidad como por problemas técnicos. Estas son:

-   <https://pleroma.libretux.com/@anoncf>
-   <https://mastodon.lol/@dark>
-   <https://todon.nl/@talogeta>
-   <https://mastodon.madrid/@notxor>
-   <https://pleroma.libretux.com/@izaro>
-   <https://qoto.org/@qapaq>

Quiero recalcar la parte en la que esta buena gente me ha permitido descargar sus datos. Me han dicho que si, pero no han tenido que hacer nada, por que yo podria haberlos descargados sin más. Si fuese un actor maligno, no se habrian enterado.

Para cerrar, hay dos temas de los cuales me parece interesante hablar, por lo que crearé dos hilos en el fediverso para ello:

-   Las redes sociales (libres o privativas), tiene ventajas y desventajas. Cúal es el balance? Los peligros en los que nos ponen sobrepasan las ventajas? El hilo es [este](https://barcelona.social/notice/AA82X5uJC1p9pKkk0O).
-   Que opinais de que se publique una herramienta que permita descargar y tratar los datos del fediverso? Concretamente hablo de **antisocial**, que es la herramienta que he desarrollado. El hilo es [este](https://barcelona.social/notice/AA82aSoAVK63ioUHx2).

Sin más dilación, sacad vuestras conclusiones. Los datos y parte de  se puede consultar [aquí](https://antisocial.daemons.it).
