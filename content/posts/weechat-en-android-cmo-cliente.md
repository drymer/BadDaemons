+++
title = "Weechat en android cómo cliente irc, xmpp y de GNU social"
author = ["drymer"]
date = 2016-06-12T20:44:00+02:00
tags = ["irc", "xmpp", "gnusocial", "android"]
draft = false
+++

En un [post anterior](https://daemons.it/posts/weechat-bitlbee/) comenté cómo instalar `weechat` y cómo usarlo unto a `bitlbee`, para tener de este modo tanto xmpp cómo irc en el mismo cliente, por lo que esa parte ya está cubierta. Lo que pasa es que se me olvidaron un par de cosas, la primera que bitlbee puede ser también un cliente de GNU social y la segunda, que hay un cliente muy molón en Android

La cosa es que weechat tiene una opción que le hace comportarse cómo un relay. Es decir, puede comportarse cómo si fuese un servidor, de este modo nos conectamos desde el móvil y podemos ver los buffers que ya tenemos abiertos.

## Añadir GNU social a Bitlbee {#añadir-gnu-social-a-bitlbee}

Así que al lío. Primero de todo, añadir la cuenta de GS. Hay que recordar que las siguientes órdenes se ejecutan siempre en el buffer `&bitlbee`. Os fiaréis en que el protocolo no se llama GNU social sino identica. Se han quedado un poco en el pasado, pero bueno, funciona igual. Pondré el ejemplo de configuración para quitter.se, que es mi nodo. Lo único que deberéis cambiar es la **base_url** y el usuario y contraseña, lógicamente.

```sh
account add gnusocial
# con la siguiente orden se ve que id tiene asociada la cuenta, supondremos que es 5
ac l
ac 5 set base_url https://quitter.se/api
ac 5 set username drymer
ac 5 set password blablabla
ac 5 on
```

Con esto ya se puede funcionar. La última orden lo que hace es enchufar la cuenta, y debería conectarse sin más. Lo siguiente son algunos parámetros que se pueden cambiar para mejorar algunas cosas. Igual algunas vienen configuradas cómo las mostraré, pero cómo no recuerdo si lo estaban, por si acaso.

```sh
# así veremos todos los parámetros posibles
ac 5 set
ac 5 set auto_connect true
ac 5 set auto_reconnect true
ac 5 set fetch_interval 60
ac 5 set fetch_mentions false
ac 5 set oauth false
ac 5 set show ids true
```

El funcionamiento no es complicado. El buffer creado, que deberías ser `#identica_gnusocial`, es un buffer que funciona por comandos. No están muy documentados, pero igualmente yo no lo uso para mucho más que leer y algún fav, para contestar en condiciones voy a la web. En cualquier caso, estos son `fav`, `rt`, `post` y `reply`. En todos, exceptuando el `post`, cómo segundo parámetro hay que pasarle el id de un post, que sale antes de este.

## Usar weechat desde un Android {#usar-weechat-desde-un-android}

Pues toda la información necesaria está en la [wiki del cliente](https://github.com/ubergeek42/weechat-android/wiki/Quickstart-Guide), pero bueh, cómo está en inglés ahí va. Primero de todo, obviamente, hay que instalar el cliente. Se puede hacer desde [f-droid](https://f-droid.org/repository/browse/?fdfilter=weechat&fdid=com.ubergeek42.WeechatAndroid). Una vez hecho, hay que pararse a reflexionar. Primero de todo, no lo he dicho por que lo he dado por supuesto, pero todo esto tiene sentido únicamente si weechat está encendido siempre.

Dicho esto, hay varias opciones de conexión. Sólo comentaré dos por que las otras son más complejas y a mi parecer innecesarias. La primera es simplemente abrir el puerto en el router que apunte a la ip de nuestro servidor, al puerto **8001**, que es el del relay (ya se verá más adelante cómo se configura el relay). La segunda, es conectarse mediante `ssh`. Dado que el puerto del ssh ya lo tengo abierto en mi casa y además tiene `fail2ban` presente, me quedo mucho más tranquilo que abriendo un puerto con la poca o nula seguridad que pueda tener `weechat`. Veremos cómo hacerlo usando `ssh`.

Primero de todo, configuraremos el relay en el `weechat` del servidor:

```sh
/relay add weechat 8001
/set relay.network.password "Contraseña Tela de Segura"
/save
```

Increíblemente complicado. Ahora, por la parte del cliente en el móvil. Soy demasiado vago para enchufar el móvil al ordenador y hacer capturas, así que pondré las órdenes sin más, que tampoco tienen mucha pérdida. Le damos a los tres puntitos en la esquina superior derecha y ahí a **Settings**. Dentro de este, **Connection**. En **Connection type** escogemos `SSH Tunnel`. Ahora, a configurar el `ssh`. Es bastante lógico:

- **SSH host**: dominio o ip de la máquina (la externa, no seáis brutos)

- **SSH port**: el puerto del `ssh`

- **SSH username**: el usuario del `ssh`

Para el último paso, hay que escoger. O contraseña para el `ssh` o una clave pública que previamente se autorice en el `~/.ssh/authorized_keys` del servidor. Si se escoge la contraseña, recordad borrar el valor por defecto que hay en **SSH private key file**.

Hecho esto, en el **Relay host** se tiene que poner localhost si el relay está en la misma máquina a la que se hace `ssh`, **Relay port** es 8001 y en **Relay password** la contraseña tela de segura. Con esto, ya estamos funcionando. Luego ya es cosa de echarle un ojo a todas las opciones restantes, que valen la pena. Además, en esta última versión han añadido la posibilidad de usar temas, lo cual mola mucho. Ahí van tres capturas de regalo.

{{< figure src="/img/weechat-Android-1.png" >}}

{{< figure src="/img/weechat-Android-2.png" >}}

{{< figure src="/img/weechat-Android-3.png" >}}
