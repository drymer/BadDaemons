+++
title = "Nikola.el v0.1 - Nikola desde emacs"
author = ["drymer"]
date = 2017-02-20T08:30:00+01:00
tags = ["emacs", "nikola", "nikolael"]
draft = false
+++

Este es un anuncio del que es mi primer paquete para emacs, `nikola.el`. Ya escribí hace tiempo un articulo sobre [org2nikola](https://daemons.cf/posts/nikola-emacs-gestionar-un-blog-de-contenido-esttico/), paquete que sigo usando. Pero este paquete solo permite exportar de org-mode a html, no permite construir la web o desplegarla, tienes que hacerlo desde la terminal.

Ya desde el principio tenia unas cutre-funciones que me permitían hacer lo más básico desde emacs, pero decidí ponerlo bonito y desarrollarlo un poco más, y así ha salido este paquete. De momento está solo en mi repositorio, en un tiempo lo meteré en elpa.

En este momento, permite crear un sitio web, crear articulos y páginas, construir el sitio web, desplegarlo, iniciar/detener el servidor web y ejecutar ganchos (hooks). Y poco más que añadir. Para verlo en mayor profundidad, hay que visitar el [Git](https://git.daemons.cf/drymer/nikola.el#org145399f).
