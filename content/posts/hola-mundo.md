+++
title = "Hola Mundo"
author = ["drymer"]
date = 2015-08-25T19:15:00+02:00
draft = false
+++

Pues nada, todo en la vida tiene un principio y un fin. Escribiré aquí a partir de ahora, que eso de escribir las chorradas que uno hace ayuda a entenderlas mejor y hace más fácil documentarlas. Todos los articulos que he escrito anteriormente están con la etiqueta de "elbinario", por si tal.

Una curiosidad para las personas pocas observadoras, esto es un servicio oculto de tor. Quien quiera ver cómo hacer uno, puede echarle un ojo a la [etiqueta de tor](http://daemon4jidu2oig6.onion/categories/tor), ahí hay un par de articulos al respecto. Lo mismo con el [git](http://daemon4jidu2oig6.onion/cgit), usa cgit y gitolite y funciona a través de tor. La web está hecha con [nikola](https://getnikola.co), los comentarios con [isso](http://posativ.org/isso) y todo es maneado casi exclusivamente con emacs. Probablemente explique cómo lo he hecho en el siguiente articulo.

Poco más que añadir. Bienvenida a Bad Daemons, un nombre de blog sin sentido, cómo cualquier otro.

PD: Es posible que los articulos escritos anteriormente tengan algunos fallos de diseño. Por ejemplo, no hay colores en las etiquetas de código. Eso es debido a que estos articulos han sido exportados y salen con un formato concreto (HTML) y para cambiarlo debería hacerlo a mano. Y cómo me da mucha pereza, así se quedará. En el futuro, el código se verá concretamente.
