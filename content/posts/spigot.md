+++
title = "Spigot: Publicando Feeds En Pump.io"
author = ["drymer"]
date = 2015-04-30T19:15:00+02:00
draft = false
tags = ["sysadmin", "elbinario"]
url = "/posts/spigot:-publicando-feeds-en-pump-io/"
+++
Andaba pensando en hacer algo similar a [gnusrss](https://elbinario.net/2015/02/11/gnusrss-publicando-feeds-en-gnu-social/) pero para [Pump.io](https://elbinario.net/?s%3Dpump), pero pude ahorrarmelo al preguntar en [la sala de redeslibres](https://www.mijabber.es/jappix/?r%3Dredeslibres@salas.mijabber.es) y saber de la existencia de [Spigot](https://pypi.python.org/pypi/spigot). Este programa hace lo que promete, publicar los feeds en Pump.io pero además con una particularidad, se encarga de hacerlo sin floodear la cuenta, por si el RSS en cuestión tiene muchas actualizaciones.

Al lío, para instalar Spigot:

```bash
su -c "pip install spigot"
```

Instalado. Ahora, crearemos un directorio dónde más convenga con mkdir para el archivo de configuración se spigot y empezaremos.

```bash
mkdir ~/spigot; cd ~/spigot
```

Ahora, ejecutamos spigot.py y nos saldrá un prompt con varias preguntas, debería ponerse algo similar a lo siguiente:

<pre>
No configuration file now, running welcome wizard.
2015-04-30 01:14:52,582 WARNING: Could not load configuration file
Adding user
Webfinger ID (e.g. bob@identi.ca): elbinario@pumpdog.me
Please follow the instructions at the following URL:
<https://pumpdog.me/oauth/authorize?oauth_token=4AAVqz7asfasdka2k1929a>
Verifier: 21390123jsdaasd-sadasd
Adding feed&#x2026;
Feed URL: <http://elbinario.net/feed/>
Choose an account:

1.  elbinario@pumpdog.me

Number: 0
Minimum time between posts (minutes): 30
Spigot formats your outgoing posts based on fields in the feed
              being scanned. Specify the field name surrounded by the '%'
              character to have it replaced with the corresponding value for the
              item (e.g. %title% or %link).
The following fields are present in an example item in
                     this feed:
summary<sub>detail</sub>
published<sub>parsed</sub>
links
title
authors
slash<sub>comments</sub>
comments
summary
content
3guidislink
title<sub>detail</sub>
link
author
published
author<sub>detail</sub>
wfw<sub>commentrss</sub>
id
tags
Format: %title%: %content% - %link%
</pre>

Esto creará un archivo json con esta pinta:

```json
{
    "feeds": {
        "<http://elbinario.net/feed>": {
            "account": "elbinario@pumpdog.me",
            "interval": 30,
            "format": "%title%: %content% %link%"
        }
    },
    "accounts": {
        "elbinario@pumpdog.me": {
            "consumer<sub>secret</sub>": "alsdñ22ñlndladmasd30llañsdña",
            "oauth<sub>token</sub>": "02lsdaa0d9ajdasdasd",
            "consumer<sub>key</sub>": "3292342l3423mkl4mk23i423",
            "oauth<sub>token</sub><sub>secret</sub>": "asddk3o0as0d0a0sd0a9s9d98a90121123"
        }
    }
}
```

Y poco más. Sólo queda meter en el crontab la ejecución de este programa. Se puede poner en un intervalo de 30 minutos, si no tiene mucha actividad el sitio web, con el siguiente formato:

<pre>\*/30 \* \* \* \* cd ~/spigot && spigot.py</pre>

Y a volar.

PD: Si, todas las claves de autentificación son muy inventadas.

