+++
title = "Pasarela de XMPP/jabber y Telegram."
author = ["drymer"]
date = 2016-05-08T22:25:00+02:00
tags = ["xmpp", "python"]
draft = false
+++

No tiene mucho misterio, sirve para hablar desde una sala de jabber a un grupo de Telegram. A continuación pego sin más el `README` del programa. Para el futuro, recomiendo mirar el repositorio git directamente. Se puede ver [aquí](http://daemons.cf/cgit/jabbergram/about/). Se aceptan sugerencias y pruebas.


## Acerca de {#acerca-de}

Con este programa es posible utilizar una sala `XMPP` para hablar con un grupo de `Telegram` y viceversa. El obetivo de este programa es el de ser sólo una pasarela sencilla, sólo tiene que pasar el texto de un lado al otro. Una vez que sea estable, probablemente no tendrá más mejoras, ya que yo no las necesito.


## Instalación {#instalación}

Como con cualquier programa escrito en `Python`, debería ser usado en un entorno virtual (virtualenv), pero eso queda a la elección del usuario. Es posible utilizar uno de los siguientes métodos de instalación:

Instalar a través de `pip`:

```sh
su -c "pip3 instalar Jabbergram"
```

Clonar el repositorio:

```sh
git clone https://daemons.it/drymer/Jabbergram
cd Jabbergram
su -c "pip3 instalar requirements.txt -r"
su -c "python3 setup.py instalar"
```


## Configuración {#configuración}

Este programa es simple, no tiene ni un menú de `ayuda`. Lo primero que hay que hacer es crear el bot de `Telegrama`. Para ello, hay que tener una cuenta de `Telegram` y hablar con [BotFather](https://telegram.me/botfather). A continuación, ejecuta:

```sh
 /start
 /newbot
 NombreDelBot # terminado en bot, siempre
 # A continuación, se mostrará el token del bot, hay que guardarlo
 /setprivacy
 NombreDelBot
 # Ahora hay que pulsar desactivar
```

La opción `/setprivacy` es para hacer que el robot pueda leer todo lo que se dice en el grupo, no sólo cuando se utilizan los comandos. Es necesario para hacer que `Jabbergram` funcione. Más información sobre la creación los bots de `Telegrama` en su [página web](https://core.telegram.org/bots).

A continuación, hay que crear un archivo de configuración, que llamaremos `config.ini`. En ese archivo, introduce los siguientes parámetros:

```ini
[Config]
ID = exampleid@nope.org
password = difficultPassword
muc_room = exampleMuc@muc.nope.org
nick = Jabbergram
token = JabbergramBotTokken
group = -10,293,943,920
```

La única cosa que vale la pena mencionar es la sección del `token` (que es la que nos da cuando se crea el robot) y el `group`, que es ID del grupo de `Telegram`.

No hay manera fácil de ver el ID desde `Telegram`, por lo que se puede utilizar el programa llamado `seeIdGroups.py`. Para ejecutarlo sólo es necesario establecer el parámetro `token` del archivo de configuración. Necesitarás que alguien invite al bot al grupo. Además, las personas de ese grupo deben enviar algunos mensajes, para que el programa pueda coger su ID. Puede llevar unos segundos el que aparezcan los mensajes. Cuando se tenga el ID de grupo que se quiere, sólo hay que pulsar Ctrl-c, copiarlo en el archivo de configuración (incluido el símbolo menos), y la configuración estará terminada.


## Uso {#uso}

Se pueden crear todos los archivos de configuración que se deseen. Sólo tendrás que pasarlo como parámetro al programa,  si no se hace se intentará coger el archivo `config.ini` por defecto, y se producirá un error si no existe:

```sh
# Es recomendable utilizar tmux o la pantalla para ejecutar la siguiente orden
Jabbergram.py example.ini
```


## Licencia {#licencia}

```text
This program is free software: you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, Either version 3 of the License, or
(At your option) any later version.

This program is distributed in the hope That it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
Along With This Program. If not, see <http://www.gnu.org/licenses/>.
```
