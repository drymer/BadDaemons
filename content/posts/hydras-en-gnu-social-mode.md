+++
title = "Hydras en gnu-social-mode"
author = ["drymer"]
date = 2017-01-31T22:56:00+01:00
tags = ["gnusocial", "emacs"]
draft = false
+++

Que son las hydras? Son un animal mitológico de muchas cabezas. Además, es un paquete de emacs que facilita la repetición de teclas. Para que quede más claro, y siguiendo el ejemplo de la [página oficial](https://github.com/abo-abo/hydra), imaginad que queréis apretar **C-n** cinco veces para bajar cinco lineas más abajo. Lo normal seria simplemente presionar **C-n** cinco veces, pero con hydra podríamos presionar **C-n nnnn**. Es un ejemplo tonto, pero ved el gif siguiente:

{{< figure src="/img/hydra.gif" >}}

Este es un ejemplo simplificado del que se puede ver en [su wiki](https://github.com/abo-abo/hydra/wiki/Emacs), en la sección **Movement.** Pero no solo permite facilitar el movimiento, en cierto modo ofrece una interfaz gráfica, como veremos en breves. Este paquete tiene muchísimo potencial y yo apenas lo uso, solo de vez en cuando con [ivy](http://oremacs.com/swiper/#hydra-in-the-minibuffer) y con `gnu-social-mode`. Por ello, recomiendo echar un ojo a [su wiki](https://github.com/abo-abo/hydra/wiki). Para ver ejemplos más prácticos.

A lo que vamos, la siguiente hydra es una adaptación simplificada de la de `twittering-mode` que sale en la wiki del paquete. Al fin y al cabo, `gnu-social-mode` es una versión actualizada de `identica-mode`, que a su vez es una adaptación de `twittering-mode`. Recordemos que ya vimos [gnu-social-mode](https://daemons.cf/posts/cliente-de-gnu-social-para-emacs).

No voy a explicar demasiado el funcionamiento por que es bastante evidente, cada letra tiene asignada su propia función. Solo dejar claro que para activar esta hydra debe presionarse **C-o** en `gnu-social-mode`. Algunas puede que no funcionen, es cosa de `gnu-social-mode`, pero en general, todo va bien.

{{< figure src="/img/gnu-social-hydra.png" >}}
