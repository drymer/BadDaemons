+++
title = "Escapar símbolos al usar marcado en org-mode"
author = ["drymer"]
date = 2016-05-30T09:34:00+02:00
tags = ["orgmode", "emacs"]
draft = false
url = "/posts/cosillas-de-emacs-escapar-simbolos-al-usar-usar-marcado-en-org-mode/"
+++

Un caso que puede sonarle a alguien, es usar rutas. Si se escribe en org-mode las palabras /home/drymer/, por ejemplo, pasará que al exportar o en el mismo buffer si tenemos activado `org-hide-emphasis-markers`, veremos que /home/drymer/ pasa a ser _home/drymer_, en cursiva y sin la primera y última barra. Por suerte, hay una manera sencilla de escapar estos carácteres usando entidades org (`org-entities`).

Las entidades de org son parecidas al latex. En vuestro editor con un buffer en `org-mode`, escribid `\ast{}`. Se ve igual? De ser así, ejecutad `M-x org-toggle-pretty-entities` y voila, el `\ast{}` pasa a ser un &lowast;.

Ahora, alguien se podría dar cuenta de que cada símbolo que se quiera usar tiene su "código" asociado, por lo que memorizarlo debe ser acojonante. Pero llega al recate el conocimiento en común. Hay un humano que soluciona este problema, cómo se puede ver en [Stack Overflow](http://emacs.stackexchange.com/questions/16688/how-can-i-escape-the-in-org-mode-to-prevent-bold-fontification/16746#16746). Cómo ahí se cuenta, hubo un debate de la mejor manera de escapar símbolos y acabó sacando ese par de funciones. Aún así, estaba hecha para funcionar en emacs 25, en las 24.&lowast; no funcionaba. Pero, después de un [intercambio de correos](http://lists.gnu.org/archive/html/emacs-orgmode/2016-05/msg00135.html) en la lista pudo arreglarlo y actualizo la función que aparece en Stack Overflow. Ahora, es tan sencillo cómo presionar `C-u SIMBOLO`, siendo SIMBOLO el símbolo que queremos que aparezca. Bien sencillo. Y ahora, el código.

```emacs-lisp
(setq org-pretty-entities t)

(defun modi/org-entity-get-name (char)
"Return the entity name for CHAR. For example, return \"ast\" for *."
(let ((ll (append org-entities-user
                  org-entities))
      e name utf8)
  (catch 'break
    (while ll
      (setq e (pop ll))
      (when (not (stringp e))
        (setq utf8 (nth 6 e))
        (when (string= char utf8)
          (setq name (car e))
          (throw 'break name)))))))

(defun modi/org-insert-org-entity-maybe (orig-fun &rest args)
"When the universal prefix C-u is used before entering any character,
insert the character's `org-entity' name if javailable."
(let ((pressed-key (char-to-string (elt (this-single-command-keys) 0)))
      entity-name)
  (when (and (listp args) (eq 4 (car args)))
    (setq entity-name (modi/org-entity-get-name pressed-key))
    (when entity-name
      (setq entity-name (concat "\\" entity-name "{}"))
      (insert entity-name)
      (message (concat "Inserted `org-entity' "
                       (propertize entity-name
                                   'face 'font-lock-function-name-face)
                       " for the symbol "
                       (propertize pressed-key
                                   'face 'font-lock-function-name-face)
                       "."))))
  (when (null entity-name)
    (apply orig-fun args))))

(advice-add 'org-self-insert-command :around #'modi/org-insert-org-entity-maybe)
```
