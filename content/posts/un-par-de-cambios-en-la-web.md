+++
title = "Un par de cambios en la web"
author = ["drymer"]
date = 2017-02-15T08:30:00+01:00
draft = false
+++

Este articulo será aburrido, solo quiero comentar un par de cosas.

A partir de ahora, mi repositorio está en [https://git.daemons.it/drymer/](https://git.daemons.cf/drymer/), en vez de daemons.it/cgit/. La nueva página usa gitea, en vez de [cgit + gitolite](https://daemons.cf/posts/autogestin-montar-y-administrar-gitolite-cgit-git-daemon/) como hasta ahora. He decidido a hacer el cambio por que, aunque al final no me sirva de nada, quiero tener un gestor de incidencias. En un primer momento fui a usar GitLab, pero los requisitos mínimos son absurdos. Pide 1GB de ram + 3GB de swap. Y aunque es cierto que está pensado para soportar muchísimos usuarios, yo quiero algo más casero que solo me sirva para mi de momento y en un futuro pueda abrir al público. Y en esto Gitea (un fork comunitario de Gogs, más información [aquí](https://blog.gitea.io/2016/12/welcome-to-gitea/)) le da mil patadas a GitLab. No he hecho ningún benchmark, pero Cgit es considerada la interfaz web de repositorios git más rápida (por algo la usan en el [repositorio del Kernel](https://git.kernel.org/cgit/)) y yo no noto diferencia respecto a Gitea. No haré ningún articulo de como instalarlo por que la verdad es que ha resultado muy muy fácil. Lo más complejo, si acaso, es como instalar Go, pero en Debian essie ya está en los repositorios, por lo que no será un problema.

Esto es lo primero, lo segundo es que a partir de ahora, aceptaré contribuciones en FairCoin. Me he decidido a aceptar donaciones, después del tiempo que llevo por dos motivos. El primero es que el dinero no me sobra, así que cualquier ayuda para pagar servidores será bien recibida, y el segundo es, precisamente, darle uso a FairCoin. No voy a explicar nada acerca de esta moneda ni de la Cooperativa que la creó por que no lo haré tan bien como puedan hacerlo ellos. Quien quiera leer sobre el tema, algo que recomiendo fervientemente, debería echar un ojo a los siguientes enlaces:

-   [https://fair.coop/es/construyendo-un-nuevo-sistema-economico/](https://fair.coop/es/construyendo-un-nuevo-sistema-economico/)

-   [https://fair.coop/es/faircoop-crea-una-guia-practica-para-saber-que-es-y-como-usar-faircoin/](https://fair.coop/es/faircoop-crea-una-guia-practica-para-saber-que-es-y-como-usar-faircoin/)

-   [https://fair.coop/?get\_group\_doc=26/1447331737-GuiaFaircoinUsuariosES.pdf](https://fair.coop/?get_group_doc=26/1447331737-GuiaFaircoinUsuariosES.pdf)

Y básicamente leer la página principal de la cooperativa, [https://fair.coop](https://fair.coop). Si a alguien le interesa estar en los grupos que tienen, pero le tira para atrás el que usen Telegram, en este momento estoy manteniendo unos cuantos enlaces de grupos de Telegram a salas XMPP mediante [Jabbergram](https://git.daemons.cf/drymer/jabbergram). Quien quiera verlas, puede echar un ojo a [este pad](https://board.net/p/r.1fab29f51cf0707ad94d919d51b5440a).

Y si alguien quiere contribuir, el ID de mi monedero es **fYH8LcKKCUyWntHshiMxihpEx4KcwQXK**.
