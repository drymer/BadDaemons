+++
title = "Cómo diseñar un rol de ansible - 1"
author = ["drymer"]
lastmod = 2021-11-28T18:24:41+01:00
tags = ["ansible", "desarrollo"]
draft = false
+++

He empezado una serie de videos en Peertube en los que explicaré como diseñar un rol de ansible.

El fin no es tanto cómo hacer un rol, ya que hay mucha documentación sobre el tema, sinó intentar mostrar el hilo del pensamiento cuando te encuentras con la necesidad de automatizar algo que tal vez no conozcas mucho y diseñar como será la interfaz para el usuario.

Peertube es compatible con cualquier implementación del fediverso, por lo que se puede [suscribir al canal](https://fediverse.tv/c/bad%5Fdaemons/videos) desde el botón de "Subscribe".

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" title="Cómo diseñar un rol de ansible - 1" src="<https://fediverse.tv/videos/embed/0dc56409-bc6a-4b0f-838f-57ac2913d80e>" frameborder="0" allowfullscreen></iframe>
