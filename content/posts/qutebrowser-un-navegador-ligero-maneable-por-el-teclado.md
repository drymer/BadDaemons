+++
title = "Qutebrowser: un navegador ligero manejable por el teclado"
author = ["drymer"]
date = 2016-10-17T08:30:00+02:00
tags = ["i3wm"]
draft = false
+++

Ya comenté en el primer articulo de Neomutt que hay dos tipos de programas que usaba que consumen mucho. Expliqué como cambiar el primero, el cliente de correo. Hoy toca cambiar el navegador.

Antes usaba Firefox, cómo la mayoría. Tiene muchos plugins y cosillas interesantes. Pero incluso cuando tenia mi querida torre funcionando, me planteé cambiarlo. Es un navegador muy, muy pesado, pero sobretodo lo que quería era depender menos del ratón. Encontré un modo de conseguir esto con [keysnail](https://github.com/mooz/keysnail/wiki/plugin), pero no me terminó de convencer. Luego empecé a probar navegadores para ser usados con el teclado. **Uzbl** y **Dwb**, básicamente. Me pareció que Uzbl era el más avanzado, pero aún así yo tenia que compilarlo por eso de usar Slackware, y me daba segfaults a cada paso. Por suerte encontré Qutebrowser.

Hay algunas cosillas que le faltan para suplir del todo el uso habitual que se le da a un navegador.

-   No soporta flash, lo cual tampoco es malo.

-   Los vídeos HTML5, depende de cómo sea, le cuesta (aunque hay manera de suplirlo, cómo se verá).

-   No tiene manera fácil de activar y desactivar javascript, aunque está en camino.

-   Los atajos de teclados son personalizables, pero aún están algo limitados para mi gusto.

-   Tema de bloqueo de anuncios. Creo que tiene soporte reciente, pero no lo he probado. Según he leído es algo ineficiente de momento, aunque todo se andará. Yo uso un archivo `/etc/hosts`, que por cierto es más recomendable de usar que un plugin. Mucho más eficiente.

Avisadas quedáis de sus limitaciones. Así que al lío, a instalarlo. Tiene paquete compatibles con Debian Jessie y derivados, Fedora, Arch y Parabola, Void Linux, NixOs, OpenSUse, OpenBSD y hasta Windows. Comentaré como instalarlo mediante pip, ya que de este modo se tiene la última versión de todo. Aún así, qt hay que instarlo de los repositorios.

Se instalan las dependencias:

```sh
# Debian
su -c "aptitude install python3 libqt5"
su -c "pip3 install qutebrowser"
```

No diré como instalarlo en Slackware paso a paso por que lo hice hace tiempo y no recuerdo el proceso exacto, ya que era un poco complejo. Esto es debido a que Slackware no tiene qt5 en repositorios ni en los slackbuilds oficiales, hay que tirar de un [SlackBuild de AlienBob](http://www.slackware.com/~alien/slackbuilds/qt5/). Una vez se tiene instalado, ya se puede instalar mediante `pip`.

Una vez hecho, pasamos a configurarlo. Veremos como usar los atajos de teclado básicos (con alguna captura), como cambiarlos, pondré mi configuración de colores, atajos y de `/etc/hosts` y por último como reproducir vídeos fuera del navegador mediante un script que usa `mpv` en conunto a `youtube-dl`. Todos los archivos de configuración usados están en el repositorio `dotfiles` de mi git, por lo que clonaremos ese git, que además tiene otras cosas, aunque no se instalarán.

```sh
git clone https://daemons.it/drymer/dotfiles
cd dotfiles
bash instala.sh dotfiles
```

Al ejecutar el script de instalación pedirá permiso de superusuario para instalar los scripts `capture` y `umpv` en `/usr/local/bin` y el archivo `hosts` en `/etc/hosts`, además de la configuración y los atajos de teclado de Qutebrowser. El script `capture` no lo comentaremos en este articulo.

Los atajos de teclado que uso los he cambiado un poco a los que vienen por defecto. No todo lo que me gustaría, si pudiese los habría hecho exactamente igual a los de emacs, pero bueno. Ahora mismo son más bien una mezcla de los de Firefox y otros navegadores y vim. A título de curiosidad, diré que gracias a este navegador le he empezado a coger el gusto al tema de la edición modal, así que igual en un futuro le echo un ojo a `evil-mode` de emacs. A continuación una tabla con los atajos que más uso.

| Función                                         | Atajo      |
|-------------------------------------------------|------------|
| Abrir buscador                                  | o          |
| Abrir dirección actual y editar                 | go         |
| Abrir nueva pestaña                             | C-t        |
| Abrir nueva ventana                             | C-n        |
| Cerrar pestaña                                  | C-w        |
| Moverse a la pestaña de la izquierda            | C-         |
| Moverse a la pestaña de la derecha              | C-k        |
| Mover la pestaña hacia la izquierda             | gl         |
| Mover la pestaña hacia la derecha               | gr         |
| Recargar página                                 | r / f5     |
| Ir una página hacia atrás en el historial       | H          |
| Ir una página hacia delante en el historial     | L          |
| Abrir link mediante el teclado                  | f          |
| Abrir link mediante el teclado en nueva pestaña | F          |
| Abrir link mediante el teclado en nueva ventana | wf         |
| Copiar link actual mediante el teclado          | ;Y         |
| Descargar página actual                         | C-e        |
| Descargar mediante teclado                      | C-d        |
| Moverse hacia la izquierda                      | h          |
| Moverse hacia la derecha                        | l          |
| Moverse hacia arriba                            | k          |
| Moverse hacia abajo                             |            |
| Reabrir pestaña cerrada                         | C-S-t      |
| Buscar                                          | C-s        |
| Buscar siguiente                                | n          |
| Buscar anterior                                 | N          |
| Ejecutar órdenes                                | M-x        |
| Entrar en modo de inserción                     | i          |
| Copiar url principal                            | yy         |
| Copiar título de la página                      | yt         |
| Copiar dominio                                  | yd         |
| Pegar url en la misma pestaña                   | pp         |
| Pegar url en pestaña nueva                      | Pp         |
| Pegar url en ventana nueva                      | wp         |
| Guardar quickmark                               | m          |
| Cargar quickmark                                | b          |
| Cargar quickmark en nueva pestaña               | B          |
| Guardar bookmark                                | M          |
| Cargar bookmark                                 | gb         |
| Cargar bookmark en nueva pestaña                | gB         |
| Guardar la configuración                        | sf         |
| Hacer zoom                                      | +          |
| Quitar zoom                                     | -          |
| Ver zoom                                        | =          |
| Ver código fuente de la página                  | C-S-u      |
| Cerrar todo                                     | C-q        |
| Cambiar a pestañas                              | M-[número] |
| Abrir url actual en mpv                         | U          |
| Abrir video mediante teclado en mpv             | u          |

No hay mucho que comentar, solo el tema de abrir los links desde el teclado. La idea es que se presiona la tecla y al lado de cada link aparecen de uno a tres carácteres. Cuando los presionas, se abre la pestaña. Tal como se ve en la imagen.

{{< figure src="/img/qutebrowser.png" >}}

De paso se puede ver los colores que uso en las pestañas, gris y azul, el que suelo usar. Solo queda comentar un par de cosas que igual interesa cambiar de mi configuración. Que use javascript por defecto y el buscador. Usa duckduckgo, pero es la versión html. Para cambiar ambos parámetros hay que editar las lineas 870, 980 y 151. Una vez activado el javascript y reiniciado el navegador, si se va a qute://settings se puede modificar la configuración de manera interactiva.

Para más información, se puede consultar la [hoja de atajos de teclado](http://qutebrowser.org/img/cheatsheet-big.png) o la [ha de comandos](https://github.com/The-Compiler/qutebrowser/blob/e4da6be5e9f181f2e630d5e2147e9b05da5059d9/doc/help/commands.asciidoc).

**Actualización**: Gracias al comentario de Florian, el desarrollador principal de Qutebrowser, puedo corregir algunas cosas que puesto más arriba. Cito:

-   Es posible usar Flash, se activa con ":set content allow-plugins true"

-   Los vídeos en HTML5 deberian poder verse si se tienen las dependencias apropiadas de GStreamer, pero algunas personas, en Debian, han tenido problemas.

-   Para javascript, se puede asignar una tecla para activar y desactivarlo.

-   El adblock funciona y consume casi lo mismo que el archivo /etc/hosts, se puede actualizar la lista usando ":adblock-update"
