+++
title = "Ansible en emacs"
author = ["drymer"]
date = 2017-06-06T20:16:00+02:00
tags = ["emacs", "ansible"]
draft = false
+++

Últimamente he estado toqueteando bastante Ansible. Para quien no lo conozca, Ansible es un gestor de configuraciones. Permite instalar y configurar programas y configuraciones en un ordenador o en 1.000. Por eso se usa en el ámbito de la Integración Continua. Pero bueno, ya hablaré más en profundidad de ello en otro articulo.

Por ahora hablaré de lo que uso para editar playbooks. Uso `yaml-mode`, `ansible-doc` y `ansible-vale-mode`. Por suerte los playbooks de Ansible se escriben en el maravilloso formato Yaml, muy cómodo para editarlo. El modo no ofrece mucho más que la identación y los colores, pero tampoco hace falta mucho más:

```emacs-lisp
(use-package yaml-mode
:ensure t)
```

`ansible-doc` lo que hace es ofrecer el comando de línea `ansible-doc` pero desde emacs, con [helm](https://daemons.it/tags/helm/) o [ivy](https://daemons.it/tags/ivy/) si se usa alguno. La primera linea de la sección **config** enlaza el `ansible-doc-mode` al `yaml-mode`, por lo que cuando se edite yaml, solo hay que ejecutar `C-?` y se accederá a los docs. Por defecto la primera vez que se lo llama tarda bastante en responder, por que guarda la respuesta para que las siguientes veces sea más rápido. Yo lo que he hecho es añadir la segunda línea, que hace que este cacheo se produzca al iniciar emacs. Tarda más en arrancar emacs, pero dado que solo lo arranco una vez cada muchas semanas, no me resulta un problema.

```emacs-lisp
(use-package ansible-doc
  :ensure t
  :config
  (add-hook 'yaml-mode-hook #'ansible-doc-mode))
```

Y por último, `ansible-vault`. Este paquete es otro wrapper de su [equivalente de terminal](http://docs.ansible.com/ansible/playbooks_vault.html). Para quién no lo sepa, vault es la forma de gestionar variables sensibles en ansible. Se puede usar de dos formas, con una contraseña genérica o una por proyecto. Para lo primero, solo hay que crear un fichero `~/.vault_pass`. Para lo segundo, hay que hacer uso del fichero `.dir-locals.el`. Este fichero permite concretar variables por directorio, como su nombre indica. Es algo propio de emacs, no de `ansible-vault`. Por lo tanto, en ese fichero habría que poner algo como esto:

```emacs-lisp
((yaml-mode
(ansible-vault-pass-file . "~/.ansible-vault/custom_vault_pass")))
```

```emacs-lisp
(use-package ansible-vault
  :ensure t
  :config
  (defun ansible-vault-mode-maybe ()
    (when (ansible-vault--is-vault-file)
      (ansible-vault-mode 1)))
  (add-hook 'yaml-mode-hook 'ansible-vault-mode-maybe))
```

Con esto, se hace mucho más fácil y cómodo editar playbooks de Ansible. Y si además queremos visitar la página de la documentación oficial, siempre se puede usar [counsel-dash](https://daemons.it/posts/ver-documentacion-usando-ivy/).
