+++
title = "Escribir prosa en emacs"
author = ["drymer"]
date = 2016-03-19T02:00:00+01:00
tags = ["orgmode", "emacs"]
draft = false
+++

Hoy inauguro una sección del blog que se llamará cómo el título, "Cosillas de emacs" seguido de un guión y un nombre descriptivo de lo que sea de lo que se habla. A título de curiosidad, [nikola](http://daemons.cf/posts/nikola-emacs-gestionar-un-blog-de-contenido-esttico/) crea RSS de las diferentes etiquetas que se usan en el sitio, por lo que si sólo interesa subscribir se a, por ejemplo, la etiqueta **emacs**, se va abajo del todo, etiquetas, emacs y se verá un índice con todos los articulos y el link al RSS. Mola.

Hay un motivo muy fuerte para escoger org-mode delante de cualquier otro lenguaje de marcado, y es la opción de la exportación. A continuación una lista de los formatos a los que se puede exportar **por defecto**: ascii, beamer, html, icalendar, latex, man, md, odt, texinfo. Y en [contribuciones](http://orgmode.org/worg/org-contrib/#orgheadline2) unos cuantos más: bibtex, confluence, freemind, rss, entre otros.

Si por lo que sea se odia org-mode, se puede usar **markdown**, **ReST**, **LaTeX** con sus respectivos modos markdown-mode, rst-mode y latex-mode. Algunos modos o paquetes complementarios:

-   Markdown: [markdown-toc](https://github.com/ardumont/markdown-toc) y [markdown-preview-mode](https://github.com/ancane/markdown-preview-mode)

-   LaTeX: [latex-preview-pane](https://www.emacswiki.org/emacs/LaTeXPreviewPane)

Y hasta aquí las cosas aburridas, ahora **org-mode** y la diversión (sin banderas). Primero unas pocas líneas de código lisp que a mi me van bien.

Por defecto emacs trunca las líneas largas, con la siguiente línea esto se evita.

```lisp
;; No truncar
(setq toggle-truncate-lines t)
```

Seguir los hipervínculos apretando RET.

```lisp
;; RET or TAB to follow a link
(setq org-return-follows-link t)
(setq org-tab-follows-link t)
```

Una función que ayuda en la creación de bloques de código. Se evalúa lo siguiente y al ejecutarla (`M-x org-insert-src-block`) se escogerá el lenguaje del código y se abrirá un buffer en el que se podrá editar el código.

```lisp
;; insert source block
(defun org-insert-src-block (src-code-type)
"Insert a `SRC-CODE-TYPE' type source code block in org-mode."
(interactive
 (let ((src-code-types
        '("emacs-lisp" "python" "C" "sh" "java" "s" "clojure" "C++" "css"
          "calc" "asymptote" "dot" "gnuplot" "ledger" "lilypond" "mscgen"
          "octave" "oz" "plantuml" "R" "sass" "screen" "sql" "awk" "ditaa"
          "haskell" "latex" "lisp" "matlab" "ocaml" "org" "perl" "ruby"
          "scheme" "sqlite")))
   (list (ido-completing-read "Source code type: " src-code-types))))
(progn
  (newline-and-indent)
  (insert (format "#+BEGIN_SRC %s\n" src-code-type))
  (newline-and-indent)
  (insert "#+END_SRC\n")
  (previous-line 2)
  (org-edit-src-code)))
```

Resaltar la sintaxis de dentro de los bloques de código.

```lisp
(setq org-src-fontify-natively t)
'(org-fontify-inline-src-block)
```

Con esto se cubre lo básico. Para la corrección ortográfica se puede usar `ispell`, que viene instalado por defecto. Se ejecuta con `M-x ispell-buffer`, por ejemplo.

Aún quedan pequeñas cosas, pero ya irán saliendo. El último par de cosas son una chorrada (sobretodo la segunda), pero nunca se sabe. [olivetti-mode](https://github.com/rnkn/olivetti) centra el texto en medio de la pantalla dejando laterales, permitiendo así poder concentrarse en la escritura. Y la última gilipollez, [selectric-mode](https://github.com/rbanffy/selectric-mode) que imita el ruido de una máquina de escribir. Por lo de los teclados silenciosos y eso. En próximas entregas se verá cómo sacarle más provecho aún a **org-mode**.
