+++
title = "Nuevo planet de emacs-es"
author = ["drymer"]
date = 2016-06-29T16:44:00+02:00
tags = ["emacs"]
draft = false
+++

Esto es un anuncio, poco hay que decir. Entre [@maxxcan](http://www.maxxcan.com) y yo hemos montado un planet de emacs-es. Con suerte, esto facilitará encontrar nuevos sitios webs y blogs sobre emacs en castellano. Incluso alguna persona que suela escribir en inglés tal vez se anime a hacerlo en castellano. La web es [http://planet.emacs-es.org](http://planet.emacs-es.org). Varias personas andamos en `#emacs-es` de freenode, por si eso. También hay un grupo en GNU social, en [https://quitter.se/group/emacses](https://quitter.se/group/emacses) (que no es del planet en si, pero publicará ahí) y la cuenta que publicará es [@emacses](https://quitter.es/emacses).

PD: En los próximos días publicaré un articulo explicando cómo lo hemos montado.
