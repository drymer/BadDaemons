+++
title = "Hybridbot: Bot pasarela irc - Jabber"
author = ["drymer"]
date = 2016-09-22T08:30:00+02:00
tags = ["python", "irc", "xmpp"]
draft = false
+++

Hace un tiempo me enteré de que había una comunidad de emacs en castellano en Telegram y pensé que seria buena idea intentar linear con la sala de `#emacs-es` de freenode. Al principio busqué un programa que linkease irc con un grupo de telegram directamente, pero como no me convenció lo poco que vi, decidí usar Jabbergram y linkear la sala xmpp con la del irc, teniendo así tres sitios posibles en los que la gente pueda entrar. Me puse a buscar y vi que la mayoría de bots pasarela que habían entre XMPP y irc eran muy viejos y que no funcionaban o lo hacían a duras penas. Y me acordé de hybridbot.

Le pedí a ninguno si me dejaba terminar el programa y ponerlo bonito, a lo que accedió. Me puse manos a la obra y por cosas de la vida le interesó a Xrevan, la persona que administra el bot pasarela de la sala `#social` de freenode. Le gustó poder abandonar `irc` (y no le culpo por ello) por lo que se puso a mejorar **hybridbot**. Llegó a modificarlo tanto que ya no se parece demasiado a lo que hizo ninguno ni a lo que yo lo mejoré. Y gracias a ello ahora se puede usar tanto en Python 2 como en Python 3 (entre otras cosas) y funciona muy bien. Lleva unas semanas enchufado tanto en `#social` como en `#emacs-es`. Veamos como usarlo.

Primero de todo clonaremos el repositorio e instalaremos las dependencias:

```sh
git clone https://daemons.it/drymer/hybridbot/
cd hybridbot
su -c "pip install -r requirements.txt"
```

Ahora solo queda editar el archivo de configuración. Las variables que aparecen son las siguientes:

```ini
[Shared]
prefix   = .
owner    = somebody

[IRC]
channel  = #daemons
nick     = pasarela
server   = chat.freenode.net
port     = 6667

[XMPP]
id      = becario@daemons.it
password = goodpassword
muc      = testeando@salas.daemons.it
nick     = pasarela
```

La sección `shared` tiene dos variables. La primera establece que prefijo se usará para usar los comandos que acepta el bot, que son `help` y `users`. El prefijo establece si se llama escribiendo `.users` o `!users`, según el prefijo que se quiera poner. La segunda es el nombre que mostrará cuando se ejecute `help`, para que la gente pueda ver quien administra el bot. Las demás secciones no tienen nada que explicar. Puertos, nicks y salas a las que conectarse. Una vez editado, se ejecutará del siguiente modo:

```sh
python hybridbot.py config.ini
```

Como podéis ver, acepta el archivo de configuración como parámetro, por lo que se pueden usar tantas pasarelas como archivos de configuración se tenga. Si solo se tiene uno y se llama `config.ini`, no hace falta pasarlo como parámetro.
