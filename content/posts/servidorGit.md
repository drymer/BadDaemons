+++
title = "Autogestión: Montar y Administrar gitolite + cgit + git-daemon"
author = ["drymer"]
date = 2015-07-05T19:15:00+02:00
draft = false
tags = ["sysadmin", "elbinario"]
+++
Los repositorios git son una de las mejores cosas que se han hecho a nivel de commpartir. No sólo permite descargar y ojear el código sinó que además permite ver que cambios se han hecho cuando, aumentando así la confianza que se pueda tener en ese programa. Así que al lío. Veremos cómo se instala en un servidor con Debian Wheezy. La parte de la instalación es fácil:

```bash
aptitude install git gitolite3
```

Nos pedirá una clave SSH. Podemos pasar por que vamos a reconfigurar el paquete, para ajustar algunas cosillas.

```bash
dpkg-reconfigure gitolite3
```

Nos preguntará que usuario vamos a usar, directorio home de ese usuario (dónde estaran los repositorios) y una clave SSH. Deberiamos poner la de nuestro ordenador. Una vez hecho, entraremos con el usuario <span class="underline">git</span> y iniciaremos el repositorio.

```bash
su git
gitolite setup -a admin
```

Veremos que en el home de git aparecen ya dos repositorios git, uno llamado testing y el otro gitolite-admin. Este es muy importante, por eso lo clonaremos a nuestro ordenador y administraremos todos los repositorios desde ahí.

```bash
git clone git@servidor.git.lol:gitolite-admin
cd gitolite-admin
```

Aquí veremos dos directorios, <span class="underline">keys</span> y <span class="underline">conf</span>. En el primero veremos un archivo llamado admin.pub, que contiene nuestra clave de administrador. En conf veremos gitolite.conf, que es dónde está la magia. Ahí añadiremos los repositorios y permisos de estos que queramos. Ahora, según las necesidades de cada persona, habrán distintas necesidades. Para una mejor comprensión, mejor leer la [documentación](http://gitolite.com/gitolite/gitolite.html#basic-admin). A continuación habrá un pequeño resumen con una pequeña explicación a modo de comentario.

```text
# gitolite.conf

# Declaramos los grupos que queramos
@usuarias = foo bar

# Necesario para que git-daemon acceda a los repositorios
repo @all
    R       = daemon

repo gitolite-admin
    RW+     =   admin

# Declaramos repositorios. El que tengan el mismo prefijo (ElBinario), hará que se divida en secciones, cómo veremos más adelante con cgit.
repo ElBinario/gnusrss
    RW+     =   @elbinario
    R       =   @all

repo ElBinario/eve
    RW+     =   @usuarias
    R       =   @all

repo ElBinario/alex
    RW+     =   @usuarias
    R       =   @all

repo ElBinario/bofh
    RW+     =   @usuarias
    R       =   @all

repo ElBinario/dni
    RW+     =   @usuarias
    R       =   @all

repo ElBinario/etherpad-lite-config-files
    RW+     =   @usuarias
    R       =   @all

repo ElBinario/asocial
    RW+     =   @usuarias
    R       =   @all

# Según lo que se quiera, esto puede no interesar. Esto permite a cualquier usuaria crear un repositorio.
repo CREATOR/[a-z].*
    C       =  @usuarias
    RW+     =  CREATOR
    RW      =  WRITERS
    R       =  READERS
```

Tras modificar el fichero conf/gitolite.conf, tendremos que hacer constar los cambios:

```bash
git add conf/gitolite.conf
git commit -m "initial commit"
git push --set-upstream origin master # esto sólo la primera vez, las siguientes será sólo:
git push
```

Ahora podriamos clonar de tal manera:

```bash
# Para clonar uno existente
git clone git@git.server.lol:ElBinario/eve.git
# Para crear un repositorio
git clone git@git.server.lol:drymer/testeando.git
```

Vale. Ahora le pondremos una interfaz web, que siempre queda más bonito. gitweb puede servir, pero para mi es demasiado sencillo, aunque tiene la ventaja de que anda en la paqueteria de debian, a diferencia de cgit. Pero bueno, cgit ofrece más cosas que gitweb, así que usaremos ese:

```bash
git clone git://git.zx2c4.com/cgit
cd cgit
git submodule init
git submodule update
make get-git
make
make install
```

A continuación la configuración. Primero el virtual host de Nginx, un script de manejo del cgi y luego el /etc/cgitrc, que es su fichero de configuración. Está todo en el [pad](https://pad.elbinario.net/p/servidor-git), para no alargar demasiado el articulo. No explicaré que hace cada linea del /etc/cgitrc por que seria muy largo. Para eso, mejor leer el [man](http://linux.die.net/man/5/cgitrc).

Hecho esto, reiniciamos nginx e iniciamos el script de inicio (muy cutre) de fcgiwrap y ya deberiamos ver nuestro repositorio git. Al final he puesto un pequeño cheatsheet de cómo administrar diferentes aspectos necesarios.

