+++
title = "Traducir archivos .po con emacs"
author = ["drymer"]
date = 2016-11-08T08:30:00+01:00
tags = ["emacs"]
draft = false
+++

Hace poco me he puesto a traducir un manual y me he encontrado con el tipo de ficheros .po, típicos de gettext. Como son bastante simples iba a empezar a editarlos sin más, pero me dio por mirar a ver si habia algún modo de emacs que lo facilitase y como es habitual, no defraudó.

En melpa tenemos el paquete **po-mode**. Lo instalamos y configuramos del siguiente modo:

```emacs-lisp
(use-package po-mode
  :ensure t
  :config
  ;; Fuente: https://www.emacswiki.org/emacs/PoMode
  (defun po-wrap ()
    "Filter current po-mode buffer through `msgcat' tool to wrap all lines."
    (interactive)
    (if (eq major-mode 'po-mode)
	(let ((tmp-file (make-temp-file "po-wrap."))
	      (tmp-buf (generate-new-buffer "*temp*")))
	  (unwind-protect
	      (progn
		(write-region (point-min) (point-max) tmp-file nil 1)
		(if (zerop
		     (call-process
		      "msgcat" nil tmp-buf t (shell-quote-argument tmp-file)))
		    (let ((saved (point))
			  (inhibit-read-only t))
		      (delete-region (point-min) (point-max))
		      (insert-buffer tmp-buf)
		      (goto-char (min saved (point-max))))
		  (with-current-buffer tmp-buf
		    (error (buffer-string)))))
	    (kill-buffer tmp-buf)
	    (delete-file tmp-file))))))
```

No tiene mucho misterio la configuración. La función `po-wrap` lo que hace es dividir las líneas hasta un máximo de 80, siguiendo las normas de estilo clásicas. No se ejecuta de forma interactiva, yo lo que hago es ejecutarla a mano una vez he terminado con un fichero.

Respecto al funcionamiento del modo, tenemos lo siguiente. Desde que se abre un fichero terminado en **.po** el modo se carga automáticamente. Lo primero que se puede ver es que todo el buffer está en modo lectura únicamente, no se puede escribir. Esto es debido a que está usando un modo interactivo similar al de la edición modal. La idea es irse moviendo a través de bloques editables rápidamente.

Si se presiona **h** o **?** nos mostrará la ayuda. Yo hasta el momento no he usado más que cuatro atajos de teclado, **n** y **p** para moverme adelante y atrás en los bloques que quiero editar, la tecla de **espacio** que lleva al último bloque para editar y la tecla de **enter**, que abre un nuevo buffer para editar el bloque en cuestión.

Hay varias más opciones, pero yo no las he usado de momento y ni siquiera se para que existen, si alguien sabe algo al respecto se agradecerá un comentario.
