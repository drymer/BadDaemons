+++
title = "i3wm + KDE"
author = ["drymer"]
date = 2015-05-30T19:15:00+02:00
draft = false
tags = ["i3wm", "kde", "elbinario"]
+++
La chorrada del día es la del título, usar [i3-wm](http://elbinario.net/2014/05/10/ensenando-i3wm-a-la-chica-rubia-del-asiento-trasero/) cómo window manager en vez de kwin. Kwin es lo que hace que kde sea tan bonito, así que avisadas estáis. Por que hacer esto, se podría preguntar alguien. La respuesta es sencilla, por que podemos. Por algo esta es una de las maravillosas cosas que implican el software libre. La cosa es bien sencilla. Partimos de la base que tenemos i3-wm y kde instalados. El segundo es fácil de instalar, si no tenéis el primero, podéis mirar en el link de más arriba.

Al lío. Buscamos un directorio llamado **windowmanagers**, más que nada por que variará el sitio según la distribución. En Slackware 14.1 está en ****/usr/share/apps/ksmserver/windowmanagers****. Para buscarlo, si tenemos slocate o mlocate instalado, sólo hay que ejecutar en terminal:

<pre>
locate windowmanagers
</pre>

Una vez tenemos la dirección, en ese directorio creamos un archivo llamado i3.desktop con el siguiente contenido:

<pre>
[Desktop Entry]
Encoding=UTF-8
Name=i3
Comment=Highly configurable framework window manager
Type=Application
Exec=i3
TryExec=i3
</pre>

Ahora, desde el escritorio kde, vamos a Preferencias del Sistema > Aplicaciones por Defecto > Gestor de Ventanas > Usar un gestor de ventanas diferente y escogemos i3 del desplegable. tendremos que salir de la sesión y entrar de nuevo para que los cambios sean activados. Pero, antes de hacer eso, podemos hacer unos cambios más para hacer que ambos escritorios se integren mejor. En ~/.i3/config podemos añadir lo siguiente, por ejemplo:

<pre>

for\_window [window\_role="Preferences"] floating enable
for\_window [window\_role="help-browser"] floating enable
for\_window [window\_role="pop-up"] floating enable
for\_window [window\_role="About"] floating enable
for\_window [class="(?i)plasma-desktop"] floating enable
for\_window [class="(?i)klipper"] floating enable

</pre>

Esto hace que ciertas ventanas se pongan en modo floating de manera automática, haciendo así que sean más bonitas. También es recomendable comentar toda la sección ****bar****, ya que sino tendremos dos barras, la de kde y la de i3. Ponerle cómo valor ****hide**** no vale, eso sólo la esconde hasta que se presiona la tecla META. Una vez hecho esto, podemos salir de la sesión y ver nuestra gilipollez del día.

