+++
title = "Emacs y org-mode"
author = ["drymer"]
date = 2015-04-03T19:15:00+02:00
draft = false
tags = ["emacs", "org-mode", "elbinario"]
+++

# Que te aporta usar org-mode?

Últimamente veo en algunos sitios que se habla de emacs, cosa que siempre es interesante. Emacs no es un editor de textos, cómo se dice, equivocamente, al compararlo con vi?m, nano o incluso gedit. Para mi emacs es:

- IDE de desarrollo, para programar y tal
- Editor de texto sencillo. De hecho, me he quitado nano y lo he puesto como alias a emacsclient
- org-mode
  - Procesador de textos
  - Publicación de esos textos
  - Listas, muchas listas

Y el último punto es el que explicaré a continuación. Comentaré <span class="underline">mi uso</span> de esta herramienta, puede tener más. Por algo este es el **editor hackeable** (el de verdad).

<a id="orgabd87ac"></a>

## Procesador de textos

Cómo procesador de textos es lo mejor que hay. Usa un formato tipo markdown, aunque ligeramente distinto, que puede ser exportado a una cantidad muy grande de formatos. Veamos un ejemplo. La siguiente imagen es este articulo en el punto en el que está ahora.

Para exportarlo a otro formato ejecutamos C-c C-e. Ahí nos abrirá un buffer con algo parecido a lo siguiente:

Le damos al HTML, por ejemplo, que serviria para montar una web sencilla. Se puede visualizar aquí:

<./listings/org-mode.html>

Sinsillo y para toda la familia.

<a id="org8d4fa20"></a>

## Publicación de estos textos

Estos modos suelen ser menores, por debajo de org-mode. Los que he usado son los de [org2nikola](https://daemons.it/posts/nikola---emacs-gestionar-un-blog-de-contenido-estático/) y el [org2blog/wp](https://daemons.it/posts/publicando-en-wordpress-desde-emacs-24.html), ambos muy interesantes. El primero para nikola y el segundo para wordpress. Y en la [wiki](http://orgmode.org/worg/org-blog-wiki.html) de emacs se pueden ver otros modos, en los cuales se pueden usar otros generadores de web estáticos y/o git.

<a id="org492e3f7"></a>

## Listas, muchas listas

Y este es su fin principal, hacer listas.
