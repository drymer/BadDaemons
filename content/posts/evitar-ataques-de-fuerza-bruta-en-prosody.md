+++
title = "Evitar ataques de fuerza bruta en Prosody"
author = ["drymer"]
date = 2016-09-30T08:30:00+02:00
tags = ["xmpp", "sysadmin"]
draft = false
+++

Este es un tema muy obvio, pero la verdad es que no había caído en investigarlo hasta hace poco. Me fié en como intentar hacer seguras las conexiones SSL con su FPS y sus cosas, y no pensé en que alguien me puede robar la cuenta. No me suele preocupar este tema, mis contraseñas suelen pasar de los 20 caracteres. Pero aún así, si no hay ningún tipo de control en los intentos y los fallos al intentar loguearse, un proceso que se podría alargar años con las medidas adecuadas, podría tardar un mes o dos solamente. Así que a ponerle remedio.

Ya hablé del gran servidor que es Prosody en el articulo enfocado a habilitarlo para el [uso móvil](https://daemons.cf/posts/xmpp-en-mviles-mentiras-y-cmo-mejorar-tu-servidor/), por si alguien no lo ha leído. Ya se vio que instalar nuevos módulos es absurdamente fácil. Ahora veremos como configurar el módulo Limit Auth. Este módulo limita las autenticaciones erróneas por IP que se pueden hacer. No banea de ningún modo, pero impone un tiempo de espera cada X intentos fallidos. Veamos como configurarlo.

Si aún no hemos clonado el repositorio de los módulos de la comunidad de prosody, se hace del siguiente modo:

```sh
su
cd
hg clone https://hg.prosody.im/prosody-modules
```

Copiamos el módulo a configurar:

```sh
cp -r ~/prosody-modules/mod_limit_auth/ /usr/lib/prosody/modules/
```

Editamos el archivo `/etc/prosody/prosody.cfg.lua`:

```lua
modules_enabled = {
-- otros modulos
"limit_auth";
-- más modulos
}

limit_auth_period = 30

limit_auth_max = 5
```

Puesto así y después de reiniciar, haremos que cada cinco intentos fallidos tengan que pasar 30 segundos hasta el siguiente intento de autenticación. No es la panacea, pero este tipo de medidas enlentecen muchísimo los ataques de fuerza bruta. Podemos complicarles la vida a los crackers un poco más, si queremos. Hay otro módulo llamado Log Auth que permite integrar Prosody con `fail2ban`, un gran programa para el control de acceso. Típicamente se usa con `ssh`, aunque se puede usar con mucho más, coḿo veremos.

El módulo que instalaremos lo que hace es, básicamente, es activar el logueo de IP únicamente cuando se da un fallo de autenticación. Recordemos que Prosody, a menos que se tenga el log en modo debug, no guarda IP. No requiere de configuración, solo hay que copiarlo en el directorio y activarlo desde la configuración de Prosody:

```lua
modules_enabled = {
-- otros modulos
"log_auth";
-- más modulos
}
```

Supondremos que está instalado y configurado, si no es el caso, recomiendo hacerlo siguiendo [este tutorial](http://www.fail2ban.org/wiki/index.php/HOWTO_fail2ban_spanish). Ahora crearemos el filtro en el archivo `/etc/fail2ban/filter.d/prosody-auth.conf`:

```sh
# /etc/fail2ban/filter.d/prosody-auth.conf
# Fail2Ban configuration file for prosody authentication
[Definition]
failregex = Failed authentication attempt \(not-authorized\) from IP: <HOST>
ignoreregex =
```

Ahora lo activamos en la configuración de `fail2ban` en `/etc/fail2ban/ail.conf`:

```ini
[prosody]
enabled = true
port    = 5222
filter  = prosody-auth
logpath = /var/log/prosody/prosody*.log
maxretry = 20
```

He puesto 20 por que por otro lado tenemos el módulo anterior y con un ataque de fuerza bruta en menos de 20 intentos no sacan la contraseña (y si lo hacen, te mereces lo que pase). Así que en aras de la comodidad, se queda en un límite alto. Y sólo queda reiniciar y a disfrutar de un pelín más de seguridad.

Más información en las páginas de los módulos:

-   [https://modules.prosody.im/mod\_limit\_auth.html](https://modules.prosody.im/mod_limit_auth.html)

-   [https://modules.prosody.im/mod\_log\_auth.html](https://modules.prosody.im/mod_log_auth.html)
