+++
title = "OnionScan - Escaneando onions"
author = ["drymer"]
date = 2016-04-13T08:27:00+02:00
tags = ["tor"]
draft = false
+++

Ya tocaba dejar un poco emacs. Hoy veremos [OnionScan](https://github.com/s-rah/onionscan). Este es un programa que, cómo su nombre indica, escanea onions (servicios ocultos de Tor). Tiene una desventaja, y es que está hecho en go-lang, el lenguaje de programación de Google. Aún sin gustarme, habitualmente, los productos de Google, no es ese el motivo de ser una desventaja, es simplemente que habrá que instalarlo únicamente para este programa.

Pero bueno, para eso están los binarios compilados, para la puta gente vaga. Así que al lío. El proceso consistirá en descargar `go`, descomprimir el archivo, hacer algún apaño usando variables de entorno para que encuentre las librerías y descargar y usar `OnionScan`.

```sh
# Crear un directorio genérico y entrar
mkdir ~/Instalados
cd ~/Instalados

# Esta es la arquitectura que uso yo, se puede escoger en https://golang.org/dl/
wget https://storage.googleapis.com/golang/go1.6.1.linux-amd64.tar.gz

# unp es un programa que gestiona la descompresión de archivos, sin importar el tipo
unp -d go1.6.1.linux-amd64.tar.gz

# Según se use Debian, Fedora, Slackware o lo que sea
apt-get install libexif-dev
dnf install libexif-devel
slackpkg search libexif

# Definir entornos de variable necesarios para go
# Tener en cuenta que son para su uso temporal, no deberían tomarse cómo referencia si se programa
# seriamente con este lenguaje de programación
export GOROOT="$HOME/Instalados/go/"
export GOPATH="$GOROOT/bin/"

# Instalar onionscan
go/bin/go get github.com/s-rah/onionscan
```

Y ya estará instalado el binario de `onionscan` en `~/go/bin/bin/onionscan`. Para usarlo, se puede ejecutar tal que así. La dirección onion es la de este mismo blog.

```sh
torify go/bin/bin/onionscan daemon4idu2oig6.onion
```

Esto devuelve parecido a este:

```text
2016/04/13 10:18:20 Starting Scan of daemon4idu2oig6.onion
2016/04/13 10:18:20 This might take a few minutes..

--------------- OnionScan Report ---------------
High Risk Issues: 0
Medium Risk Issues: 0
Low Risk Issues: 0
```

Un poco pobre. Si se le pasa el argumento `-verbose`, en cambio:

```text
2016/04/13 10:19:04 Starting Scan of daemon4idu2oig6.onion
2016/04/13 10:19:04 This might take a few minutes..

2016/04/13 10:19:04 Checking daemon4idu2oig6.onion http(80)
2016/04/13 10:19:04 Found potential service on http(80)
2016/04/13 10:19:05 Attempting to Derive Server Type from Headers..
2016/04/13 10:19:05     Server Version: nginx
2016/04/13 10:19:05     Apache mod_status Not Exposed...Good!
2016/04/13 10:19:06 Scanning daemon4idu2oig6.onion/
2016/04/13 10:19:06     Page daemon4idu2oig6.onion/ is Accessible
2016/04/13 10:19:06 Found Related URL https://gnusocial.net/maxxcan
2016/04/13 10:19:06 Found Related URL https://wiki.cyanogenmod.org/w/Devices
2016/04/13 10:19:06 Found Related URL http://acentoveintiuno.com/?Facebook-puede-registrar-lo-que
2016/04/13 10:19:06 Found Related URL http://elbinario.net/2014/05/06/500-licencias-de-microsoft-office-para-adif/
2016/04/13 10:19:06 Found Related URL http://creativecommons.org/licenses/by/4.0/
2016/04/13 10:19:06 Found Related URL https://i.creativecommons.org/l/by/4.0/88x31.png
2016/04/13 10:19:06 Found Related URL http://creativecommons.org/licenses/by/4.0/
2016/04/13 10:19:06     Scanning for Images
2016/04/13 10:19:06      Found image wp-content//diagram.png
2016/04/13 10:19:24      Found image wp-content//emacs-theme-editar.png
2016/04/13 10:19:25      Found image https://i.creativecommons.org/l/by/4.0/88x31.png
2016/04/13 10:19:26 Directory /style either doesn't exist or is not readable
2016/04/13 10:19:26 Directory /styles either doesn't exist or is not readable
2016/04/13 10:19:26 Directory /css either doesn't exist or is not readable
2016/04/13 10:19:26 Directory /uploads either doesn't exist or is not readable
2016/04/13 10:19:27 Directory /images either doesn't exist or is not readable
2016/04/13 10:19:27 Directory /img either doesn't exist or is not readable
2016/04/13 10:19:27 Directory /static either doesn't exist or is not readable
2016/04/13 10:19:27 Directory /wp-content/uploads either doesn't exist or is not readable
2016/04/13 10:19:27 Directory /products either doesn't exist or is not readable
2016/04/13 10:19:28 Directory /products/cat either doesn't exist or is not readable
2016/04/13 10:19:28
2016/04/13 10:19:28 Checking daemon4idu2oig6.onion ssh(22)
2016/04/13 10:19:28 Checking daemon4idu2oig6.onion ricochet(9878)
2016/04/13 10:19:28 Failed to connect to service on port 9878
--------------- OnionScan Report ---------------
High Risk Issues: 0
Medium Risk Issues: 0
Low Risk Issues: 0
```

Parece que tiene hardcodeada la búsqueda de distintos directorios, algunos de `wordpress`, otros de `drupal`. Bueno, es una herramienta curiosa. De momento muy útil no es, no dice mucho. Para haber sido creado hace tres días, no está nada mal.
