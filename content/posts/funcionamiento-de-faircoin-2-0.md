+++
title = "Funcionamiento de Faircoin 2.0"
author = ["drymer"]
date = 2016-05-03T14:08:00+02:00
tags = ["faircoin"]
draft = false
+++

Lo siguiente son unos apuntes/resumen/traducción del [paper](https://chain.fair-coin.org/download/FairCoin2-Draft.pdf) de la nueva versión de FairCoin, la 2.

Hasta ahora se funciona con PoS, el cual no es justo, ya que el poder lo tiene quien tiene poder de computación. En esta nueva versión se pasa a PoC (Proof of Cooperation). Esto quiere decir que las usuarias no minarán más. Los encargados de generar bloques serán los [CVN](https://fair.coop/node-certification-procedure/) (Certified Validation Nodes). En el futuro, estos nodos funcionaran con un método de reputación.

No hay recompensa en la creación de bloques, de modo que la cantidad de dinero no cambiará con el tiempo y quedará fía en el momento de migración a FC2&nbsp;[^fn:1]. Los beneficios de las transferencias irán a las operadoras de un CNV, cómo recompensa.

Este valor, entre otros, será ajustable de manera dinámica (sin necesidad de sacar nueva versión de la cartera) por un consenso democrático comunitario.


## Características de la creación de bloques {#características-de-la-creación-de-bloques}

-   Se crean cada 3 minutos
-   Se decide que CVN lo crea en función del tiempo que ha pasado desde su última creación de un bloque
    -   E: Si CVN A lo creó hace 50 minutos y CVN B lo creó hace 55 minutos, el siguiente bloque lo creará el CVN B
-   Cuando se une un nuevo CVN, creará el siguiente bloque
-   Entre la creación de dos bloques, sólo un nodo puede unirse


## Fases De la creación de bloques {#fases-de-la-creación-de-bloques}

-   Acumulación de transacciones: Los nodos transfieren las transacciones que reciben a  todos los nodos a los que estén conectados. Dura 170 segundos al menos. Si no hay transacciones pendientes, habrá que esperar a que las haya. Dicho de otro modo, esta fase sólo termina cuando hay al menos una transacción en la red.

-   Anunció de tiempo desde la última creación de un bloque: Cada CVN determina cuanto hace y lo anuncia. La formula para calcular este es:

    \begin{equation}
    espera = 10 - (\frac{10 \* otw}{tn})
    \end{equation}

Siendo:

-   **otw** el tiempo desde la última creación de un bloque (own time-weight)
-   **tn** el número total de nodos activos
-   **10** la constante de 10 segundos, que es lo que dura la fase

Esto reducirá la cantidad de anuncios, por que cada nodo que recibá un anuncio que tenga prioridad, no lanzará el suyo.

-   Elección del creador del bloque: Cuando se han enviado todos los anuncios, los CVN escogerán el nodo que hace más tiempo que no crea un bloque, lo firmaran el anuncio con su voto a este y lo enviaran por la red. Esta fase no tiene un tiempo máximo definido. Se detiene cuando un nodo sale elegido por al menos el 90% de todos los nodos activos. Entonces, el elegido crea el bloque en el que mete todas las transacciones pendientes unto a los votos firmados por los otros nodos en el que le eligen para crear el bloque cómo prueba.


## Nodos de Validación de Certificados {#nodos-de-validación-de-certificados}

Su finalidad es securizar la red validando todas las transacciones que han sido enviadas por la red y meterlas en las cadenas de bloques.

Requisitos para montar un CVN:

-   Requisitos técnicos
    -   El sistema debe estar conectado a internet 24/7 y debe tener el puerto 46392 abierto.
    -   El sistema debe usar servidores NTP públicos, preferiblemente pool.ntp.org.
    -   La entidad debe tener una cuenta en la web de FairCoop.
    -   La cartera debe estar configurada con un certificado de la FairCoop.

Por qué hay que certificar los nodos? Por que un procedimiento de certificación adecuado asegura que un alto porcentaje de los CVN son **honestos**. Hasta el momento, el autor no ve una manera fácil de asegurar que cada nodo sólo tiene una identidad sin establecer un sistema de reputación.

[^fn:1]: Antes decía lo siguiente, pero lo cambié ya que es una interpretación personal (probablemente errónea, además): "de manera que el precio de la moneda no estará definido por la especulación típica de bitcoin, ya que esta no podrá existir."
