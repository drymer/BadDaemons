+++
title = "Un par de funciones útiles de emacs"
author = ["drymer"]
date = 2016-05-01T01:35:00+02:00
tags = ["emacs", "snippets", "orgmode"]
draft = false
+++

No tengo ni idea de elisp, pero aún así me las he apañado para hacer un par de snippets medio útiles para un par de cosas que me hacían falta. Las he hecho basándome en dos articulos, [este](http://pragmaticemacs.com/emacs/wrap-text-in-an-org-mode-block/) y [este](http://wenshanren.org/?p=334). Seguro que se pueden optimizar.

La primera de ellas sirve para insertar cabeceras típicas en `org-mode`. Le he puesto sólo tres tipos, ya que son los que uso para exportar. Después de evaluar el código y ejecutar la función, mediante `ido` aparecerá una pregunta que nos dará a escoger entre los distintos tipos. Sólo hay que escogerlo y se insertará (en la posición del buffer en la que se esté) la cabecera. El código es el siguiente:

```emacs-lisp
(defun org-mode-insert-header (language)
"Make a template at point."
(interactive
   (let ((languages '("Generic" "HTML" "LaTeX" )))
     (list (ido-completing-read "To which export: " languages))
     )
   )

(when (string= language "Generic")
  (progn
    (insert (format "#+TITLE:\n"))
    (insert (format "#+AUTHOR:\n"))
    (insert (format "#+LANGUAGE: es \n"))
    (insert (format "#+OPTIONS: toc:1\n"))
    (insert (format "#+TOC: headlines 3\n"))
    (insert (format "#+STARTUP: indent\n\n"))
      )
  )

(when (string= language "HTML")
  (progn
    (insert (format "#+TITLE:\n"))
    (insert (format "#+AUTHOR:\n"))
    (insert (format "#+LANGUAGE: ca\n"))
    (insert (format "#+OPTIONS:   toc:1\n"))
    (insert (format "#+TOC: headlines 3\n"))
    (insert (format (concat "#+SETUPFILE: " user-emacs-directory "/css/org-html-themes/setup/theme-readtheorg.setup\n\n")))
    )
  )

(when (string= language "LaTeX")
  (progn
    (insert (format "#+Title:\n"))
    (insert (format "#+Author:\n"))
    (insert (format "#+LANGUAGE: es\n"))
    (insert (format "#+LATEX_CLASS: article\n"))
    (insert (format "#+LATEX_CLASS_OPTIONS: [spanish,a4paper]\n"))
    (insert (format "#+LATEX_HEADER: \\usepackage{color}\n"))
    (insert (format "#+LATEX_HEADER: \\usepackage[spanish]{babel}\n\n"))

    )
  )
)

```

Lo suyo es modificar el código para que se adecue a las necesidades de quien lo usa.

El siguiente snippet de código sirve para insertar bloques de código. Se puede usar tanto de manera normal cómo al resaltar un trozo de código o letras. Es un poco dificil de explicar, por lo que he hecho un gif para mostrarlo.

{{< figure src="/img/insert-src.gif" >}}

El código es:

```emacs-lisp
(defun org-src-insert (choice)
"Insert src code blocks."
(interactive
   (if (org-at-table-p)
       (call-interactively 'org-table-rotate-recalc-marks)
     (let ((choices '("emacs-lisp" "python" "shell" "css" "ledger" "latex" "lisp" "sqlite")))
       (list (ido-completing-read "Source code type: " choices)))))

(cond
 ((region-active-p)
  (let ((start (region-beginning))
	  (end (region-end)))
    (progn
	(goto-char end)
	(insert "\n#+END_SRC\n")
	(goto-char start)
	(insert (format "#+BEGIN_SRC %s\n" choice)))
    )
  )

 (t
  (insert (format "#+BEGIN_SRC %s\n" choice))
  (save-excursion (insert "\n#+END_SRC")))))

```
