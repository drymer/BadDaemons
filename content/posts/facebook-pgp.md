+++
title = "Facebook se lava la cara"
author = ["drymer"]
date = 2015-06-02T19:15:00+02:00
draft = false
tags = ["privacidad", "elbinario"]
+++

Que fantabuloso. La noticia (que por cierto he leído en el [blog de Chema Alonso](http://www.elladodelmal.com/2015/06/facebook-ahora-te-permite-cifrar-los.html)) de hoy es que implementarán cifrado end-to-end con PGP en todas las comunicaciones de las usuarias que quieran.

Yo, que soy desconfiado (mire usted por dónde), he pensado que harían cómo los de Whatsapp, que anuncian a bombo y platillo que usarán cifrado end-to-end implementando Textsecure con el propio Whatsapp. Esto en teoria está bien. Problema? Dónde están las llaves? Pues no se sabe, es lo que tiene el software privativo. Todo apunta a que las llaves las manejan desde el servidor principal de Whatsapp. Otra vez, problema? Que no es seguro, claro. Nada nos asegura que ante una orden judicial tendrán que dar las llaves. Por no decir que les puedan entrar y acceder a todas las llaves.

En fin, lo que decia es que pensaba que Facebook, que al fin y al cabo es la dueña de Whatsapp, harian lo mismo. Pero resulta que no, que dejan subir a cada persona su propia clave. Esto seria muy interesante y útil de no ser por un pequeño fallo. Facebook [registra cualquier texto que escribas](http://acentoveintiuno.com/?Facebook-puede-registrar-lo-que), lo envíes o no. Que sentido tiene cifrar un mensaje, si ya lo tienen aún sin enviar? Más bien ninguno. Sólo una nueva demostración de Facebook limpiándose la cara con el cubo de la fregona. Nada ha cambiado.

PD: Curiosamente, al buscar (poco exhaustivamente y durante unos cinco minutos) algúna fuente más conocida que habláse de que Facebook registra lo que escribes, sólo he encontrado ese.

