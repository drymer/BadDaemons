+++
title = "Mejoras en el blog: Hugo, ox-hugo y drone"
author = ["drymer"]
date = 2018-08-25T21:11:00+02:00
tags = ["orgmode", "hugo", "ci", "drone"]
draft = false
+++

Hace un tiempo que no actualizo el blog. Pensaba que es por que ando a otras cosas, pero reflexionando me di cuenta de que me daba pereza. No por escribir en general, sino por que el flujo de publicación que tenia en mi blog no me gustaba. Debido a que odio todo lo que tenga que ver con diseño, en vez de hacer las modificaciones del blog en la plantilla, me dediqué a hacer scripts que modificasen el HTML generado de forma dinámica, lo que iba bien al principio, pero luego tardaba hasta medio minuto en construir. Además, al ser tan personalizado, tenia que hacer las cosas de una forma muy particular o me encontraba errores después. En fin, un dolor. Por eso, por que me cansé del tema y por que echaba en falta alguna funcionalidades, me decidí a migrar de nikola a hugo.

Y aquí estamos. El flujo que sigo ahora es el siguiente:

{{< figure src="/img/blog-workflow.png" >}}

El repositorio git del blog es público, y se puede ver [aquí](https://git.daemons.it/drymer/BadDaemons). No voy a explicar como he hecho nada, ya que en todos los casos he seguido la documentación oficial de cada proyecto. Pero por si alguien quiere empezar a hacer lo mismo, propongo que se siga el orden siguiente:

-   [Empezar con hugo](https://gohugo.io/getting-started/quick-start/)
-   [Instalar y configurar ox-hugo](https://ox-hugo.scripter.co/doc/usage/)
-   [Empezar con git](https://victorhckinthefreeworld.com/2016/05/19/curso-interactivo-de-git-en-15-minutos/)
-   [Empezar con Integración Continua](https://labrecha.digital/blog/integracion-continua-drone/)
