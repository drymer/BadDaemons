+++
title = "Como gestionar las configuraciones y programas de tu ordenador con Ansible"
author = ["drymer"]
date = 2018-01-08T08:30:00+01:00
tags = ["ansible"]
draft = false
+++

Hace relativamente poco abandoné Slackware por Debian por dos motivos. Uno fue que mi ordenador no tenia mucha potencia y me cansé de compilar y el segundo que quería automatizar la gestión de todo lo que tuviese que ver con mi ordenador, tanto la instalación de paquetes del sistema operativo, paquetes pip y paquetes compilados, como la configuración de ciertos programas. Así que me puse a ello y creé unos cuantos roles. Algunos se pueden reutilizar, los veremos a continuación mientras explico por encima como funciona Ansible y que tiene de genial.

La lista de roles que he hecho y que pueden utilizarse son los siguientes:

-   [virtualenvwrapper](https://git.daemons.it/ansible-roles/virtualenvwrapper/commits/master)

-   [syncthing](https://git.daemons.it/ansible-roles/syncthing)

-   [emacs](https://git.daemons.it/ansible-roles/emacs)

-   [virtualbox](https://git.daemons.it/ansible-roles/virtualbox)

-   [qutebrowser](https://git.daemons.it/ansible-roles/qutebrowser)

-   [termite](https://git.daemons.it/ansible-roles/termite)

-   [slim](https://git.daemons.it/ansible-roles/slim)

-   [i3-gaps](https://git.daemons.it/ansible-roles/i3-gaps)

-   [fonts](https://git.daemons.it/ansible-roles/fonts)

-   [docker](https://git.daemons.it/ansible-roles/docker)

Al crear roles hay que intentar seguir la filosofía de roles atómicos, que significa crear roles con una función muy específica de modo que sean muy manejables, fáciles de mantener y de reutilizar. Veamos la estructura de uno de ellos, por ejemplo el rol de instalación de [slim](https://wiki.archlinux.org/index.php/SLiM_(Español)):

```text
~/Documentos/ansible/roles/desktop/slim
├── defaults
│   └── main.yml
├── files
│   ├── slim.service
│   └── xinitrc
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
└── tasks
└── main.yml
```

Están los directorios:

-   **defaults**: Para definición de variables.
-   **files**: Para ficheros externos al rol.
-   **handlers**: Para el maneo de servicios.
-   **meta**: Para manear metadatos del rol.
-   **tasks**: Para dotar al rol de funcionalidad.

En **defaults** se define la variable `session_manager`, que define un gestor de sesiones que será desactivado. En **files** está el servicio de systemd de `slim` y el fichero `xinitrc`, en el que se arranca i3-wm. En **handlers** se deshabilita el gestor de sesiones de `session_manager` y se habilita `slim`. En **meta** se concretan los metadatos del rol, como ya he comentado. En la práctica no es demasiado relevante, pero cuando se instala un rol con `ansible-galaxy` se queja si no existe (ya comentaremos este programa). Y por último, las tareas:

```yaml
---
# tasks file for slim
- name: Install slim in debian or derivates
become: yes
apt:
name: slim
state: present
update_cache: yes

- name: Modify /etc/slim.conf
become: yes
lineinfile:
path: /etc/slim.conf
regexp: "{{ item.regexp }}"
line: "{{ item.line }}"
with_items:
- { regexp: "#? ?numlock", line: "numlock on" }
- { regexp: "#? ?hidecursor", line: "hidecursor true" }
- { regexp: "^login_cmd", line: "login_cmd exec /bin/sh - ~/.xinitrc" }

- name: Copy slim systemd service
become: yes
copy:
src: "{{ role_path }}/files/slim.service"
dest: /lib/systemd/system/slim.service
force: yes
when:
ansible_connection != 'docker'
or
force_docker == 'yes'
notify:
- disable session manager
- enable slim
```

Ansible usa el formato yaml para sus ficheros. Esto mola mucho por que es muy legible. Sin saber nada de Ansible se puede entender bastante lo que hace. Analicemos la primera tarea:

-   `name`: Es el nombre de la tarea, para poder identificarla cuando se ejecute el rol.

-   `become`: Se usa para escalar privilegios. Por defecto uso `sudo`, pero puede usarse `su`.

-   `apt`: Es el módulo de apt. Los módulos son el fuerte de Ansible, permiten ejecutar tareas concretas (si están bien hechos) de forma idempotente. Esto significa que una tarea puede ejecutarse **n** veces pero solo provocará cambios si es necesario. En el caso de este módulo, solo instala un paquete si este no está instalado. El equivalente de esta tarea seria el siguiente:

```bash
sudo apt update
sudo apt install slim -y
```

La siguiente tarea usa el módulo `lineinfile`, que sirve para modificar ficheros. Se le pasa la variable `path` para determinar la ruta del fichero a modificar, la variable `regexp` para la expresión regular que queremos cambiar y la variable `line` para lo que se quiere que substituya lo que encaje con la expresión regular. Además, aquí se puede ver como se usa un bucle en Ansible.

En la última tarea vemos el módulo `copy`, que sirve para copiar un fichero local con la variable `src` al destino de la variable `dest`. Y además podemos ver la palabra clave `when`, que sirve para gestionar condicionales. En este caso, se copia ese fichero cuando la variable `ansible_connection` es diferente a **docker** o la variable `force_docker` es `yes`.

Hasta aquí hemos visto lo que es un rol de Ansible. Al principio he puesto la lista de los que pueden ser relevantes para otras personas. Para utilizar alguno de ellos, hay que crear un playbook. Si solo quisiésemos usar el rol de `slim`, seria así:

```yaml
- hosts: localhost
connection: local
vars:
install_dir: ~/Instalados/
roles:
- slim
```

La palabra clave `hosts` apunta al host **localhost**, valga la redundancia. `connection` por defecto usa ssh, pero como es una conexión al propio ordenador, es mejor usar el valor `local`, que es más rápido. Con `vars` definimos variables a usar en los roles y `roles` para los roles. Como dije, lo bueno que tiene el lenguaje de Ansible es que es muy claro si sabes inglés.

Ahora tocará ver como gestionar las dependencias de roles. Se podría tirar de submódulos de git, pero por suerte Ansible provee de una herramienta para esto, `ansible-galaxy`. Por defecto instala del [hub de ansible-galaxy](https://galaxy.ansible.com/explore), un repositorio centralizado de roles. Por lo tanto no tengo mayor interés en usarlo. Por ello tengo la costumbre de usar `ansible-galaxy` con repositorios git sin más. Para instalar roles, hay que crear un fichero llamado `requirements.txt` y llenarlo de roles en un formato como este:

```text
- src: git+https://git.daemons.it/ansible-roles/slim
```

Una vez creado el fichero, solo hay que ejecutar `ansible-galaxy install -r requirements.txt -p roles`. Y a reusar roles a dos manos.

Para ver un ejemplo más completo de como hacerse un playbook que gestione todos los roles que hagan falta, se puede ver [mi repositorio principal de dotfiles](https://git.daemons.it/drymer/dotfiles).

Por último solo queda hablar de la documentación. En la [documentación oficial](http://docs.ansible.com/ansible/latest/index.html) de Ansible se puede encontrar mucha información relevante, pero la herramienta más útil para mi es `ansible-doc`. Viene por defecto al instalar Ansible y permite ver la documentación completa de un módulo concreto.

Lyz ha dado varias charlas en La Brecha Digital sobre Ansible y tiene los índices en el [repositorio de charlas de La Brecha Digital](https://git.digitales.cslabrecha.org/La_Brecha_Digital/charlas-y-talleres/src/master/20171108-basic-ansible-Lyz), por si alguien le quiere echar un ojo. Cualquier duda sólo hay que preguntarla ya sea en los comentarios o en la sala XMPP de [La Brecha Digital](https://www.suchat.org/jappix/?r=brechadigital_gestion@sala.chat.cslabrecha.org&n=daemonita).
