+++
title = "Golden-ratio - redimensión automática de ventanas"
author = ["drymer"]
date = 2016-11-01T08:30:00+01:00
tags = ["emacs"]
draft = false
+++

Cuando se tienen varias ventanas se suele dar el problema de que tienes que ir-las redimensionando a mano. Yo ni siquiera he encontrado la manera de hacerlo cómodamente con el teclado, por lo que encima tengo que tirar del ratón, lo cual es bastante incómodo. Con este paquete, no hace falta. Se puede ver su comportamiento en [este gif](https://camo.githubusercontent.com/26b1ac5fec67a2c557cfbe87382a0134d3443fd0/68747470733a2f2f7261772e6769746875622e636f6d2f726f6d616e2f676f6c64656e2d726174696f2e656c2f6173736574732f676f6c64656e5f726174696f5f656c2e676966).

**Actualización**: El código comentado es el original, el cambiado es el de la variable `golden-ratio-exclude-buffer-regexp`. Al principio usaba esa función por que pensaba que no habia nada nativo en el modo, pero si que lo hay. Se puede usar esta variable para expresiones regulares o la variable `golden-ratio-exclude-buffer-name` para nombres completos de los buffers.

La instalación y configuración necesaria es la siguiente:

```emacs-lisp
(use-package golden-ratio
  :config
  (defun init/no-golden-ratio-for-buffers (bufname)
    "Disable golden-ratio if BUFNAME is the name of a visible buffer."
    (and (get-buffer bufname) (get-buffer-window bufname 'visible)))
  (defun init/no-golden-ratio ()
    "Disable golden-ratio for guide-key popwin buffer."
    (or (init/no-golden-ratio-for-buffers " *undo-tree*")
        (init/no-golden-ratio-for-buffers " *undo-tree Diff*")
        ))

  (add-to-list 'golden-ratio-inhibit-functions
               'init/no-golden-ratio)

  (golden-ratio-mode t)
  (setq golden-ratio-exclude-buffer-regexp '("undo-tree" "help" "diff" "calendar" "messages" "Org Agenda" "Agenda Commands" "Org Select" "magit:" "Calendar"))
  (setq golden-ratio-auto-scale t)
  :diminish golden-ratio-mode
  :ensure t)
```

La función `golden-ratio-mode` es la que activa el modo. Las dos funciones posteriores y el add-to-list lo que hace es definir en que buffers no usaremos el golden-ratio. Yo tengo puesto, de momento un par de buffers de undo-tree. Seguramente en los próximos días que lo use más lo termine actualizando. Para añadir más buffers solo hay que añadir más lineas con el nombre del buffer.
