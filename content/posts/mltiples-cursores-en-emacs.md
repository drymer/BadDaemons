+++
title = "Múltiples cursores en emacs"
author = ["drymer"]
date = 2017-12-11T08:30:00+01:00
tags = ["emacs"]
draft = false
+++

Este es un paquete al que cuesta cogerle el punto. Pisa un poco las funcionalidades de las macros, es un poco especialito y hay que tener un poco de imaginación al usarlo, pero una vez le pillas el truco es muy útil.

El paquete que muestra el siguiente gif es de `multiple-cursors`, y hace exactamente esto, abrir múltiples cursores en emacs. Lo he estado usando últimamente que he tenido que he estado usando [Terraform](https://www.terraform.io/) en el trabajo, y se presta al uso de este paquete. Veamos un ejemplo:

{{< figure src="/img/multiple-cursors.gif" >}}

La primera función es `mc/mark-next-like-this`, que crea un cursor en el siguiente elemento que es igual al que se está seleccionando. Tiene su equivalente `mc/mark-previous-like-this` para lo mismo pero para atrás. La siguiente función es `mc/mark-next-lines` que marca la siguiente linea sin más. Igual que la anterior, tiene su equivalente inverso, `mc/mark-previous-lines`. Y por último, el más útil, como todos los dwim, Do What I Mean. Se comporta de forma distinta según desde dónde se ejecute. Si se selecciona una región, introduces una cadena de carácteres y inserta cursores en esas regiones. En otra región hará otra cosa.

En la fabulosa página de [Emacs Rocks](http://emacsrocks.com) hay un screencast sobre [este paquete](http://emacsrocks.com/e13.html). Son solo tres minutos y muestra otro ejemplo de como usar este modo.
