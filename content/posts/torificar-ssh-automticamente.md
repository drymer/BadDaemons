+++
title = "Torificar ssh automáticamente"
author = ["drymer"]
date = 2018-02-05T21:22:00+01:00
tags = ["ideas"]
draft = false
+++

El archivo de configuración del cliente de SSH permite cosas muy chulas. Las más conocidas son las de asociar una clave ssh y un usuario a un host. Por si alguien no lo conoce, en el fichero `~/.ssh/config` se pone:

```text
host daemons
 user drymer
 IdentityFile ~/.ssh/drymer-daemons
 host daemons.it
```

Al ejecutar `ssh daemons` iniciará sesión en el servidor definido en `host` con el usuario definido en `drymer` usando la clave definida en `~/.ssh/drymer-daemons`.

Pero podemos ir más allá y dear de confiar en acordarnos de meter un `torify` antes de la orden anterior modificando la orden que usa el propio cliente de ssh. Para ello, podemos usar el parámetro `ProxyCommand`. Se podría usar con `proxychains`, `torify` y `torsocks`, por ejemplo, pero a mi me lo enseñaron con `socat`. Así que el anterior ejemplo lo cambiaríamos a esto:

```text
host daemons
 user drymer
 IdentityFile ~/.ssh/drymer-daemons
 ProxyCommand socat - SOCKS4A:localhost:daemon4idu2oig6.onion:22,socksport=9050
```

Este último ejemplo se lo agradezco a [Andreas](https://gnusocial.net/andreastsu), que me mostró que también podemos hacer que SSH por defecto torifique todos los hosts con el TLD `onion`:

```text
Host *.onion
ProxyCommand socat STDIO SOCKS4A:127.0.0.1:%h:%p,socksport=9050
```
