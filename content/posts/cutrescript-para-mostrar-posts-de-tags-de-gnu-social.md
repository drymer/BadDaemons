+++
title = "Cutrescript para mostrar posts de tags de GNU social"
author = ["drymer"]
date = 2017-10-29T17:51:00+01:00
tags = ["python", "gnusocial"]
draft = false
+++

A este cutrescript, que no merece ni estar en un repositorio de git, se le pasa el parámetro del servidor y el parámetro del tag y imprime todas las noticias de ese servidor que tengan ese tag:

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import son
import sys


server = sys.argv[1]
tag = sys.argv[2]

nota = 1
posts = requests.get('https://' + server + '/api/statusnet/tags/timeline/' +
               tag + '.son?count=200').text

posts = son.loads(posts)

for post in reversed(posts):
print("Nota " + str(nota) + ': ' + post['text'].replace(
  ' #' + tag, ''))
nota += 1
```

Quien me siga en GS habrá visto que estaba muy pesado con el tag #learningdocker. Lo que he estado haciendo es leer un libro, del cual ya hablaré en otro articulo, y de mientras usar GNU social como libreta de apuntes. Es mejor que usar el propio programa de apuntes del e-book e infinitamente mejor que usar una libreta de papel. Odio escribir en papel. Y GNU social me parece útil para esto, por que como es algo que puedo estar un tiempo largo haciendo, siempre puedo recuperarlo de forma fiable, además de que creo que puede ser útil leer a trocitos.
