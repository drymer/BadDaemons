+++
title = "Sobre DNS, DDOS y la fragilidad de internet"
author = ["drymer"]
date = 2016-10-25T15:40:00+02:00
tags = ["sysadmin"]
draft = false
+++

El día 21 de Octubre de 2016 se ha producido un ataque ddos de los que hacen historia. No ha sido solo por que han distribuido el ataque en distintas oleadas y que además lo han distribuido también por tipos de dispositivos infectados, siendo la última oleada efectuada por cacharros típicamente usados para el internet de las cosas. Más importante que eso, es el objetivo del ddos, Dyn, que, como ya veremos más adelante, <span class="underline">no</span> es un DNS raíz aunque tiene mucha relevancia en la resolución de dominios. Lo digo concretamente por que en muchos blogs y sitios webs dicen que lo son, pero no es así.

Lo que pretendo con este articulo es responder tres preguntas que me han surgido al enterarme del ddos:

-   Por que al tirar un solo DNS ciertos dominios no son accesibles? (Aunque Dyn no sea un DNS, si que gestiona la conexión a uno)

-   Por que, cuando lo anterior ha pasado, solo afecta a ciertas partes del mundo? (en este caso, EEUU y parte de Europa)

-   Los servidores DNS, en general, no tienen una caché?

Antes que nada, dos cosas. La primera, dar las gracias a la gente de #info en la red de irc de los servidores andreas6slkgnyct.onion y fojobaruwolbfyws.onion por ayudarme a aclarar algunos conceptos del articulo y a Ceilano por leerlo antes de publicarlo. Segunda, este articulo lo he sacado de mis conocimientos y buscando por ahí, así que es perfectamente posible que falle en algún punto. Si alguien detecta un fallo, agradeceré que lo comente. Esperemos que no sea garrafal.

Empecemos. Para responder a las preguntas, intentaré empezar por el principio, desde explicar el funcionamiento del DNS a un análisis del ataque en cuestión. Lo que tenia claro, antes de buscar algo más de información, es lo siguiente:

-   Los DNS son los servicios encargados de traducir las ip de los servidores en dominios usables por personas. Dicho de otro modo, en vez de insertar en vuestro navegador la ip 185.101.93.221 para esta web, solo hay que recordar que es daemons.it.

-   Ahora viene la pregunta, como sabe el navegador que dominio traduce que ip? Pues usando el DNS del sistema. En el caso de los ñus, se puede ver mirando el archivo `/etc/resolv.conf`. Ahí pueden haber varias ip. Las más usadas son las de google o las de los ISP, si no te preocupa mucho la privacidad.

Repasemos lo que dice la [wikipedia](https://es.wikipedia.org/wiki/Sistema_de_nombres_de_dominio) respecto al DNS:

> El sistema de nombres de dominio1 (DNS, por sus siglas en inglés, Domain Name System) es un sistema de nomenclatura jerárquico descentralizado para dispositivos conectados a redes IP como Internet o una red privada.

Por lo tanto, aunque el DNS está diseñado (o rediseñado, más bien) para que sea descentralizado, no es federado. Esto quiere decir que ningún servidor DNS tiene todos los dominios del mundo, sino que están repartidos entre lo que se conocen como servidores raíz. La lista de estos, según la wikipedia:

| Letra | Dirección      | Operador                           | Ubicación geográfica                    |
|-------|----------------|------------------------------------|-----------------------------------------|
| A     | 198.41.0.4     | Verisign                           | Distribuido                             |
| B     | 192.228.79.201 | USC-ISI                            | Marina del Rey, California, U.S.        |
| C     | 192.33.4.12    | Cogent Communications              | Distribuido                             |
| D     | 199.7.31.13    | Universidad de Maryland            | College Park, Maryland, U.S             |
| E     | 192.203.230.10 | NASA                               | Mountain View, Californa, U.S           |
| F     | 192.5.5.241    | Internet Systems Consortium        | Distribuido                             |
| G     | 192.112.36.4   | Defense Information Systems Agency | Distribuido                             |
| H     | 128.63.2.53    | U.S. Army Research Lab             | Aberdeen Proving Ground, Maryland, U.S. |
| I     | 192.36.148.18  | Netnod                             | Distribuido                             |
|       | 192.58.128.30  | Verisign                           | Distribuido                             |
| K     | 193.0.14.129   | RIPE NCC                           | Distribuido                             |
| L     | 199.7.83.42    | ICANN                              | Distribuido                             |
| M     | 202.12.27.33   | Proyecto WIDE                      | Distribuido                             |

Se puede ver que hay cierta centralización en EEUU, más aún si tenemos en cuenta que la sede ICANN está en California y que depende del Departamento de Comercio de EEUU, pero ese es otro tema. Lo que hay que tener en cuenta es que todos los servidores DNS que usamos (los del ISP, dnscrypt, opendns, google, etc) tanto directamente como indirectamente hacen peticiones a estos servidores.

Ahora veremos un poco más sobre el ataque en cuestión. Que ha pasado exactamente y a quien? Haciendo una búsqueda rápida en el pato, vemos que la empresa afectada es una llamada Dyn, y que el ataque ddos ha hecho que ciertos dominios no sean accesibles desde ciertas localizaciones (EEUU y Europa). Esta gente, Dyn, se dedica a la "Gestión del rendimiento de Internet basado en la nube" y [hicieron un acuerdo](http://dyn.com/blog/dyn-partners-with-the-internet-systems-consortium-to-host-global-f-root-nameservers/) con el Internet Systems Consortium para llevar la carga de su servidor raíz. Se puede ver en la tabla que la letra F, que corresponde al ISC, tiene su ubicación distribuida. Como dice en el primer párrafo, Dyn no es un DNS raíz y se puede ver que ni aparecen en la web [http://root-servers.org](http://root-servers.org). Si que es cierto que ofrecen dominios, como se puede ver [aquí](http://dyn.com/dns/).

Que quiere decir que Dyn lleva la carga del servidor raíz de ICS y que este es distribuido? Quiere decir que mediante el protocolo anycast[^fn:1], Dyn distribuye todas las peticiones que recibe a los 58 servidores[^fn:2] del ICS desde sus 17 puntos de presencia[^fn:3] en todo el mundo.

Se que de momento parece que esté dando datos al tuntún, pero en breves uniremos los puntos y empezaremos a responder a las tres preguntas que he planteado.

Leyendo [este articulo](http://dyn.com/blog/dyn-statement-on-10212016-ddos-attack/) de dyn vemos un poco más de información sobre el ataque, aunque tampoco es que se extiendan demasiado. Sin entrar en muchos detalles, hubo tres oleadas, la primera apuntó únicamente al POP (Point Of Presence, es decir, donde tienen infraestructura física) de la costa este de EEUU y las dos siguientes fueron más genéricas.

Y ahora ha llegado la hora de responder las preguntas:

-   **Por que al tirar un solo DNS ciertos dominios no son accesibles?**: Primero de todo, no cayó un DNS cualquiera, cayó un servidor raíz. Como el funcionamiento de los dominios es jerárquico y no distribuido, los dominios de Twitter, Reddit, Spotify, etc. estaban definidos en la raíz del ICS, únicamente ahí, y por eso al no estar disponible el servidor, no podía resolver.

-   **Por que, cuando lo anterior ha pasado, solo afecta a ciertas partes del mundo?**: Es debido a varios factores, pero antes que nada hay que tener clara una cosa. En la práctica no se ha atacado al servidor raíz del ICS, sino a los servidores que gestionan la distribución de peticiones del servidor raíz del ICS, que han sido los de Dyn. El que haya afectado a X zonas geográficas más que a otras ha sido, únicamente, debido a su gestión e infraestructura. Si miráis el siguiente mapa[^fn:3], es fácil ver que toda la infraestructura de Dyn está concentrada en EEUU y Europa, por lo que es normal que fuesen las principales zonas geográficas afectadas.

-   **Los servidores DNS no tienen una caché?**: Este es la pregunta que he visto que la gente (me incluyo) más se ha hecho. "Como puede ser que el simple motivo de que un servidor raíz no esté activo tire tantísimas webs abajo? Si su trabajo es una chorrada al final, solo tiene que responder que Paco vive en la calle Falsa 123, su misión no es más complicada que la que hace la gente mayor en los pueblos cuando están sentados en su puerta con una silla plegable." Esto estaba pensando mientras leía las noticias del ddos. No ha sido hasta que me he animado a documentarme más en serio hasta que no he visto el motivo, y al final ha resultado de un obvio que me ha costado no darme una palmada en la frente: **seguridad, integridad, disponibilidad y autenticación**, las máximas de la seguridad de la información.

Aunque complicado, el siguiente escenario es posible, si los servidores DNS usasen caché:

-   Comprometer el servidor que responde a, por ejemplo, twitter.com

-   ddos a esa IP de twitter hasta que, para mitigarlo, la gente de twitter la quita de la rotación del DNS

-   La gente sigue entrando a la ip comprometida por culpa de la caché

O uno más tonto, yo tengo el dominio "trabajosmolones.com", que es una red para encontrar trabajo. Evidentemente tiene usuario y contraseña para evitar usos maliciosos. Ahora, como trabajosmolones.com tiene mucho uso, me he dado cuenta de que necesito crecer y migrar a otro servidor. Lo monto todo en la nueva máquina, queda todo precioso, actualizo el DNS para que apunte al nuevo servidor y ... No llega tráfico al nuevo servidor, por que los servidores DNS han decidido que para que consultar a los servidores raíz si ya tienen la caché. Esto podría ser malo si aún tuviésemos el control del antiguo servidor, pero podría ser catastrófico si no lo controlásemos, por que podría estar mandando información sensible al servidor de cualquier otra persona.

Y se puede seguir. Al final, el tema es que la caché no se puede usar a la ligera ya que podría dar muchísimos problemas. Por ello, el caso de estas webs tan grandes, han decidido que en el caso de que su DNS caiga, no se resuelva nada. Nos encontramos con la disyuntiva de siempre, comodidad vs seguridad.

Y ya están respondidas las tres preguntas que me atormentaban. The more you know.

[^fn:1]: Más información del protocolo anycast en la [Wikipedia](https://es.wikipedia.org/wiki/Alguna_difusión).
[^fn:2]: Se puede ver la cantidad en la web [http://root-servers.org](http://root-servers.org) (presionando la letra **F**).
[^fn:3]: Fuente: <http://dyn.com/blog/research-end-users-web-traffic-geographic-distribution-points-of-presence>
