+++
title = "Como replicar una web de HTML estático en ZeroNet"
author = ["drymer"]
date = 2016-08-19T23:43:00+02:00
tags = ["zeronet"]
draft = false
url = "/posts/como-replicar-una-web-de-html-estatico-en-zeronet/"
+++

Leyendo [el articulo](https://elzorrolibre.noblogs.org/2016/07/navegando-por-zeronet/) de [@zorro](https://quitter.se/zorrito) acerca de ZeroNet me dio curiosidad por esa plataforma y me puse a probar y leer un poco sobre el tema. Para quien no lo sepa, ZeroNet es una darknet estricta que permite tener webs (y otros servicios) resistentes a la censura, ya que se distribuyen en todos los ordenadores que visitan esas webs. Funciona con la tecnología de Bitcoin (los blockchains) para firmar las webs y la red BitTorrent para distribuirlas. Al usar P2P es casi imposible hacer desaparecer una web, ya que esta estará en cualquier usuaria que la haya visitado. Es obvio por que este tipo de tecnología es tan interesante, sobretodo en los tiempos tan precarios que corren. En España mismo estamos sueltos a una vigilancia y censura cada vez más evidente.

No es la primera red de este tipo que existe, también está i2p o freenet, que de hecho son proyectos mucho más maduros, ya que al fin y al cabo ZeroNet tiene apenas un año de vida. Pero aún así hay algo que para mi lo hace un claro vencedor y es que no está hecho en puto java de los cojones, sino python. Hasta hace poco, para mi usar java era una mierda por que era usar java, pero ahora ya me resultaría imposible del todo, con el ordenador tan justo de recursos que tengo. Por lo que una red de este tipo es una bendición (Si, i2p tiene una versión en c++ en desarrollo, pero la última vez que la probé fallaba más que una escopeta de feria).

Lo que menos me gusta de la ZeroNet es la documentación, que es más bien pobre. Te dicen cómo instalarlo y como crear una web, pero todo lo demás lo tienes que averiguar tu mismo.

Pero bueno, el tema es como meter tu web de HTML estático en ZeroNet. No hay que hacer mucho, la verdad. El único tema es el de los comentarios, sea lo que sea que uses, en zeronet no funcionará, ni los comentarios de ZeroNet se postearán en la web. Yo lo que he hecho es poner un simple aviso para los usuarios de ZeroNet diciendo que si quieren comentar, que vayan a la dirección de la clearnet o la del onion. En otro articulo explicaré como lo he hecho.

Primero de todo, hay que instalar ZeroNet, lógicamente. Recomiendo para ello visitar el enlace del [articulo de @zorro](https://elzorrolibre.noblogs.org/2016/07/navegando-por-zeronet/articulo).

Una vez instalado, en el directorio en el que esté instalado, se ejecuta lo siguiente para crear la web cómo tal:

```sh
./zeronet.py siteCreate
```

Devolverá la clave pública y la clave privada. Muy importante: **hay que guardar la clave privada**. Si se pierde, la web se va a la mierda. Cuando termine de ejecutarse, se habrá creado el directorio `$RUTA_A_ZERONET/data/[Clave Pública]`. En mi caso se creó el directorio `$RUTA_A_ZERONET/data/1M8FhYZe11wWGgCAf8X13eEtQxeyWmgL5/`. Dentro de este directorio se crea un archivo llamado `index.html` y el importante, que es `content.son`. Este archivo es una especie de sitemap de la web, cada vez que alguien visite la web, lo primero que descargará es ese archivo, en el que se listan todos los archivos que forman parte de la web unto a algún dato más.

Suponiendo que ya tengamos el HTML generado, que en mi caso está en el directorio `/home/drymer/Documentos/blog/output/`, solo hay que copiar el ya mencionado `content.son` en el directorio anterior, borrar el directorio `$RUTA_A_ZERONET/data/1M8FhYZe11wWGgCAf8X13eEtQxeyWmgL5/` y crear un enlace simbólico del directorio del HTML al directorio `data` de ZeroNet con el nombre del directorio borrado. Igual queda más claro visto en comandos:

```sh
cp $RUTA_A_ZERONET/data/1M8FhYZe11wWGgCAf8X13eEtQxeyWmgL5/content.son /home/drymer/Documentos/blog/output/
rm -rf $RUTA_A_ZERONET/data/1M8FhYZe11wWGgCAf8X13eEtQxeyWmgL5/
ln -s /home/drymer/Documentos/blog/output/ $RUTA_A_ZERONET/data/1M8FhYZe11wWGgCAf8X13eEtQxeyWmgL5
```

De este modo, sin tener que estar copiando de un lado a otro, ZeroNet ya accede a la web existente. Ahora solo queda firmarla y distribuirla.

```sh
# si se quiere automatizar, la clave privada se puede poner usto detrás de la clave pública, sino preguntará por ella
./zeronet.py siteSign $clave_publica [$clave_privada]
./zeronet.py --tor always sitePublish $clave_publica
```

Es posible que la primera vez no funcione el `sitePublish`. No he sabido encontrar un motivo claro, pero a mi me pasó. Lo que tuve que hacer es ver yo mismo la web desde el navegador y pasársela a otra persona. No se si fue lo anterior o simplemente que pasaron unos minutos, pero después ya pude publicarla sin problemas.

Y con esto ya estamos, una manera muy sencilla de replicar contenido. Mi web, por si alguien usa ya ZeroNet, está [aquí](http://127.0.0.1:43110/1M8FhYZe1j1wWGgCAf8X13eEtQxeyWmgL5/).

De regalo unos cuantos enlaces de ZeroNet que me parecen interesantes:

-   [ZeroMe con un tema oscuro](http://127.0.0.1:43110/1HmJ7GadjdiQUPevaK7QXVrK4nhyCp1fSj/)

-   [Un mirror de Privacy Tools](http://127.0.0.1:43110/1J625kWnshbcBk9gE9BdPr4peTeXusRzU8/)

-   [Un foro en castellano](http://127.0.0.1:43110/165eqHdoQfyf7CVGqtVCGNDvMBZhwVSJBL/)

-   [Una wiki en castellano](http://127.0.0.1:43110/1FwbupJhHcnFWGXuu3bn5VxcRVW9tx2tDQ/)
