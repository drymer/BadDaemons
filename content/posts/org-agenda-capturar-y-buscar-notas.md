+++
title = "org-agenda: Capturar y buscar notas"
author = ["drymer"]
date = 2016-11-22T08:30:00+01:00
tags = ["emacs", "orgmode"]
draft = false
+++

La agenda de `org-mode` es la herramienta que ha conseguido que mucha gente pase a usar emacs. Esta puede usarse de muchas formas, no solo como una agenda como tal. Yo la uso para intentar estar medio organizado en mi día a día, para hacer un seguimiento de los proyectos y programas que desarrollo y hasta sustituí wallabag. Esto último toca otro día, lo que veremos hoy es el funcionamiento básico de la captura de notas y los comandos de agenda personalizados mediante ejemplos.

Pero como siempre, hay que empezar por el principio. Hay tres paradigmas (generalizando) en el uso y organización de la agenda de org-mode. El primero se podría llamar el paradigma de la estructura jerarquizada. Este buscará la organización visual. Un archivo de ejemplo es el siguiente:

```text
* Personal
** TODO Tarea de ejemplo
* Trabajo
** TODO No matar lusers
* Ordenadores
** Proyectos Personales
*** General
**** TODO Actualitzar README de programes
**** DONE Abrir links de emacs en navegador
 - State "DONE"       from "TODO"       [2016-10-16 dom 22:01]
 SCHEDULED: <2016-09-13 mar>
*** isso-gnusocial
**** TODO Cambiar codificación de títulos
*** gnusrss
**** TODO Hacerlo compatible con mastodon
La idea es usar la api atom.
Para probar en mastodon: mastodon.social
*** twitter2rss
**** WAITING Esperar feedback
** VPS
*** TODO Renova el domini daemons.it
DEADLINE: <2016-09-16 vie>
```

A simple vista se puede ver la organización de las notas que se toman. Este paradigma se enfoca más en la búsqueda de TODO, fechas programadas y deadlines. Veamos ahora un ejemplo del paradigma de la estructura por etiquetas:

```text
* TODO Tarea de ejemplo                                                                       :personal:
* TODO No matar lusers                                                                        :trabajo:
* TODO Actualitzar README de programes                                                        :proyectos:general:
* DONE Abrir links de emacs en navegador                                                      :proyectos:general:
 - State "DONE"       from "TODO"       [2016-10-16 dom 22:01]
 SCHEDULED: <2016-09-13 mar>
* TODO Cambiar codificación de títulos                                                        :proyectos:issoGnusocial:
* TODO Hacerlo compatible con mastodon                                                        :proyectos:gnusrss:
La idea es usar la api atom.
Para probar en mastodon: mastodon.social
* WAITING Esperar feedback                                                                    :twitter2rss:
* TODO Renova el domini daemons.it                                                            :vps:
DEADLINE: <2016-09-16 vie>
```

En el ejemplo se ve claro lo que hay, pero si tienes más de 20 notas empieza a liarse. Este paradigma se enfoca más en la búsqueda por etiquetas o contextos. Ahora viene el tercero, que es el que uso yo, que es una mezcla de ambos. De este modo, puedo ver a simple vista si me interesa o puedo buscar con la agenda.

```text
* Personal                                                         :personal:
** TODO Tarea de ejemplo
* Trabajo                                                           :treball:
** TODO No matar lusers
* Ordenadores
** Proyectos Personales                                           :projectes:
*** General                                                       :general:
**** TODO Actualitzar README de programes
**** DONE Abrir links de emacs en navegador
 - State "DONE"       from "TODO"       [2016-10-16 dom 22:01]
 SCHEDULED: <2016-09-13 mar>
*** isso-gnusocial                                          :issoGnusocial:
**** TODO Cambiar codificación de titulos
*** gnusrss                                                       :gnusrss:
**** TODO Hacerlo compatible con mastodon
La idea es usar la api atom.
Para probar en mastodon: mastodon.social
** VPS                                                                 :vps:
*** TODO Renova el domini daemons.it
DEADLINE: <2016-09-16 vie>
, * Caldav
```

Se puede ver, además, que los tags son hereditarios, por lo que solo hace falta etiquetar un árbol y los que estén por debajo de este tendrán la misma etiqueta. Ahora vamos al código, la parte de la captura de notas:

```emacs-lisp
(setq org-capture-templates
      '(("s" "Tasca Simple" entry (file+headline "~/Documentos/org/notas.org" "Notes")
         "** TODO %? \n:PROPERTIES:\n:CREATED: %u\n:END:\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n")
        ("b" "Idea per article" entry (file+headline "~/Documentos/blog/Articulos/articulos.org" "Ideas")
         "** TODO %?\n:PROPERTIES:\n:CREATED: %u\n:END:\n")
        ("p" "Personal" entry (file+headline "~/Documentos/org/index.org" "Personal")
         "* TODO %? \n:PROPERTIES:\n:CREATED: %u\n:END:\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n")
        ("t" "Treball" entry (file+headline "~/Documentos/org/index.org" "Treball")
         "* TODO %? \n:PROPERTIES:\n:CREATED: %u\n:END:\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n")
        ("w" "Marcadors" entry (file "~/Documentos/org/web.org")
         "* %x\n  %U\n  %c"
         :empty-lines 1)
        ("v" "Relacionat amb els VPS / chustaserver" entry (file+headline "~/Documentos/org/index.org" "VPS")
         "* TODO %?\n:PROPERTIES:\n:CREATED: %u\n:END:\n")))

(setq org-capture-templates
      (append org-capture-templates
              '(("o" "Ordinadors")
                ("og" "General" entry (file+headline "~/Documentos/org/index.org" "General")
                 "* TODO %?\n:PROPERTIES:\n:CREATED: %u\n:END:\n")
                ("oi" "isso-gnusocial" entry (file+headline "~/Documentos/org/index.org" "isso-gnusocial")
                 "* TODO %?\n:PROPERTIES:\n:CREATED: %u\n:END:\n")
                ("or" "gnusrss" entry (file+headline "~/Documentos/org/index.org" "gnusrss")
                 "* TODO %?\n:PROPERTIES:\n:CREATED: %u\n:END:\n")
                ("oj" "jabbergram" entry (file+headline "~/Documentos/org/index.org" "jabbergram")
                 "* TODO %?\n:PROPERTIES:\n:CREATED: %u\n:END:\n")
                ("os" "SimpleUpload" entry (file+headline "~/Documentos/org/index.org" "SimpleUpload")
                 "* TODO %?\n:PROPERTIES:\n:CREATED: %u\n:END:\n"))))
```

En el primer trozo de código se define la variable `org-capture-templates`. Si se evalua el código y ejecuta `org-capture` (C-c c) se verá que han aparecido nuevas opciones.

{{< figure src="/img/2016-11-21-224806_1024x768_scrot.png" >}}

Cada una de ellas hace una cosa distinta, y por el código se puede entender bastante bien. Explicaré las palabras clave que se usan para que quede un poco más claro:

-   **%?**: Es dónde se colocará el cursor al capturar.

-   **%u**: La hora de la creación de la nota usando corchetes, por lo que no aparecerá en la agenda.

-   **%(org-insert-time-stamp (org-read-date nil t \\"+0d\\")**: La hora de la creación de la nota usando <>, por lo que aparecerá en la agenda.

A grandes rasgos, la **Tasca Simple** lo que hace es crear una nota en el archivo "~/Documentos/org/notas.org" debajo de la cabecera "Notas". **Idea per article** crea una nota en el archivo "~/Documentos/blog/Articulos/articulos.org" debajo de la cabecera "Ideas". **Personal** y **Treball** más de lo mismo con sus propias características.

La segunda parte del código se define a parte básicamente por que no supe hacerlo en la anterior definición. Lo que hace es crear un submenú. En la imagen de las capturas se puede ver que hay una sección que se llama "Ordinadors" y que no aparecen ahí todas las opciones que se ven en el código. Esto es debido a que están escondidas hasta que se presiona la letra clave, que en este caso es la **o**. Véase la imagen.

{{< figure src="/img/2016-11-21-230839_1024x768_scrot.png" >}}

Creo que no hace falta comentar mucho más. Para más información se puede ver esta [página web](http://orgmode.org/manual/Capture-templates.html#Capture-templates) o preguntar en los comentarios. El caso de los marcadores lo comentaré en otro articulo.

Ahora vamos con las búsquedas personalizadas en la agenda. Véamos el código:

```emacs-lisp
(setq org-agenda-custom-commands
    '(;; Cosas del blog
      ("b" . "Blog")
      ("bi" "Ideas para articulo" todo "TODO|INPROGRESS" ((org-agenda-files '("~/Documentos/BadDaemons/Articulos/articulos.org")) (org-agenda-sorting-strategy)))
      ("bp" "Articulos listos para publicar" todo "READY|PROGRAMMED" ((org-agenda-files '("~/Documentos/BadDaemons/Articulos/articulos.org"))))
      ("bc" "Articulos cancelados" todo "CANCELLED" ((org-agenda-files '("~/Documentos/BadDaemons/Articulos/articulos.org"))))
      ("bf" "Articulos publicados" todo "DONE" ((org-agenda-files '("~/Documentos/BadDaemons/Articulos/articulos.org"))))
      ;; Casa
      ("c" . "Casa")
      ;; Ver lo que se ha hecho, no terminado
      ("cg" "Estado de todas las tareas" tags "+casa|+chustaserver|+vps|+labrecha|+emacs|+productividad/+TODO|NEXT|INPROGRESS|HOTFIX|BLOCKED|TESTING" ((org-agenda-files '("~/Documentos/org/index.org"))))
      ("ce" "Estado de las tareas activas" tags "+casa/+TODO|NEXT|INPROGRESS|HOTFIX" ((org-agenda-files '("~/Documentos/org/index.org"))))
      ("cp" "Estado de las tareas paradas" tags "+casa/+BLOCKED|TESTING" ((org-agenda-files '("~/Documentos/org/index.org"))))
      ("cd" "Tareas terminadas sin terminar" tags "/DONE" ((org-agenda-files '("~/Documentos/org/index.org"))))
      ("cm" "Mostras miscelaneos" tags "+miscelaneos/TODO" ((org-agenda-files '("~/Documentos/org/index.org"))))
      ("cr" "Mostras radar" tags "+radar/TODO" ((org-agenda-files '("~/Documentos/org/index.org"))))
      ("ct" "Hecho la semana anterior"
       ((agenda "")
        (org-agenda-overriding-header "Semana anterior")
        (org-agenda-span 7)
        (org-agenda-archives-mode t)
        (org-agenda-show-log 'clockcheck)
        (org-agenda-files '("~/Documentos/org/index.org"))))
      ("cs" "Calendario de la semana que viene"
       ((agenda "")
        (org-agenda-overriding-header "Semana que viene")
        (org-agenda-span 7)
        (org-agenda-start-day "+7d")
        (org-agenda-files '("~/Documentos/org/index.org"))))
      ;; Reportes del trabajo
      ("t" . "Trabajo")
      ;; Ver lo que se ha hecho, no terminado
      ("tl" "Hecho en los últimos tres días" agenda ""
       ((org-agenda-overriding-header "Last three days")
	(org-super-agenda-mode)
	(org-super-agenda-groups org-agenda-supergroup-tags-log)
        (org-agenda-span 3)
       	(org-agenda-start-day "-3d")
       	(org-agenda-start-with-log-mode t)
       	(org-agenda-log-mode-items '(clock closed))
        (org-agenda-archives-mode t)
        (org-agenda-show-log 'clockcheck)))
      ("ti" "Hecho en la última semana" agenda ""
       ((org-super-agenda-mode)
	(org-super-agenda-groups org-agenda-supergroup-tags-log)
        (org-agenda-span 7)
       	(org-agenda-start-day "-7d")
       	(org-agenda-start-with-log-mode t)
       	(org-agenda-log-mode-items '(clock closed))
        (org-agenda-archives-mode t)
        (org-agenda-show-log 'clockcheck)))
      ("tn" "Siguientes tareas" tags-todo "+trabajo/NEXT"
       ((org-super-agenda-mode)
	(org-super-agenda-groups org-agenda-supergroup-tags-next)))
      ("ta" "Tareas activas" tags-todo
       "+trabajo/INPROGRESS|TESTING|HOTFIX"
       ((org-super-agenda-mode)
	(org-super-agenda-groups org-agenda-supergroup-tags-active)))
      ("tb" "Tareas paradas" tags-todo "+trabajo/BLOCKED|TOFOLLOW"
       ((org-super-agenda-mode)
	(org-super-agenda-groups org-agenda-supergroup-tags-blocked)))
      ("tt" "Tareas por hacer" tags "+trabajo/TODO"
       ((org-super-agenda-mode)
	(org-super-agenda-groups org-agenda-supergroup-tags-todo)))
      ("td" "Tareas terminadas sin archivar" tags "+trabajo/DONE|CANCELLED"
       ((org-super-agenda-mode)
       	(org-super-agenda-groups org-agenda-supergroup-tags-done)))
      ;; Reportes
      ("r" . "Reportes")
      ;; Mostrar tareas a reubicar
      ("rr" "Reubicar" tags "+refile" ((org-agenda-remove-tags nil) (org-agenda-hide-tags-regexp "refile")))
      ;; Reporte
      ("ra" "Ver anteriores asambleas de La Brecha Digital" tags "ARCHIVE_ITAGS={labrecha}&ARCHIVE_ITAGS={asamblea}/DONE"
       ((org-agenda-files (file-expand-wildcards "~/Documentos/org/index.org_archive"))))
      ;; Buscar en archivados solamente
      ("l" . "Archivo")
      ("ls" "Archive search" search ""
       ((org-agenda-files (file-expand-wildcards "~/Documentos/org/*.org_archive"))))))
```

Cuando se evalua el código anterior, al ejecutar `org-agenda` (C-c a) ahora aparecerán nuevas opciones. Concretamente cuatro en el primer buffer:

-   **b** de blog

-   **p** de Proyectos personales

-   **v** de relacionados con los VPS

-   **w** de los marcadores web

{{< figure src="/img/org-agenda.png" >}}

Si presionamos la **b**, se mostrará un nuevo desplegable con la **i** de ideas para el blog y la **p** de articulos listos para públicar.

{{< figure src="/img/org-agenda2.PNG" >}}

Lo mismo cuando se presiona la letra **p**, pero con otras opciones. El de la letra **w**, como mencioné más arriba, lo deamos para otro articulo.

Veamos ahora un ejemplo de la búsqueda de todas las notas etiquetadas como **Ideas** de cosas a escribir en el blog.

{{< figure src="/img/2016-11-21-231744_1024x768_scrot.png" >}}

Como se puede ver, es muy cómodo y tiene mucha potencia detrás. Cualquier duda, comenten.
