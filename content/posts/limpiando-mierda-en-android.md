+++
title = "Limpiando mierda en Android"
author = ["drymer"]
date = 2016-01-17T17:24:00+01:00
tags = ["android", "movil", "privacidad"]
draft = false
+++

Cuando alguien está pensando en comprar un móvil, lo primero que debería hacer es mirar en la [wiki](https://wiki.cyanogenmod.org/w/Devices) de Cyanogen Mod para ver que el que le interesa tiene posibilidad de usar esa ROM. Eso es lo que hice yo, me llegó el móvil y al mirar más en profundidad vi que dentro del propio móvil habían sub-categorías, en las cuales algunas soportaban CM y otras no. La mía fue la que no. Así que recomiendo mirar bien en profundidad, no sólo por encima cómo hice yo.

Por este motivo, decidí quedarme con el Android de stock y quitarle toda la mierda. Un requisito, que no se tocará en este manual, es le de tener el móvil rooteado. Sin esto no hacemos nada, las mierdas de Google no se podrán desinstalar. Al lío. Lo primero que hay que hacer es instalar `fastboot` y el `adb` de Android. En debian:

```sh
su -c "apt-get install android-tools-adb android-tools-fastboot"
```

Lo siguiente es enchufar el móvil al ordenador usando el cable USB y activar las herramientas de debug USB. Hay que acordarse de quitarlo después ya que permite ciertas cosas que en condiciones normales no se podrían hacer. Depende de la versión de Android, puede variar un poco el proceso, pero se hace más o menos así: "Ajustes > Acerca del teléfono > Número de compilación" Este último, hay que presionarlo unas cuantas veces rápidamente y se acabarán activando las "Herramientas de desarrollador", que aparecerán usto encima de "Acerca del teléfono". Ahí, se activa la "Depuración USB" y por comodidad "Pantalla Activa", ya que si el móvil se bloquea, adb no funciona correctamente. Ahora al enchufar el móvil, en la pantalla saldrá algo del tipo del fingerprint del ordenador y que si queremos dejar que acceda al móvil. Se acepta y ale. Hecho lo aburrido, vamos a lo divertido.

Cómo root siempre:

```sh
adb devices
List of devices attached
BY4X4Xthtq34418 device
```

Si se ve algo parecido a lo de arriba al ejecutar ese comando, significa que adb tiene acceso al móvil y que se puede seguir adelante.

Antes de seguir, es muy recomendable instala [f-droid](https://f-droid.org) e instalar un par de aplicaciones. La primera será `Hacker Keyboard` (por ejemplo, cualquier teclado vale) y la segunda será `oandbackup`. La primera es necesaria porqué nos cargaremos el teclado de Google, y la segunda porqué con esa aplicación, que de por si no hará nada si no está instalado `busybox` (ni falta que hace), averigua el nombre completo del apk que queremos desinstalar.

A lo práctico:

```sh
adb shell pm list packages -f
adb shell "su -c 'pm disable $paquete'"
adb shell top
```

El primer comando listará todas las aplicaciones que hayan instaladas, y el segundo deshabilitará el \$paquete que se le diga. Me inclino más por deshabilitarlo que por borrarlo, porque nunca se sabe. Viendo la lista de procesos (tercer comando) se puede ver que una vez desactivados no se ejecutan, por lo que lo considero más o menos fiable. Con esto ya se puede tirar millas. Sólo hace falta una lista de programas a desinstalar y un bonito bucle que ejecute el comando de desactivación con los miembros de la lista. La lista (que es una mezcla (ligeramente modificada) de [esta](https://github.com/jaredsburrows/android-bloatware/blob/master/disable-list.txt) y mía) se puede descargar [aquí](http://daemons.cf/wp-content/deshabilitar.txt). En esta lista está todo lo de google (al menos de mi móvil), mierdas de huawei y alguna cosa más. Aviso de que, cómo siempre que se tocan cosas de este tipo, hay que ir con cuidado. Para quitar todo lo que hay en la lista:

```sh
for i in $(cat definitivodelto); do adb shell "su -c 'pm disable $i'";done
```

Y ya está. Cuando desactiva la aplicación, en la terminal sale `Package $paquete new state: disabled`. Sino, es que no se ha desinstalado, probablemente por que no existe.

Y con esto ya se ha quitado mucha mierda. Ahora sólo queda repasar, y para eso está `oandbackup`. Sólo queda mirar el nombre de la aplicación que se quiera desinstalar y debajo se ve el nombre del apk. Por ejemplo, weechat se llama `com.ubergeek42.Weechat.Android`. Sólo queda ejecutar:

```sh
adb shell "su -c 'pm disable com.ubergeek42.Weechat.Android'"
```

{{< figure src="/img/oandbackup.png" >}}

Y a volar.

PD: Un par de comandos molones de adb de regalo:

```sh
# Hacer captura de pantalla del móvil y descargarla al ordenador con el nombre "test.png"
adb shell screencap -p /sdcard/test.png; adb pull /sdcard/test.png

# Backup completo de todo el móvil
adb backup -all -f backup.ab
adb restore backup.ab

## Extraer apk
adb shell pm path org.andstatus.app
package:/data/app/org.andstatus.app-1.apk
adb pull /data/app/org.andstatus.app-1.apk
```
