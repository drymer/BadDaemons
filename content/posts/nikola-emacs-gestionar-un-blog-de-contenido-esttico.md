+++
title = "Nikola + emacs: Gestionar un blog de contenido estático"
author = ["drymer"]
date = 2015-09-04T22:26:00+02:00
tags = ["python", "nikola", "emacs"]
draft = false
+++

Lo de tener un blog siempre es divertido, uno coge, escribe sus mierdas y los demás lo leen o no, eso ya depende de la mierda. Pero oye, siempre se puede intentar, es lo fantástico de internet. Y además es gratis, según cómo se monte. Se puede ir a lo fácil y tirar de wordpress.com o aún peor, blogspot. Entiendo que en ciertos casos hay que tirar del primero (del segundo no hay ningún motivo válido para usarlo, lo siento), no todo el mundo puede o quiere pagar un VPS o puede tener una máquina encendida en casa todo el día. El siguiente articulo es para los que si tienen una máquina disponible.

Nikola es un generador de web estático. Esto tiene distintas ventajas respecto a una web con php tal cómo wordpress, por ejemplo.

-   Más seguras, requiere menos recursos
-   Es rápido, crea el HTML de manera incremental (sólo crea las páginas necesarias cada vez)
-   Acepta distintos formatos: iPython, RST, Markdown, HTML y con el plugin adecuado, org-mode.
-   CLI sencilla

Tiene varios plugins, desde importar articulos de wordpress hasta el ya mencionado org-mode plugin. Es cuestión de probarlo, en realidad es fácil hacerlo. Además tienen bastante documentación, no he visto nada que no estuviese ahí. Así que al lío:

```sh
apt-get install python-pip
pip install nikola
```

Y ya. Ahora viene lo divertido, que es configurarlo. Para hacerlo tenemos que ir al directorio en el que queramos que esté y ejecutar:

```sh
nikola init miweb
# la carpeta lisp o la que sea, dentro de .emacs.d
cd ~/.emacs.d/lisp/
git clone https://github.com/redguardtoo/org2nikola.git
```

Y entonces hará unas cuantas preguntas. Podéis responderlas ahora o podéis modificar el archivo conf.py una vez hayáis terminado, así que cómo queráis. No me extenderé en la configuración por que es bastante sencilla. La configuración que tengo la podéis ver [aquí](http://daemon4jidu2oig6.onion/cgit/isso-utils/tree/conf.py), en el git. Después de eso instalaremos el plugin org2nikola, que permite exportar arboles org al directorio que le digas y con formato HTML. Configuramos el nuevo modo de emacs poniendo en el ~/.emacs o en el ~/.emacs.d/init.el lo siguiente:

```emacs-lisp
(use-package org2nikola
:load-path "~/Proyectos/org2nikola/"
:init
;; Se instala el htmlize de melpa, el de org-mode es demasiado viejo
(use-package htmlize :ensure t)
:config
(setq org2nikola-output-root-directory (concat (getenv "HOME") "/Documentos/BadDaemons"))
(setq org2nikola-use-verbose-metadata t))

(defun daemons/nikola-deploy-partido ()
"Ejecuta un script que tira de at para programar el push al repositorio git."
(interactive)
(shell-command (concat "/bin/bash $HOME/Documentos/BadDaemons/scripts/deploy-partido.sh " (org-read-date))))

;; Las dos siguientes funciones probablemente no sirvan a nadie
(defun daemons/nikola-url-post ()
"Devuelve el enlace del articulo en el que esté el cursor."
(interactive)
(setq url (concat "https://daemons.it/posts/"
                  (cdr (car (org-entry-properties (point) "post_slug")))))
(message url)
(kill-new url))

(defun daemons/nikola-url-config ()
"Devuelve el enlace de la parte de la configuración en la que esté el cursor."
(interactive)
(setq url (concat "https://daemons.it/stories/mi-configuracin-de-emacs/#"
                  (cdr (car (org-entry-properties (point) "CUSTOM_ID")))))
(message url)
(kill-new url))

(use-package nikola
:load-path "~/Proyectos/nikola.el/"
:config
(setq nikola-output-root-directory "~/Documentos/BadDaemons/")
(setq nikola-verbose t)
(setq nikola-webserver-auto nil)
(setq nikola-webserver-host "127.0.0.1")
(setq nikola-webserver-port "8080")
(setq nikola-webserver-open-browser-p t)
(setq nikola-deploy-input t)
(setq nikola-deploy-input-default "Nuevo articulo")
(setq nikola-build-before-hook-script (concat nikola-output-root-directory "scripts/pre-build.sh"))
(setq nikola-build-after-hook-script (concat nikola-output-root-directory "scripts/post-build.sh")))
```

En las funciones opcionales quedaría mucho mejor pasarle la variable del directorio de root de nikola, pero no he conseguido hacer que 'shell-command la coja, así que se queda en modo cutre.
Hecho esto, ya podremos escribir articulos. La idea de este plugin es tener todos los articulos en un sólo org, tal que así:

```text
* Primer Articulo
** Subtitulo del primer articulo
  ola ka se
* Segundo articulo
  loremp ipsum bla bla bla
```

Para exportar tenemos que situarnos en el sub-árbol que queremos exportar y ejecutar M-x org2nikola-export-subtree. Esto, cómo hemos dicho, creará un articulo con formato HTML en el directorio 'org2nikola-output-root-directory.

[La sección](https://daemons.cf/stories/mi-configuracin-de-emacs/#org2nikola) pertinente en mi configuración.
