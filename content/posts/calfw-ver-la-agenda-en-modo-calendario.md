+++
title = "Calfw: ver la agenda en modo calendario"
author = ["drymer"]
date = 2016-12-11T08:30:00+01:00
tags = ["orgmode", "emacs"]
draft = false
+++

Esto fue lo primero que busqué cuando empecé a usar la agenda hasta que me di cuenta de que no tenia. Me sorprendió bastante al principio, hasta que entendí que `org-mode` está pensado para gestionar listas y hacer búsquedas, no dar una visión general. Que ojo, eso va muy bien, pero hay veces que se prefiere el formato clásico del calendario. Y buscando, encontré `calfw`.

Este paquete muestra un calendario simple, sin misterio. Permite 4 tipos de fuentes:

-   [howm](http://howm.osdn.jp/index.html)

-   ical

-   org-mode

-   diary-mode

Solo veremos como usar el de `org-mode`, que es el que uso yo. La configuración necesaria es mínima:

```emacs-lisp
(if (not (el-get-package-installed-p 'calfw))
(el-get 'sync 'calfw))

(use-package calfw
:load-path "el-get/calfw"
:config
(require 'calfw-org)
(setq cfw:org-overwrite-default-keybinding t)
(setq calendar-week-start-day 1)
(setq calendar-month-name-array
    ["Gener" "Febrer" "Març" "Abril" "Maig" "Juny" "Juliol" "Agost" "Septembre" "Octubre" "Novembre" "Desembre"])
(setq calendar-day-name-array
    ["Diumenge" "Dilluns" "Dimarts" "Dimecres" "Dijous" "Divendres" "Dissabte"])
:bind ("C-c f" . cfw:open-org-calendar))
```

Como se puede ver, lo he instalado mediante [el-get](https://daemons.cf/posts/el-get-otro-instalador-de-paquetes). Se puede instalar mediante `package.el`, por eso. Respecto al resto de la configuración:

-   `cfw:org-overwrite-default-keybinding`: establece los atajos de teclado de la agenda de `org-mode`, bastante práctico si es la que se suele usar.

-   `calendar-week-start-day`: al establecerla en 1, la semana empieza el lunes.

-   `calendar-month-name-array`: la traducción de los meses al catalan.

-   `calendar-day-name-array`: la traducción de los días al catalan.

Y ya. Respecto a los atajos de teclado, al activar los de `org-mode` pasan a ser los siguiente:

| Tecla | Función                |
|-------|------------------------|
| g     | Actualiza la agenda    |
|       | Salta a fecha indicada |
| k     | org-capture            |
| x     | Cierra calfw           |
| v d   | Vista diaria           |
| v w   | Vista semanal          |
| v m   | Vista mensual          |

Para abrir el buffer del calendario, hay que ejecutar `cfw:org-calendar-buffer` o, si se usa el bind que tengo establecido, C-c f. Se ve así:

{{< figure src="/img/calfw.png" >}}

Más información en el [repositorio git](https://github.com/kiwanami/emacs-calfw) del proyecto.
