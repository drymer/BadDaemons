+++
title = "Logcheck"
author = ["drymer"]
date = 2015-03-27T19:15:00+02:00
draft = false
tags = ["sysadmin", "elbinario"]
+++
Una de las tareas del buen sysadmin es la de revisar los logs de sus servicios. Los logs son importantes, de ahí la reticencia de algunos sitios de borrar completamente estos. Nosotros en breves sacaremos una página explicando que guardamos y que no. Pero bueno, el tema es otro.

La cosa es que revisar los logs de una máquina es fácil. Te logueas por ssh, haces un cat, un more o lo que sea y echas un ojo. Pero la cosa se complica cunado no es una máquina sino 10, 20 o 100. Para estas cosas va bien algo cómo logchek, que se encarga de mandar por correo los logs que le digas. Pero los logs son, normalmente, algo sensibles. Así que mejor mandarlos cifrados, no? A eso vamos.

Instalamos logcheck en debian y derivados cómo siempre:

```bash
su -c "aptitude install logchek"
```

Sencillo. Ahora editamos el fichero de configuración con un editor cualquiera, tal cómo nano:

```bash
su -c "nano /etc/logcheck/logcheck.conf"
```

A continuación pegaré solamente las lineas que vamos a modificar junto con un comentario explicando el porqué:

```bash
DATE="$(date +'%d-%m-%Y %H:%M')" # Cómo siempre, los americanos y sus cosas. Así se verá en el formato europeo.

REPORTLEVEL="paranoid" # Porqué nosotros lo valemos

MAILASATTACH=1 # Enviará los resultados por correo

ATTACKSUBJECT="Security Alerts" # Esta y las siguientes dos lineas mandaran un asunto distinto dependiendo del evento
SECURITYSUBJECT="Security Events"
EVENTSSUBJECT="System Events
```

Después metemos en el fichero /etc/logcheck/logcheck.logfile los ficheros de log que queramos monitorizar. Por defecto viene auth.log y syslog. Podemos añanidr, por ejemplo, los de dovecot, postfix, nginx, &#x2026;

Y con esto, tendremos lo básico. Si dejáis pasar una hora, veréis que recibís un correo. Si dejáis pasar un día, veréis que habéis recibido 24 correos. Esto es debido a que no se está filtrando nada de los logs. Logcheck puede aplicar expresiones regulares para sacar lo mas importante de los logs. Pero, por defecto, no trae ninguna. En Debian y derivados estas vienen en un paquete a parte, que se llama logcheck-databases. Es cuestión de instalarlo y en los directorios *etc/logcheck/ignore.\* tendremos un bueno montón de reglas que nos ahorraran mucho trabajo. Se incluyen para distintos servicios, tales cómo sftp, dovecot, postfix, ssh,&#x2026; Pero no viene ninguna para nginx o apache, que probablemente son los servicios que en un servidor cómo el nuestro más visitas van a llegar. Cómo es posible que no se tenga mayor interés en ver las peticiones con un código de 200, es decir las peticiones correctas a la web, se puede crear un archivo en /etc/logcheck/ignore.d.paranoid* llamado nginx, por ejemplo con una expresión regular que filtre todas estas peticiones. Un ejemplo sería así:

```bash
regexp
```

Esto debería quitarnos todo lo gordo de nginx. Y ahora, sólo recibiremos de vez en cuando algunas líneas de algo llamado [wpscan](http://elbinario.net/wpscan) o [joomscan](http://elbinario.net/joomscan), por ejemplo. Esto son scanners de vulnerabilidades (también tienen otras funciones) que, con la configuración que hemos visto más arriba, debería hacer saltar las alarmas y mandar un correo con esas lineas, ya que buscará ciertos archivos y patrones que devolverán, muchos o todos, un código 400, es decir, no encontrado. Pero cómo somos paranoicos (por algo escogimos esa configuración de logcheck) nos aseguraremos creando un archivo en *etc/logcheck/violations.d* llamado scanner, por ejemplo. Dentro de éste pondremos WPScan y JoomScan. Así nos aseguraremos de que cualquier linea del log con estas palabras nos salga en nuestro correo. Es curioso, pero un programa de pentesting (hacking ético?) cómo este es muy ruidoso. Aún usando la opción **-r**, que cambia el user agent, si se miran los logs se ve un brillante y gran WPScan en medio. Se puede comprobar haciendo:

```bash
grep WPScan /var/log/nginx/access.log # O dónde sea que esté el log del servidor web
```

En fin, a lo que íbamos era cifrar los correos que mande logcheck. La manera más sencilla de hacerlo es que se mire en el pad el script modificado. Realmente son tres lineas cambiadas, adjunto el diff del original también. Antes de ejecutarlo, hay que tener en cuenta que hay que importar la clave GPG del correo al cual enviaremos los correos, con el usuario logcheck. Para hacerlo:

```bash
su logcheck -c "gpg &#x2013;recv-keys $clave"
```

Una vez hecho, para comprobar que funcione bien, podemos hacer lo siguiente:

```bash
echo "Test de mails cifrados" >> /var/log/syslog
su logcheck -c logcheck
```

Y si todo va bien, mails cifrados y a volar.

