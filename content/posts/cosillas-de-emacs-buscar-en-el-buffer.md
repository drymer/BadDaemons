+++
title = "Buscar en el buffer de emacs"
author = ["drymer"]
date = 2016-05-18T19:18:00+02:00
tags = ["emacs", "helm"]
draft = false
url = "/posts/cosillas-de-emacs-buscar-en-el-buffer/"
+++

El siguiente paquete es de los buenos. Para buscar en un buffer, se suele usar `isearch`, que se ejecuta con `C-s`. Con esto se puede buscar una palabra o varias siempre que estén en orden. Yo echaba en falta el poder buscar mediante expresiones regulares, cómo hace `helm`. Entonces encontré `helm-swoop`. Este permitía hacer justo lo que andaba buscando, sólo habia que bindejarlo a `C-s` y a volar. Pero entonces perdía el comportamiento por defecto de `isearch`, lo cual no quería hacer, de poder evitarlo. Y el siguiente paquete fue la clave `ace-isearch-mode`.

Este paquete tiene tres dependencias,que son `isearch`, `avy` o `ace-jump` y `helm-swoop`. `ace-jump` y `avy` lo que hacen es pedir un carácter y luego mostrar las coincidencias en el buffer y asociar una letra a cada coincidencia. Al escribir esa letra asociada, mueve el cursor a esa posición. Hacen lo mismo sólo que de manera distinta, por lo que se puede escoger cualquiera de los dos.

{{< figure src="/img/avy.gif" caption="Figura 1: avy" >}}

Al ejecutar la función de búsqueda, se presiona `e a a` y lleva a la primera `e` del buffer, que es la de el primer parágrafo.

{{< figure src="/img/ace.gif" caption="Figura 2: ace" >}}

Al ejecutar la función de búsqueda, se presiona `e a a` y lleva a la primera `e` del buffer, que es la de el primer parágrafo.

Entonces, de que modo se combinan las tres funciones? Se presiona `C-s` y al escribir un carácter, se usa `ace-jump`. Al escribir cuatro más, osea cinco carácteres, se usa `isearch`, y al llegar a las seis o más, se usa `helm-swoop`. Muy versátil.

Ahora al lío. Se instalan los paquetes `M-x package-install RET ace-jump`, `M-x package-install RET helm-swoop` y `M-x package-install RET ace-isearch`.

Se configura con las siguientes lineas:

```emacs-lisp
(custom-set-variables
 '(ace-isearch-input-length 7)
 '(ace-isearch-jump-delay 0.25)
 '(ace-isearch-function 'avy-goto-char)
 '(ace-isearch-use-jump 'printing-char))
```

Y a buscar más eficientemente.

Más información en su [repositorio git](https://github.com/tam17aki/ace-isearch).

[La sección](https://daemons.cf/stories/mi-configuracin-de-emacs/#Buscar más eficazmente en Helm) pertinente en mi configuración.
