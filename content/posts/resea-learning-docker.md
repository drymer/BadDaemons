+++
title = "Reseña: Learning docker"
author = ["drymer"]
date = 2017-11-22T08:30:00+01:00
tags = ["docker", "libro"]
draft = false
+++

Últimamente estoy empezando a tocar bastante docker, tanto en el trabajo como en [La Brecha Digital](https://labrecha.digital) (certificado incorrecto temporalmente) y he pensado que aunque soy más de darme de ostias con las cosas hasta que las entiendo, leerme un libro al respecto no haría mal.

Y por ello me leí el libro de Learning Docker. No lo escogí enfrente a otros por ningún motivo en concreto, simplemente lo tenia a mano. Si no me equivoco lo saqué de [PackTub](https://www.packtpub.com), una web que a parte de vender libros cada día dan uno gratuito. FYI.

El libro me parece bastante bueno tanto para gente que no ha usado docker nunca como gente que sin meterse demasiado en su funcionamiento interno, lo usa habitualmente. A continuación las notas que he tomado [en GNU social](https://daemons.it/posts/cutrescript-para-mostrar-posts-de-tags-de-gnu-social/):

-   **Nota 1:** Una curiosidad sobre docker. Cuando inicias un contenedor de forma interactiva, puedes "desatacharte" presionando Ctrl-P Ctrl-Q, de forma que el contenedor sigue corriendo.

-   **Nota 2:** Otro par de datos que no sabia de docker. Por un lado, puedes rearrancar un contenedor que ha sido parado. Por otro lado, se puede simplemente congelar un contenedor con el subcomando pause.

-   **Nota 3:** Docker por defecto no borra los contenedores que han terminado, para ello hay que pasarle un --rm al arrancarlos. Parece lo suficientemente poco práctico como para hacerse un alias y meter el --rm por defecto.

-   **Nota 4:** La diferencia entre COPY y ADD es que el segundo manea tar y url.

-   **Nota 5:** Una diferencia entre CMD y ENTRYPOINT es que el primero se puede sobre-escribir cuando creas un contenedor. Pero cuando se arranca un contenedor con un ENTRYPOINT establecido, en vez de sobre-escribirlo, lo que hace es pasarlo como argumento.

-   **Nota 6:** Joder, no entendía como se podían mantener las imágenes limpias sin usar funciones complejas a la hora de construir, pero resulta que es mucho más simple que eso, solo hay que usar .dockerignore.

-   **Nota 7:** Buenas prácticas para escribir dockerfiles: <https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#onbuild>

-   **Nota 8:** Todo el capítulo de publicación de imágenes es bastante meh, no tiene tanto misterio. docker push $registry/imagen y a volar.

-   **Nota 9:** Docker por defecto usa la red 172.16.0.0/16 para lanzar contenedores. La forma no obvia de ver la IP de un contenedor es el subcomando inspect.

-   **Nota 10:** Sobre la publicación de puertos, tampoco tiene misterio. -p puertoContenedor:puertoHost. Es un nateo simple, se puede ver en la tabla nat de iptables. Pero lo interesante es que el -p sirve para más cosas, como establecer la IP en la que escuchará el contenedor tal que así: -p 192.51.100.73:80:80.

-   **Nota 11:** El EXPOSE del dockerfile tiene el mismo efecto que el -p al arrancar un contenedor. Pero si se asume el uso de la imagen por un tercero, es mejor ser explicito abriendo los puertos sin la intervención de quien lanza el contenedor (más que usar -P, que viene a ser que mapee todos los puertos sin concretar cuales son).

-   **Nota 12:** Hay dos formas de montar volúmenes. Una la que ya se mencionó, usar -v y mapear un directorio del host. Pero el otro es crear un contenedor que sea el volúmen. De este modo, varios contenedores pueden compartir volumen. Además, curiosamente no hace falta que el contenedor-volumen esté en marcha. Se puede hacer usando --volume-from al arrancar un contenedor.

-   **Nota 13:** El capítulo sobre orquestación me lo he leído en diagonal, pero básicamente hablan sobre el concepto de orquestar y sobre docker-compose.

-   **Nota 14:** El capítulo de testing me está gustando mucho. Hablan sobre como enfocar TDD unto a docker. Un poco pedestre, pero supongo que se puede automatizar y/o existe ya algo como virtualenv (dockerenv?).

-   **Nota 15:** La parte del capitulo de testing que hablan de Jenkins es muy meh. Aunque es importante mostrar la utilidad de docker con un programa de CI/CD, queda muy pegote.

-   **Nota 16:** El capitulo de debugging de contenedores está resultando muy interesante. De momento explica como funciona a bao nivel (para que sirven los cgroups, como aísla los procesos docker, redes, etc).
