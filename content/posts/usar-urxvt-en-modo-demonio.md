+++
title = "Usar urxvt en modo demonio"
author = ["drymer"]
date = 2016-06-30T20:10:00+02:00
tags = ["i3wm"]
draft = false
+++

Cómo ya sabrá quien haya leído [otros articulos](http://daemons.cf/categories/i3wm/), yo uso i3-wm en mis ordenadores. No lo dice anteriormente, pero uso urxvt, que es la terminal cuyo renderizado falla menos a menudo y es de las más ligeras, algo importante para un escritorio tiling.

Pues bien, hasta ahora he estado usando `urxvt` a pelo sin más, asignando a `META+RET` a `urxvt` sin más. Pero descubrí que hay un modo daemon del que se puede colgar distintos clientes, con lo que se reaprovecha la memoria usada. Con un equipo moderno puede no notarse mucha diferencia, pero yo, por ejemplo, tengo un netbook con una Pentium M y 512MB de RAM, lo cual es bastante justito, por lo que por poco que se rasque, se nota muchísimo.

El cutre-benchmark que he hecho es el siguiente (en ese PC). He abierto nueve terminales en cada espacio de trabajo de `i3-wm` y he abierto `htop` en todas ellas menos en una, en la que he hecho las mediciones, que ha venido a ser un `ps` esteroides:

```sh
ps -e -o rss,comm= | grep urxvt | egrep " [0-9]{4}" | cut -d' ' -f2 > /tmp/cosas.txt; =0; for i in `cat /tmp/cosas.txt`; do echo =$(($+$i)); done; echo "$(($/1024))MB"
11MB
```

Un "one liner" bonico der to. Con la anterior orden se mira todos los procesos mostrando sólo la RAM y el nombre del proceso que los usa con `ps`, luego se filtra con `grep` buscando urxvt y luego solo los que usen una RAM con un número de 4 dígitos, ya que cada proceso de urxvt tiene otro colgando, que es el que nos interesa. Luego se coge el valor de rss solamente y se vuelva en ese archivo. Se lee, se suma y se divide entre 1024, ya que los datos están en KB y los queremos en MB para que sea más visible.

Pues bien, vemos que devuelve 11MB. Es poco, pero usando `urxvtd`, que es el daemon, se consigue bajar a 3MB. Cuando se tienen 512MB, no es poca diferencia, sobretodo si se hace algo más que usar un simple htop.

El modo de uso es muy sencillo. Se tiene que meter en `/usr/local/bin/terminal`, por ejemplo, el siguiente script:

```sh
#!/bin/sh

comprueba="$(ps aún | grep urxvtd | grep -v grep)"

if [[ -z $comprueba ]]
then
urxvtd -q -o -f
urxvtc
else
urxvtc
fi
```

Lo que hace es lanzar `urxvtd` si no hay ningún demonio funcionando, sino solamente lanza el cliente, que es `urxvtc`. Esto tiene el problema de que si peta `urxvtd`, petan todas las terminales. Pero no es algo que pase habitualmente, por lo que no debería ser un problema. Yo soy mucho más feliz con esto, desde luego. Sólo queda darle permisos de ejecución a este script y asignarlo a `META+RET` en el archivo de configuración de `i3-wm`.
