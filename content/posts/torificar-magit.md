+++
title = "Torificar magit"
author = ["drymer"]
date = 2015-09-29T20:41:00+02:00
tags = ["tor", "git", "emacs"]
draft = false
+++

Pues me he dado cuenta de que en emacs, y con la programación en general, no me van a salir articulos tan largos cómo tocando temas de administración de sistemas. Y dado que ahora me estoy dedicando más a eso, haré algunos articulos absurdamente cortos pero que creo que pueden ser útiles.

[Magit](https://github.com/magit/magit) es un frontend de git para emacs, y es absolutamente maravilloso. Permite hacer cosas que de hecho aún ni sé ni necesito hacer, lo cual tampoco es tan raro. Se puede instalar con package.el, por lo que, si no lo tenemos, ponemos en nuestro .emacs:

```lisp
(require 'package)
(add-to-list 'package-archives
   '("melpa" . "http://melpa.org/packages/") t)
```

Y luego ejecutamos M-x package-install RET magit.

Hecho esto, la única configuración que necesita es la de la torificación, si se quiere. Magit tiene una variable en la que te deja escoger que binario de git quieres usar, de darse el caso de que hay varios o no está en el path. Es la siguiente:

```lisp
(setq magit-git-executable "/usr/bin/git")
```

Pero no permite poner, por ejemplo:

```lisp
(setq magit-git-executable "/usr/bin/torify /usr/bin/git")
; o
(setq magit-git-executable "torify /usr/bin/git")
```

Por lo que simplemente crearemos un wraper alrededor de todo el binario. En /usr/local/bin/tgit pondremos:

```sh
#!/bin/bash

torify /usr/bin/git "$@"
```

Y entonces sólo queda poner en el .emacs lo siguiente:

```lisp
(setq magit-git-executable "/usr/local/bin/tgit")
```

Y a disfrutar de magit torificado.
