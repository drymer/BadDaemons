+++
title = "Las fotopollas"
author = ["drymer"]
date = 2015-04-27T19:15:00+02:00
draft = false
tags = ["privacidad", "elbinario"]
+++
El otro día estaba viendo un programa llamado "Last Week Tonight With John Oliver" y me llamó la atención algo. Para empezar, es muy recomendable ver este vídeo por que el tipo tiene bastante gracia. El programa trata noticias de la actualidad pero metiéndole toques de humor, algo así cómo El Intermedio pero siendo gracioso.

En fin, la cosa es que en este programa entrevistan a Edward Snowden. El tal John se pega un viaje de 12 horas a Russia sólo para poder hacerlo. Otro motivo más que añadir para ver este vídeo.

Pero a lo que voy no esta entrevista en si y el tema que tratan. Probablemente esa noche muchos americanos entendieron lo que implicaba el espionaje masivo, y lo John lo hizo posible de una manera muy concreta. Puso todo el contexto en las fotopollas. Da que pensar. A veces nos olvidamos de que hay ciertas cosas que para los que no están iniciadas en el arcano arte de darle al botón de reiniciar, lo que decimos a veces no tiene sentido. Porqué, y esto es una realidad, la mayor parte de gente no quiere aprender. El ejemplo lo ponen en el vídeo, cito textualmente:

<quote>Es cómo cuando estás en la oficina y viene el informático y tu dices: "Oooooh, mieeeeeeerda. No me enseñes nada, no quiero aprender. Hueles a cáncer.</quote>

Y es que no es nada sencillo. Por mucho que algunos entendamos ciertos aspectos técnicos, comprimir esos conocimientos en menos de, pongamos diez minutos, es muy complicado. Y ahí entra John. Decide intentar ayudar a Snowden explicando el problema del espionaje masivo externo a EEUU. Y lo hace en el contexto de las mencionadas fotopollas. Eso es a lo que la gente le tiene miedo, a que la NSA le vea la polla. Por que la gente manda fotos de su polla, tetas y otros interesantes apéndices constantemente, y es de esas cosas de las que, evidentemente, no se habla (exceptuando el caballero negro que sale en el vídeo), pero están ahí. En el minuto 11:16 podemos ver la respuesta de la gente entrevistada. Y se ve que no les gusta que les miren sus partes íntimas, que cosas. En ese contexto, la gente entrevistada pide que hayan leyes que controlen el uso de todos los programas de espionaje que la NSA tiene.

Después de esto, por eso, les preguntan si creen que existe un programa dedicado a capturar las fotopolla, a lo que responden que no. Pero la realidad es que, aunque no haya un programa exclusivo dedicado a eso, si que las capturan. Y en tal contexto, los siguientes programas de la NSA que se han ido explicando:

<ul>
<li>Amendment Act Sección 702: Esta "ley" permite el espionaje y captura masivo de todo el tráfico que la NSA pueda capturar. Una fotopolla enviada por correo de gmail que en algún momento sale a algún servidor fuera de EEUU es susceptible de ser capturado.</li>
<li>Orden Ejecutiva 12333: Es lo que la NSA usa cuando las demás autoridades no recolectan los suficientes datos para ellos. Dan órdenes de mover las fotopollas de servidores de dentro de EEUU a unos que estén fuera para poder así, legalmente, capturar esas fotos.</li>
<li>Prism: Es lo que usan para sacar la fotopolla del servidor de Google con la ayuda de Google. Prism les obliga a colaborar tanto si quieren cómo sinó, convirtiéndolos en los sheriffs de las fotopollas.</li>
<li>Upstream: Es cómo roban las fotopollas de Google./li>
<li>Mystic: Si describes tu polla en una llamada telefónica, tienen la duración de esa llamada para todos los paises y el contenido de esta en algunos paises.</li>
<li>Patriot Act 215: Lo relativo al móvil. A quien le mandas fotopollas, cuantas veces lo haces y el tamaño de la fotopolla. Se incluyen también las llamadas a lineas calientes.</li>
</ul>

Y esto es, en resumen, lo que hace la NSA últimamente. Sencillo de entender, no? Y ahí está el tema. **Debemos** aprender a simplificar. Al menos, si queremos que nos entiendan.

El vídeo lo podéis descargar desde nuestro mediagoblin.

<https://goblinrefuge.com/mediagoblin/u/elbinario/m/last-week-tonight-with-john-oliver-edward-snowden-interview/>

