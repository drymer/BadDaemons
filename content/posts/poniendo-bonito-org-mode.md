+++
title = "Poniendo bonito org-mode"
author = ["drymer"]
date = 2016-04-07T16:23:00+02:00
tags = ["orgmode", "emacs"]
draft = false
+++

`org-mode`, cómo ya se ha comentado antes, es de lo mejor que hay en emacs y con mucha diferencia. No ya tiene sentido comentar para que sirve, para ver sus posibilidades, se puede mirar en [articulos anteriores](http://daemons.cf/categories/emacs/). Al lío.


## Los asteriscos son feos {#los-asteriscos-son-feos}

Más claro el agua. Para cambiarlos, se usará [org-bullet](https://github.com/sabof/org-bullet). Esto hace que las cabeceras no empiecen por un asterisco cutre, sinó que empiezan por una bola utf-8. Instalarlo es muy fácil, se puede tirar de `elpa` y ejecutar `M-x package-install RET org-bullet RET`. Y sólo queda añadirlo al `init.el`

```emacs-lisp
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
```

La primera línea carga el paquete, la segunda hace que `org-bullets-mode` se active únicamente en los buffers con `org-mode` activo.


## Cabeceras más grandes {#cabeceras-más-grandes}

Otra cosa que lo mejora mucho es diferenciar el tamaño las cabeceras o árboles. En mi caso concreto, esto no lo necesito, ya que uso el tema llamado `cyberpunk-theme` que ya lo hace de manera automática. Pero para quien su tema no lo haga, puede guardar las siguientes funciones `lisp` en su `init.el`.

```emacs-lisp
(custom-theme-set-faces 'user
                      `(org-level-8 ((t (,@headline ,@variable-tuple))))
                      `(org-level-7 ((t (,@headline ,@variable-tuple))))
                      `(org-level-6 ((t (,@headline ,@variable-tuple))))
                      `(org-level-5 ((t (,@headline ,@variable-tuple))))
                      `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
                      `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
                      `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
                      `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
                      `(org-document-title ((t (,@headline ,@variable-tuple :height 1.5 :underline nil))))))
```


## Esconder los elementos de marcado {#esconder-los-elementos-de-marcado}

Hablo de los símbolos de cursiva, negrita o subrallado. Al definir la siguiente variable, se pasará de tener:

```text
*Hola*
/pequeño/
_mundo_
```

A ver el marcado resaltado, pero sin esos símbolos.

```org
*Hola*
/pequeño/
_mundo_
```

Esto se consigue añadiendo lo siguiente a `init.el`

```emacs-lisp
(setq org-hide-emphasis-markers t)
```

Y si se quiere modificar la manera en la que se muestra algún marcado en concreto, se puede usar lo siguiente:

```emacs-lisp
(add-to-list 'org-emphasis-alist
           '("=" (:foreground "DimGrey")
             ))
```

En este caso aumenta ligeramente la oscuridad del color del símbolo de marcado "literal" (verbatim en inglés). También se puede usar el atributo `:background`, por ejemplo, si se quiere resaltar el fondo de la pantalla.


## Centrar el texto en pantalla {#centrar-el-texto-en-pantalla}

A mi esto me parecía una gilipollez, hasta que me he encontrado usando una pantalla bastante grande en el trabajo. En mi casa son todas relativamente pequeñas y no entendía que aportaba el modo que comentaré a continuación, pero ya lo veo claro. Para ajustar el buffer al centro de pantalla, se puede usar un modo llamado `olivetti-mode`, que suena muy de hipsters, pero viene bien. A continuación pondré dos capturas, la primera sin este modo activa y la segunda estando activo.

{{< figure src="/img/sin-olivetti.PNG" caption="Figura 1: Sin olivetti-mode" >}}

{{< figure src="/img/con-olivetti.PNG" caption="Figura 2: Con olivetti-mode" >}}

Para instalarlo y configurarlo:

```emacs-lisp
M-x package-install RET olivetti-mode RET
M-x olivetti-mode
;; Hacerlo grande con:
C-]
;; Hacerlo pequeño con:
C-[
;; Cuando se llegue al tamaño deseado, hay que mirar el minibuffer messages y fiarse en que valor númerico sale
;; Entonces, se define la variable olivetti-set-width al tamaño que se ha visto
(olivetti-set-width 100)
```


## Indentar jerárquicamente {#indentar-jerárquicamente}

Esto no tiene mucho misterio. Activando el siguiente modo cuando se carga `org-mode`, se hará que se indenten todos los sub-arboles y texto que estos contengan. Se puede ver en las siguientes capturas.

{{< figure src="/img/indentando.PNG" caption="Figura 3: Sin indentar" >}}

{{< figure src="/img/sin-indentar.PNG" caption="Figura 4: Indentando" >}}

Para activarlo, sólo hay que añadir lo siguiente al archivo de configuración:

```emacs-lisp
(org-indent-mode)
```

Y con esto ya se tiene un `org-mode` más bonito.

PD: Las cabeceras de las capturas se ven del mismo tamaño que el resto del texto por que estoy usando emacs por terminal, algo que no suelo hacer. Cuando lo uso con la interfaz gráfica, se ven cómo he comentado.

[Fuente](http://www.howardism.org/Technical/Emacs/orgmode-wordprocessor.html)
