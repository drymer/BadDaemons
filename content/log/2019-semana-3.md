+++
title = "Semana 3 del año 2019"
author = ["drymer"]
publishDate = 2019-01-20T18:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [15 Useful Helm Charts Tools - DZone Cloud](https://dzone.com/articles/15-useful-helm-charts-tools): tiene que ver con nubecitas, kubernetes y se puede leer un{os} 4 minuto{s}
-   [r/orgmode - Using org-mode for novel writing/creative writing](https://www.reddit.com/r/orgmode/comments/ae5str/using_orgmode_for_novel_writingcreative_writing/): tiene que ver con emacs y se puede leer un{os} 4 minuto{s}
-   [Managing Secrets in Kubernetes](https://www.weave.works/blog/managing-secrets-in-kubernetes): tiene que ver con kubernetes y se puede leer un{os} 5 minuto{s}
-   [Why Is Storage On Kubernetes So Hard?](https://softwareengineeringdaily.com/2019/01/11/why-is-storage-on-kubernetes-is-so-hard/): tiene que ver con kubernetes y se puede leer un{os} 7 minuto{s}
-   [I'm leaving GitLab to help everyone work remotely](https://www.linkedin.com/pulse/im-leaving-gitlab-help-everyone-work-remotely-job-van-der-voort/): tiene que ver con git y se puede leer un{os} 4 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Mujeres que no renuncian al sexting: estas hackers te enseñan a hacerlo de forma más segura](https://www.eldiario.es/tecnologia/Sexting-feminista_0_846215574.html): tiene que ver con feminismo y se puede leer un{os} 7 minuto{s}
-   [My Org Capture Templates - Part 1 · The Art of Not Asking Why](https://joshrollinswrites.com/emacsorg/org-capture-template-1/): tiene que ver con org-mode y se puede leer en un{os} 3 minuto{s}
-   [“La protesta de los chalecos amarillos tiene que ver con la vida, la gente dice: ‘No conseguimos vivir así’”](https://www.elsaltodiario.com/chalecos-amarillos/la-protesta-de-los-chalecos-amarillos-tiene-que-ver-con-la-vida-dicen-no-conseguimos-vivir-asi): tiene que ver con libertades y se puede leer en un{os} 19 minuto{s}
-   [Una película de antifascismo, la trampa de la diversidad y el pianista José María Aznar](https://www.elsaltodiario.com/el-blog-de-el-salto/antifascismo-trampa-diversidad-pianista-jose-maria-aznar): tiene que ver con libertades y se puede leer en un{os} 10 minuto{s}
-   [The antisocial laptop](http://scattered-thoughts.net/blog/2019/01/16/the-antisocial-laptop/): tiene que ver con hardware y se puede leer en un{os} 2 minuto{s}
-   [2019-01-14 Some Org Agenda keybindings](http://mbork.pl/2019-01-14_Some_Org_Agenda_keybindings): tiene que ver con org-mode y se puede leer en un{os} 1 minuto{s}
-   [El anuncio de cuchillas que cuestiona la masculinidad tóxica por el que muchos hombres llaman al boicot](https://www.eldiario.es/micromachismos/anuncio-cuchillas-cuestiona-masculinidad-hombres_6_857474247.html): tiene que ver con masculinidades disidentes y se puede leer en un{os} 2 minuto{s}
-   [Bashfuscator/Bashfuscator](https://github.com/Bashfuscator/Bashfuscator/blob/master/README.md): tiene que ver con bash, ofuscador y se puede leer en un{os} 4 minuto{s}
-   [Collins - Infrastructure Management for Engineers](https://tumblr.github.io/collins/): tiene que ver con ingenieria y se puede leer en un{os} 7 minuto{s}
-   [Bash-5.0 release available](http://lists.gnu.org/archive/html/bug-bash/2019-01/msg00063.html): tiene que ver con bash y se puede leer en un{os} 10 minuto{s}
-   [How To Ask Questions The Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html): tiene que ver con la vida y se puede leer en un{os} 55 minuto{s}
-   [Globally scoped platform ARG's in Docker BuildKit](http://sujayopillai.com/2019/01/02/docker-platform-args.html?mkt_tok=eyJpIjoiTjJJd1lUQmhNVGN3TVRrMyIsInQiOiJrQ1FYdlZxUTFlbDl6VktkMVdubTZiUktVRWVUdlV3YVpLcjlDbm1BV2J2Q1F6ZHlkR0VRa0k1WUtcL2gxSFpDVWM3ZnZSaXZUS2dtcFZraHplcWZVK0lPVTJqbDNxaU5aaHd1MFNMQVRCOUQrY3VsdUJPS0QzZ1grQ1BRenVKUksifQ==): tiene que ver con docker y se puede leer en un{os} 1 minuto{s}
-   [r/emacs - My First package: ecloud. A magit-style interface to access Azure, AWS and GCP.](https://www.reddit.com/r/emacs/comments/aeps9n/my_first_package_ecloud_a_magitstyle_interface_to/): tiene que ver con emacs, nubecitas, git y se puede leer en un{os} 0 minuto{s}
-   [r/orgmode - OrgModeWeb - a new web app for org-mode](https://www.reddit.com/r/orgmode/comments/adkck8/orgmodeweb_a_new_web_app_for_orgmode/): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Git Reset 101  Hacker Noon](https://hackernoon.com/reset-101-ba05d9e3f2c7?source=rss----3a8144eabfe3---4&gi=815149ccedd2): tiene que ver con git y se puede leer en un{os} 6 minuto{s}
-   [‘The Age of Surveillance Capitalism’ Review: The New Big Brother](https://www.wsj.com/articles/the-age-of-surveillance-capitalism-review-the-new-big-brother-11547509779): tiene que ver con productividad y se puede leer en un{os} 5 minuto{s}
-   [r/devops - What's your monitoring and alerting stack look like?](https://www.reddit.com/r/devops/comments/afqye3/whats_your_monitoring_and_alerting_stack_look_like/): tiene que ver con devops y se puede leer en un{os} 0 minuto{s}
