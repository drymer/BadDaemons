+++
title = "Semana 37 del año 2018"
author = ["drymer"]
publishDate = 2018-09-16T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [I broke login for 80k users](https://dev.to/rhymes/i-broke-login-for-80k-users-3bjm): tiene que ver con nada en general y se puede leer un{os} 5 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Mantener Git limpio](https://labrecha.digital/blog/git-limpio/): tiene que ver con git y se puede leer en un{os} 2 minuto{s}
-   [Security firm uses Twitter to disclose critical zero-day flaw in Tor Browser](https://www.hackread.com/security-firm-uses-twitter-to-disclose-critical-zero-day-flaw-in-tor-browser): tiene que ver con seguridad y se puede leer en un{os} 2 minuto{s}
-   [hjacobs/kube-ops-view](https://github.com/hjacobs/kube-ops-view/blob/master/README.rst): tiene que ver con kubernetes y se puede leer en un{os} 4 minuto{s}
-   [astefanutti/kubebox](https://github.com/astefanutti/kubebox): tiene que ver con kubernetes y se puede leer en un{os} 2 minuto{s}
