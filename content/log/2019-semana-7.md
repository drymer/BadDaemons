+++
title = "Semana 7 del año 2019"
author = ["drymer"]
publishDate = 2019-02-10T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Cinco razones por las que los derechos animales son una cuestión feminista](https://www.elsaltodiario.com/antiespecismo/cinco-razones-por-las-que-los-derechos-animales-son-una-cuestion-feminista-): tiene que ver con veganismo, fenimismo y se puede leer un{os} 10 minuto{s}
-   [When AWS Autoscale Doesn’t](https://segment.com/blog/when-aws-autoscale-doesn-t/): tiene que ver con aws y se puede leer un{os} 9 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Benefits of a daily diary and topic journals](https://sivers.org/dj): tiene que ver con productividad y se puede leer en un{os} 5 minuto{s}
-   [Fighting the Surveillance Economy](https://irreal.org/blog/?p=7826): tiene que ver con privacidad y se puede leer en un{os} 1 minuto{s}
