+++
title = "Semana 51 del año 2018"
author = ["drymer"]
publishDate = 2018-12-23T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Enfadados con todo: Vox y la masculinidad](https://www.elsaltodiario.com/vox/enfadados-con-todo-vox-masculinidad): tiene que ver con feminismo, masculinidades disidentes y se puede leer un{os} 8 minuto{s}
-   [How to ask good questions](https://jvns.ca/blog/good-questions/): tiene que ver con aprender y se puede leer un{os} 11 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Front-End Micro Services](https://jobs.zalando.com/tech/blog/front-end-micro-services/): tiene que ver con microservicios y se puede leer en un{os} 3 minuto{s}
-   [A blogging style guide](https://robertheaton.com/2018/12/06/a-blogging-style-guide/): tiene que ver con escribir y se puede leer en un{os} 6 minuto{s}
