+++
title = "Semana 15 del año 2019"
author = ["drymer"]
publishDate = 2019-04-14T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [The niceties of evil in Doom Emacs](https://medium.com/@aria_39488/the-niceties-of-evil-in-doom-emacs-cabb46a9446b): tiene que ver con emacs y se puede leer un{os} 2 minuto{s}
-   [Automating Security Compliance with Ansible: DevSecOps made Easy | Tandem](https://madeintandem.com/blog/automating-security-compliance-ansible-devsecops-made-easy/): tiene que ver con ansible y se puede leer un{os} 5 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Leadership CI](https://blog.jessfraz.com/post/leadership-ci/): tiene que ver con git y se puede leer en un{os} 4 minuto{s}
-   [What Is the INFJ Door Slam, and Why Do INFJs Do It?](https://introvertdear.com/news/infj-door-slam-things-you-should-know/): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [The Truth Seekers](https://blog.jessfraz.com/post/the-truth-seekers/): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [Amid employee uproar, Microsoft is investigating sexual harassment claims overlooked by HR](https://qz.com/1587477/microsoft-investigating-sexual-harassment-claims-overlooked-by-hr/): tiene que ver con feminismo y se puede leer en un{os} 4 minuto{s}
-   [S3 compatible storage on Kubernetes](https://karrier.io/blog/minio-on-kubernetes/): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [¿Por qué hay más hombres víctimas de suicidio pero más mujeres que lo intentan?](https://www.bbc.com/mundo/vert-fut-47694296): tiene que ver con feminismo y se puede leer en un{os} 10 minuto{s}
-   [How to instrument code: Custom metrics vs APM vs OpenTracing](https://sysdig.com/blog/how-to-instrument-code-custom-metrics-vs-apm-vs-opentracing/): tiene que ver con monitorizacion y se puede leer en un{os} 7 minuto{s}
