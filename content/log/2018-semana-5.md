+++
title = "Semana 5 del año 2018"
author = ["drymer"]
publishDate = 2018-02-04T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [DevOps: Boring Is the New Black](https://dzone.com/articles/boring-is-the-new-black): tiene que ver con devops y se puede leer en un{os} 9 minuto{s}
-   [Testing of Microservices](https://labs.spotify.com/2018/01/11/testing-of-microservices/): tiene que ver con microservicios, testing y se puede leer en un{os} 7 minuto{s}
-   [Docker Security](https://dzone.com/refcardz/docker-security): tiene que ver con docker, seguridad y se puede leer en un{os} 16 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [The Microservices Paradox](https://dzone.com/articles/the-microservices-paradox): tiene que ver con microservicios y se puede leer en un{os} 4 minuto{s}
-   [How can I improve startup time despite many packages?](https://emacs.stackexchange.com/questions/38368/how-can-i-improve-startup-time-despite-many-packages): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [Why Social Situations Exhaust Introverts: A Programmer's Take](https://dzone.com/articles/why-social-situations-exhaust-introverts-a-program): tiene que ver con nubecitas y se puede leer en un{os} 9 minuto{s}
-   [Emacs as DevOps editor](https://www.reddit.com/r/emacs/comments/7u45lv/emacs_as_devops_editor/): tiene que ver con emacs, devops y se puede leer en un{os} 0 minuto{s}
-   [Org-noter - A synchronized, Org-mode, document annotator](https://www.reddit.com/r/orgmode/comments/7txcie/orgnoter_a_synchronized_orgmode_document_annotator/): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Learning Docker Networking](https://hackernoon.com/learning-docker-networking-part-1-53b09fda9078?gi=dd82c8046fd): tiene que ver con docker y se puede leer en un{os} 3 minuto{s}
-   [Cloud Shell Tutorials: Learning experiences integrated into the Cloud Console](https://cloudplatform.googleblog.com/2018/01/Cloud-Shell-Tutorials-learning-experiences-integrated-into-the-Cloud-Console.html?m=1): tiene que ver con nubecitas y se puede leer en un{os} 1 minuto{s}
-   [Women in DevOps: Nicole Munro](https://dzone.com/articles/women-in-devops-nicole-munro): tiene que ver con devops y se puede leer en un{os} 4 minuto{s}
-   [Kubernetes Resource Usage: How Do You Manage and Monitor It?](https://dzone.com/articles/kubernetes-resource-usage-how-do-you-manage-and-mo): tiene que ver con kubernetes y se puede leer en un{os} 6 minuto{s}
-   [Learning Docker Networking](https://medium.com/devopslinks/learning-docker-networking-part-2-655dab37de4f): tiene que ver con docker, devops y se puede leer en un{os} 1 minuto{s}
-   [Finding locks](https://www.reddit.com/r/lockpicking/comments/7u65ui/finding_locks/): tiene que ver con lockpicking y se puede leer en un{os} 0 minuto{s}
-   [Long-time GTD users, what works for you in being productive? What doesn't?](https://www.reddit.com/r/emacs/comments/7txqh8/longtime_gtd_users_what_works_for_you_in_being/): tiene que ver con productividad y se puede leer en un{os} 7 minuto{s}
