+++
title = "Semana 9 del año 2018"
author = ["drymer"]
publishDate = 2018-03-04T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Meet Fiona, the pen testing pen.](https://www.reddit.com/r/lockpicking/comments/7zvv8p/meet_fiona_the_pen_testing_pen/): tiene que ver con seguridad y se puede leer en un{os} 2 minuto{s}
-   [Mike Newswanger - Building a Secure Public Key Infrastructure for Kubernetes](https://www.mikenewswanger.com/posts/2018/kubernetes-pki/): tiene que ver con kubernetes y se puede leer en un{os} 11 minuto{s}
-   [Understanding When to use RabbitMQ or Apache Kafka](https://content.pivotal.io/blog/understanding-when-to-use-rabbitmq-or-apache-kafka): tiene que ver con colas y se puede leer en un{os} 10 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [elastic/beats-docker](https://github.com/elastic/beats-docker/tree/6.2): tiene que ver con docker y se puede leer en un{os} 0 minuto{s}
-   [Sprint Zero](https://dzone.com/articles/sprint-zero): tiene que ver con productividad y se puede leer en un{os} 3 minuto{s}
-   [How to Manage Your Time and Focus on What Really Matters](https://dzone.com/articles/how-to-manage-your-time-and-focus-on-what-really-m): tiene que ver con productividad y se puede leer en un{os} 3 minuto{s}
-   [Automator workflow for emacsclient](https://www.reddit.com/r/emacs/comments/80d0z3/automator_workflow_for_emacsclient/): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Draft | Streamlined Kubernetes Development](https://draft.sh/): tiene que ver con kubernetes y se puede leer en un{os} 0 minuto{s}
-   [Spring Boot 2.0 New Features: Infrastructure Changes - DZone Java](https://dzone.com/articles/spring-boot-20-new-features-infrastructure-changes): tiene que ver con java y se puede leer en un{os} 5 minuto{s}
-   [Org-Chef: manage and extract recipes with org-mode](https://www.reddit.com/r/emacs/comments/81tgzh/orgchef_manage_and_extract_recipes_with_orgmode/): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [steckerhalter/steckemacs.el](https://github.com/steckerhalter/steckemacs.el/blob/2e90a717066cbfa9408b5d9ab19d09a0fee099ab/steckemacs.el): tiene que ver con emacs y se puede leer en un{os} 42 minuto{s}
-   [My weapon against Emacs Pinky](https://www.reddit.com/r/emacs/comments/7zvw2b/my_weapon_against_emacs_pinky/): tiene que ver con emacs y se puede leer en un{os} 8 minuto{s}
-   [Navigate yaml using emacs-libyaml dynamic module](https://www.reddit.com/r/emacs/comments/803az3/navigate_yaml_using_emacslibyaml_dynamic_module/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [It Takes a DevSecOps Village](https://dzone.com/articles/it-takes-a-devsecops-village): tiene que ver con devops, seguridad y se puede leer en un{os} 3 minuto{s}
-   [What is Socks 5 & What are the advantages of using Socks 5 Proxies -](http://limeproxies.com/blog/socks-5-advantages-using-socks-5-proxies/): tiene que ver con redes y se puede leer en un{os} 4 minuto{s}
-   [Heavy note-takers and programmers, do you use the same font for both?](https://www.reddit.com/r/emacs/comments/80m808/heavy_notetakers_and_programmers_do_you_use_the/): tiene que ver con productividad y se puede leer en un{os} 4 minuto{s}
-   [A Nice Mu4e Configuration](http://irreal.org/blog/?p=6985): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [exwm version 0.17 - looks like things have been happening](https://www.reddit.com/r/emacs/comments/80sm7t/exwm_version_017_looks_like_things_have_been/): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [El 'gurú' de Telefónica, ante las dudas de su megaproyecto: "Somos la teleco más valiente"](https://www.elconfidencial.com/tecnologia/2018-02-27/chema-alonso-telefonica-aura-cuarta-plataforma-movistar_1527426/): tiene que ver con nada en general y se puede leer en un{os} 8 minuto{s}
-   [Org Spreadsheet Cheat Sheet](http://irreal.org/blog/?p=6983): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Monitoring with Prometheus](https://dzone.com/articles/monitoring-with-prometheus): tiene que ver con prometheus y se puede leer en un{os} 10 minuto{s}
-   [DevSecOps Is a Key to Cost Reduction](https://dzone.com/articles/devsecops-is-a-key-to-cost-reduction): tiene que ver con devops, seguridad y se puede leer en un{os} 3 minuto{s}
