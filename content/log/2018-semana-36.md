+++
title = "Semana 36 del año 2018"
author = ["drymer"]
publishDate = 2018-09-09T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Robust Notes with Embedded Code](https://www.arcadianvisions.com/blog/2018/org-nix-direnv.html): tiene que ver con nada en general y se puede leer un{os} 1 minuto{s}
-   [Why You Should Tell Your Co-Workers How Much Money You Make](https://www.nytimes.com/2018/08/31/smarter-living/pay-secrecy-national-labor-rights-act.html): tiene que ver con nada en general y se puede leer un{os} 0 minuto{s}
-   [Software Engineering Daily - GitOps Key Takeaways](https://www.weave.works/blog/software-engineering-daily-gitops-key-takeaways): tiene que ver con git y se puede leer un{os} 4 minuto{s}
-   [aquasecurity/kube-hunter](https://github.com/aquasecurity/kube-hunter): tiene que ver con kubernetes, seguridad y se puede leer un{os} 4 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Literate Programming for Programming Notes](http://irreal.org/blog/?p=7451): tiene que ver con programacion y se puede leer en un{os} 1 minuto{s}
-   [La habitación que aprendió a hablar chino](http://lapiedradesisifo.com/2013/10/31/la-habitación-que-aprendió-a-hablar-chino/): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [Unit Testing: The Good, Bad, and Ugly](https://dzone.com/articles/unit-testing-the-good-bad-amp-ugly): tiene que ver con nada en general y se puede leer en un{os} 4 minuto{s}
-   [knative/build](https://github.com/knative/build/blob/master/README.md): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [¿En qué consiste y para qué sirve un zombi filosófico?](http://lapiedradesisifo.com/2013/11/05/en-qué-consiste-y-para-qué-sirve-un-zombi-filosófico/): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [Monitoring and Logging Strategies with Docker Enterprise - Docker Blog](https://blog.docker.com/2018/08/proactive-ops-for-container-orchestration-environments/): tiene que ver con docker y se puede leer en un{os} 3 minuto{s}
-   [aquasecurity/kube-bench](https://github.com/aquasecurity/kube-bench): tiene que ver con kubernetes y se puede leer en un{os} 5 minuto{s}
