+++
title = "Semana 6 del año 2018"
author = ["drymer"]
publishDate = 2018-02-11T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [At what point, or in what situation, should one adopt a tool like consul?](https://www.reddit.com/r/devops/comments/7wdb6b/at_what_point_or_in_what_situation_should_one/): tiene que ver con microservicios y se puede leer en un{os} 3 minuto{s}
-   [Respuestas: ¿Por qué todos los planetas orbitan el sol en el mismo sentido?](http://cienciadesofa.com/2018/02/respuestas-lxxxvii-por-que-todos-los-planetas-orbitan-el-sol-en-el-mismo-sentido.html): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [The Advantages of Blogging for an Emacs User](http://irreal.org/blog/?p=6944): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [Vim documentation: motion](http://vimdoc.sourceforge.net/htmldoc/motion.html): tiene que ver con evil-mode y se puede leer en un{os} 42 minuto{s}
-   [Keeping an Engineering Notebook](http://dankleiman.com/2018/01/28/keeping-an-engineering-notebook/): tiene que ver con productividad y se puede leer en un{os} 3 minuto{s}
-   [PeerTube](https://video.tedomum.net/videos/search?search=FOSDEM18): tiene que ver con redes libres y se puede leer en un{os} 0 minuto{s}
-   [Another Take on Engineering Notebooks](http://irreal.org/blog/?p=6938): tiene que ver con productividad y se puede leer en un{os} 1 minuto{s}
-   [Helm vs Ivy: What are the differences, what are the advantages?](https://www.reddit.com/r/emacs/comments/7vcrwo/helm_vs_ivy_what_are_the_differences_what_are_the/): tiene que ver con emacs y se puede leer en un{os} 5 minuto{s}
-   [UX and UI: Poles Apart](https://dzone.com/articles/ux-amp-ui-poles-apart): tiene que ver con nubecitas y se puede leer en un{os} 4 minuto{s}
-   [The DevOps Scorecard](https://devops.com/devops-scorecard/): tiene que ver con devops y se puede leer en un{os} 3 minuto{s}
-   [Why Metrics Must Guide Your DevOps Initiative](https://dzone.com/articles/why-metrics-must-guide-your-devops-initiative-blog): tiene que ver con devops y se puede leer en un{os} 2 minuto{s}
