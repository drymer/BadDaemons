+++
title = "Semana 10 del año 2018"
author = ["drymer"]
publishDate = 2018-03-11T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Prometheus and Kubernetes: Monitoring Your Applications](https://www.weave.works/blog/prometheus-and-kubernetes-monitoring-your-applications/): tiene que ver con kubernetes, monit, prometheus y se puede leer en un{os} 7 minuto{s}
-   [Provisioning and Lifecycle of a Production Ready Kubernetes Cluster](https://www.weave.works/blog/provisioning-lifecycle-production-ready-kubernetes-cluster/): tiene que ver con kubernetes y se puede leer en un{os} 11 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Women in DevOps: Tessa and Ellis](https://dzone.com/articles/women-in-devops-tessa-amp-ellis): tiene que ver con devops y se puede leer en un{os} 5 minuto{s}
-   [The Agile Fluency Model](https://martinfowler.com/articles/agileFluency.html): tiene que ver con nubecitas y se puede leer en un{os} 32 minuto{s}
-   [Ensure High Availability and Uptime With Kubernetes Horizontal Pod Autoscaler and Prometheus](https://dzone.com/articles/ensure-high-availability-and-uptime-with-kubernete): tiene que ver con kubernetes y se puede leer en un{os} 7 minuto{s}
-   [Alex Schroeder: 2018-02-27 Emacs Packages](https://alexschroeder.ch/wiki/2018-02-27_Emacs_Packages): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Anybody use org-mode to track bad habits?](https://www.reddit.com/r/orgmode/comments/81vhgb/anybody_use_orgmode_to_track_bad_habits/): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [Using exclusion patterns when grepping · (or emacs](https://oremacs.com/2018/03/05/grep-exclude/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [Using JWT For Sessions - Bozho's tech blog](https://techblog.bozho.net/using-jwt-sessions/): tiene que ver con programacion y se puede leer en un{os} 4 minuto{s}
-   [Week ending 2018-03-02 ](http://sachachua.com/blog/2018/03/week-ending-2018-03-02/): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [News in org?](https://www.reddit.com/r/emacs/comments/828plb/news_in_org/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [1.3Tbps DDOS Attack on GITHUB | Blame: memcached?](https://www.reddit.com/r/Information_Security/comments/824lpz/13tbps_ddos_attack_on_github_survived_blame/): tiene que ver con git, sysadmin y se puede leer en un{os} 0 minuto{s}
