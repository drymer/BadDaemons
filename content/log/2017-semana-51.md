+++
title = "Semana 51 del año 2017"
author = ["drymer"]
publishDate = 2017-12-24T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Chaos Engineering 101  Production Ready](https://medium.com/production-ready/chaos-engineering-101-1103059fae44): tiene que ver con caos y se puede leer en un{os} 6 minuto{s}
-   [Org Mode - Organize Your Life In Plain Text!](http://doc.norang.ca/org-mode.html): tiene que ver con org-mode y se puede leer en un{os} 3 minuto{s}
-   [Evil Collection has hit MELPA: Enjoy the full Vim experience!](https://www.reddit.com/r/emacs/comments/7loyln/evil_collection_has_hit_melpa_enjoy_the_full_vim/): tiene que ver con emacs, evil-mode y se puede leer en un{os} 5 minuto{s}
-   [Principles of Chaos Engineering](http://principlesofchaos.org/): tiene que ver con nada en general y se puede leer en un{os} 4 minuto{s}
-   [Elasticsearch for Dummies](https://dzone.com/articles/elasticsearch-for-dummies): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [ChAP: Chaos Automation Platform  Netflix TechBlog](https://medium.com/netflix-techblog/chap-chaos-automation-platform-53e6d528371f): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [¿QUÉ PASA CON LA VITAMINA D?](http://www.dimequecomes.com/2017/06/vitamina-d.html): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}
-   [VITAMINA A, VEGETARIANOS Y PROBLEMAS FANTASMA](http://www.dimequecomes.com/2017/08/vitamina-A-vegetarianos.html): tiene que ver con nada en general y se puede leer en un{os} 4 minuto{s}
-   [PREGUNTAS FRECUENTES SOBRE LA B12](http://www.dimequecomes.com/2017/08/FAQ-preguntas-frecuentes-B12.html): tiene que ver con nada en general y se puede leer en un{os} 8 minuto{s}
-   [Personal Infrastructure](https://blog.jessfraz.com/post/personal-infrastructure/): tiene que ver con docker, ci/cd y se puede leer en un{os} 4 minuto{s}
-   [1.4 billion password breach compilation wordlist](https://gist.github.com/scottlinux/9a3b11257ac575e4f71de811322ce6b3): tiene que ver con seguridad y se puede leer en un{os} 0 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [15 Great Programming Blogs You May Not Have Heard About](https://dzone.com/articles/15-great-programming-blogs-you-may-not-have-heard): tiene que ver con nubecitas y se puede leer en un{os} 4 minuto{s}
-   [Tectonic 1.8 and the Open Cloud Services Catalog are now available](https://coreos.com/blog/tectonic-18-now-available): tiene que ver con nubecitas y se puede leer en un{os} 1 minuto{s}
-   [CoreOS](https://coreos.com/etcd/docs/2.3.7/index.html): tiene que ver con kubernetes y se puede leer en un{os} 0 minuto{s}
-   [Operating etcd clusters for Kubernetes](https://kubernetes.io/docs/tasks/administer-cluster/configure-upgrade-etcd/): tiene que ver con kubernetes y se puede leer en un{os} 16 minuto{s}
-   [The 2017 OWASP Top 10 Update](https://dzone.com/articles/the-long-awaited-2017-owasp-top-10-update-is-here): tiene que ver con nada en general y se puede leer en un{os} 3 minuto{s}
-   [Declaraciones nutricionales en el etiquetado alimentario (infografía)](https://www.midietacojea.com/2017/12/05/declaraciones-nutricionales-etiquetado-alimentos-infografia/): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [Spinnaker vs hosted tools like Codeship, Codefresh](https://www.reddit.com/r/devops/comments/7kodqw/spinnaker_vs_hosted_tools_like_codeship_codefresh/): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [A Basic Guide to Kubernetes Storage - DZone Cloud](https://dzone.com/articles/a-basic-guide-to-kubernetes-storage): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [Git-undo](http://irreal.org/blog/?p=6832): tiene que ver con git y se puede leer en un{os} 0 minuto{s}
-   [How to Build Reusable, Composable, Battle tested Terraform Modules](http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=LVgP63BkhKQ&format=xml): tiene que ver con terraform, nubecitas y se puede leer en un{os} 0 minuto{s}
-   [4 sencillas prácticas sobre seguridad en Docker](http://babel.es/es/blog/blog/marzo-2017/4-sencillas-practicas-sobre-seguridad-en-docker): tiene que ver con docker, seguridad y se puede leer en un{os} 3 minuto{s}
