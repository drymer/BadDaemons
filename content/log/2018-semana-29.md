+++
title = "Semana 29 del año 2018"
author = ["drymer"]
publishDate = 2018-07-22T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Zipkin vs Jaeger: Getting Started With Tracing - Logz.io](https://logz.io/blog/zipkin-vs-jaeger/): tiene que ver con contenedores, traceo y se puede leer en un{os} 6 minuto{s}
-   [11 Ways (Not) to Get Hacked - Kubernetes](https://kubernetes.io/blog/2018/07/18/11-ways-not-to-get-hacked/): tiene que ver con kubernetes y se puede leer en un{os} 15 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [10 Best Practices for Microservice Architectures](https://dzone.com/articles/10-best-practices-for-microservice-architectures): tiene que ver con microservicios y se puede leer en un{os} 2 minuto{s}
-   [Software Architecture: The 5 Patterns You Need to Know](https://dzone.com/articles/software-architecture-the-5-patterns-you-need-to-k): tiene que ver con microservicios y se puede leer en un{os} 10 minuto{s}
-   [Microservices Architecture Design and Patterns](https://dzone.com/articles/microservices-july2018): tiene que ver con microservicios y se puede leer en un{os} 1 minuto{s}
-   [Plan Your Day: Daily Time Management with Emacs, Org-Mode, and Google Calendar](https://medium.com/@mwfogleman/plan-your-day-daily-time-management-with-emacs-org-mode-and-google-calendar-a9162837fdb3): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [r/emacs - TIP: How to integrate company as completion framework](https://www.reddit.com/r/emacs/comments/8z4jcs/tip_how_to_integrate_company_as_completion/): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [r/emacs - How do you sort your info so you can find it later?](https://www.reddit.com/r/emacs/comments/8xu2zt/how_do_you_sort_your_info_so_you_can_find_it_later/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [Ansible Galaxy: Doin’ It Right](https://www.ansible.com/blog/ansible-galaxy-doin-it-right): tiene que ver con ansible y se puede leer en un{os} 0 minuto{s}
-   [How the sausage is made: the Kubernetes 1.11 release interview, from the Kubernetes Podcast - Kubernetes](https://kubernetes.io/blog/2018/07/16/how-the-sausage-is-made-the-kubernetes-1.11-release-interview-from-the-kubernetes-podcast/): tiene que ver con kubernetes y se puede leer en un{os} 34 minuto{s}
-   [Introducing Skaffold: Easy and repeatable Kubernetes development](https://cloudplatform.googleblog.com/2018/03/introducing-Skaffold-Easy-and-repeatable-Kubernetes-development.html): tiene que ver con kubernetes y se puede leer en un{os} 3 minuto{s}
-   [End-to-End Tests: The Pinnacle in Test Automation](https://dzone.com/articles/end-to-end-tests-the-pinnacle-in-test-automation): tiene que ver con devops y se puede leer en un{os} 7 minuto{s}
-   [Effective DevSecOps](https://dzone.com/articles/effective-devsecops): tiene que ver con devops y se puede leer en un{os} 8 minuto{s}
-   [Into the Borg  SSRF inside Google production network | OpnSec](https://opnsec.com/2018/07/into-the-borg-ssrf-inside-google-production-network/): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [“I Was Devastated”: The Man Who Created the World Wide Web Has Some Regrets](https://www.vanityfair.com/news/2018/07/the-man-who-created-the-world-wide-web-has-some-regrets): tiene que ver con redes libres y se puede leer en un{os} 16 minuto{s}
-   [Nicaragua: la complicidad del silencio](https://www.elsaltodiario.com/descentradas/nicaragua-la-complicidad-del-silencio): tiene que ver con nada en general y se puede leer en un{os} 3 minuto{s}
-   [How to hack your learning and be creative  Hacker Noon](https://hackernoon.com/how-to-hack-your-learning-and-be-creative-e5927c3a30b6?source=rss----3a8144eabfe3---4&gi=e7b0bd73802b): tiene que ver con nada en general y se puede leer en un{os} 7 minuto{s}
-   [r/productivity - 6 Reasons Why You’re More Productive on Airplanes (And How to Recreate Them)](https://www.reddit.com/r/productivity/comments/8zd4zz/6_reasons_why_youre_more_productive_on_airplanes/): tiene que ver con productividad y se puede leer en un{os} 0 minuto{s}
-   [Principles of container-based application design](https://www.redhat.com/en/resources/cloud-native-container-design-whitepaper): tiene que ver con contenedores general y se puede leer en un{os} 0 minuto{s}
-   [r/productivity - How to Write More](https://www.reddit.com/r/productivity/comments/8zawpy/how_to_write_more/): tiene que ver con productividad y se puede leer en un{os} 5 minuto{s}
-   [r/productivity - Kanban vs Scrum - Differences Explained in 4 minute video](https://www.reddit.com/r/productivity/comments/8zmwtx/kanban_vs_scrum_differences_explained_in_4_minute/): tiene que ver con productivdad y se puede leer en un{os} 0 minuto{s}
-   [The History of Kubernetes & the Community Behind It - Kubernetes](https://kubernetes.io/blog/2018/07/20/the-history-of-kubernetes--the-community-behind-it/): tiene que ver con kubernetes y se puede leer en un{os} 6 minuto{s}
-   [Here's Why Your Static Website Needs HTTPS](https://www.troyhunt.com/heres-why-your-static-website-needs-https/): tiene que ver con sysadmin y se puede leer en un{os} 5 minuto{s}
