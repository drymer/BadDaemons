+++
title = "Semana 32 del año 2018"
author = ["drymer"]
publishDate = 2018-08-12T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Debugging microservices - Squash vs. Telepresence](https://www.weave.works/blog/debugging-microservices-squash-vs-telepresence): tiene que ver con microservicios y se puede leer en un{os} 5 minuto{s}
-   [What you need to know about Kubernetes RBAC](https://about.gitlab.com/2018/08/07/understanding-kubernestes-rbac/): tiene que ver con kubernetes y se puede leer en un{os} 3 minuto{s}
-   [Do you schedule time and tasks for refactors?](https://dev.to/chrisvasqm/do-you-schedule-time-for-refactors-----1m7p): tiene que ver con programacion y se puede leer en un{os} 2 minuto{s}
-   [Why do great developers love writing tests?](https://dev.to/anabella/why-do-great-developers-love-writing-tests-1o6j): tiene que ver con programacion y se puede leer en un{os} 5 minuto{s}
-   [11 Painful Git Interview Questions You Will Cry On](https://dev.to/aershov24/11-painful-git-interview-questions-you-will-cry-on-1n2g): tiene que ver con git y se puede leer en un{os} 8 minuto{s}
-   [Draft vs Skaffold - Developing on Kubernetes](https://www.weave.works/blog/draft-vs-skaffold-developing-on-kubernetes): tiene que ver con kubernetes y se puede leer en un{os} 3 minuto{s}
-   [The SSH management TO-DOs list  ITNEXT](https://itnext.io/the-ssh-management-to-dos-list-7c40421387e1?gi=6c5f606758d): tiene que ver con ssh y se puede leer en un{os} 6 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Introducing eksctl 0.1.0 - our first major release](https://www.weave.works/blog/introducing-eksctl-0-1-0): tiene que ver con kubernetes, aws y se puede leer en un{os} 2 minuto{s}
-   [Container-Native Integration Testing](https://dzone.com/articles/container-native-integration-testing): tiene que ver con devops y se puede leer en un{os} 6 minuto{s}
-   [My editor journey: sublime, vim, emacs, vscode](https://dev.to/lucasprag/my-editor-journey-sublime-vim-emacs-vscode-19k0): tiene que ver con emacs, nada en general y se puede leer en un{os} 6 minuto{s}
-   [Introducing Kubebuilder: an SDK for building Kubernetes APIs using CRDs - Kubernetes](https://kubernetes.io/blog/2018/08/10/introducing-kubebuilder-an-sdk-for-building-kubernetes-apis-using-crds/): tiene que ver con kubernetes y se puede leer en un{os} 2 minuto{s}
-   [Kubernetes & Traefik 101— When Simplicity Matters](https://dev.to/geraldcroes/kubernetes--traefik-101-when-simplicity-matters-6k6): tiene que ver con kubernetes y se puede leer en un{os} 10 minuto{s}
-   [haskellcamargo/sclack](https://github.com/haskellcamargo/sclack): tiene que ver con cli, slack y se puede leer en un{os} 3 minuto{s}
-   [Sway](https://github.com/swaywm): tiene que ver con wayland y se puede leer en un{os} 0 minuto{s}
-   [Emacs for your modern document needs: A quick guide to working with PDF, LibreOffice and Microsoft Office files](https://emacsnotes.wordpress.com/2018/08/09/222/): tiene que ver con emacs y se puede leer en un{os} 11 minuto{s}
-   [Getting started with Doom Emacs — A great transition from Vim to Emacs](https://medium.com/@aria_39488/getting-started-with-doom-emacs-a-great-transition-from-vim-to-emacs-9bab8e0d8458): tiene que ver con emacs y se puede leer en un{os} 3 minuto{s}
-   [3 Common Mistakes that Python Newbies Make](https://dev.to/rpalo/3-common-mistakes-that-python-newbies-make-4da7): tiene que ver con python y se puede leer en un{os} 5 minuto{s}
-   [A quick review of my Let’s Encrypt setup](https://drewdevault.com/2018/06/27/My-lets-encrypt-setup.html): tiene que ver con letsencrypt y se puede leer en un{os} 2 minuto{s}
-   [The Zen of Programming](https://zen-of-programming.com/start-programming/): tiene que ver con programacion y se puede leer en un{os} 10 minuto{s}
-   [Toolkit de Fanzines: Auto defensa para una manada cyborg organizada](https://cyborgfeminista.tedic.org/toolkit-de-fanzines-auto-defensa-para-una-manada-cyborg-organizada/): tiene que ver con feminismo y se puede leer en un{os} 0 minuto{s}
-   [ActivityPub could be the future](https://blog.digitalscofflaw.com/articles/activitypub-could-be-the-future/): tiene que ver con redes libres y se puede leer en un{os} 2 minuto{s}
-   [Do your commits pass this simple test?](https://dev.to/sublimegeek/do-your-commits-pass-this-simple-test-4ak2): tiene que ver con git y se puede leer en un{os} 1 minuto{s}
-   [Las Redes Sociales hablan y hablan](http://gatossindicales.blogspot.com/2018/08/las-redes-sociales-hablan-y-hablan_6.html): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [ungleich Blog - Mozilla's new DNS resolution is dangerous](https://blog.ungleich.ch/en-us/cms/blog/2018/08/04/mozillas-new-dns-resolution-is-dangerous/): tiene que ver con privacidad y se puede leer en un{os} 5 minuto{s}
-   [Netflix Cloud Security: Detecting Credential Compromise in AWS](https://medium.com/netflix-techblog/netflix-cloud-security-detecting-credential-compromise-in-aws-9493d6fd373a?source=rss----2615bd06b42e---4): tiene que ver con aws y se puede leer en un{os} 8 minuto{s}
-   [Git happens! 6 Common Git mistakes and how to fix them](https://about.gitlab.com/2018/08/08/git-happens/): tiene que ver con git y se puede leer en un{os} 4 minuto{s}
-   [Spoof a commit on GitHub. From Anyone.](https://dev.to/agrinman/spoof-a-commit-on-github-from-anyone-4gf4): tiene que ver con git y se puede leer en un{os} 1 minuto{s}
