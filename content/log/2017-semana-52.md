+++
title = "Semana 52 del año 2017"
author = ["drymer"]
publishDate = 2017-12-31T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [DVIA (Damn Vulnerable iOS App) - A vulnerable iOS app for pentesting](http://damnvulnerableiosapp.com/): tiene que ver con wargames, security y se puede leer en un{os} 0 minuto{s}
-   [DVWA - Damn Vulnerable Web Application](http://www.dvwa.co.uk/): tiene que ver con wargames, security y se puede leer en un{os} 0 minuto{s}
-   [Hacking Dojo Media  The Hacking Dojo](http://hackingdojo.com/dojo-media/): tiene que ver con wargames, security y se puede leer en un{os} 0 minuto{s}
-   [OverTheWire: Wargames](http://overthewire.org/wargames/): tiene que ver con wargames, security y se puede leer en un{os} 0 minuto{s}
-   [Libros gratuitos de O'Reilly](http://www.cyberhades.com/2016/10/04/libros-gratuitos-de-oreilly/): tiene que ver con libros y se puede leer en un{os} 5 minuto{s}
-   [The Locksmith Who Picked Two “Unbeatable” Locks and Ended the Era of “Perfect Security”](http://www.slate.com/blogs/the_eye/2015/04/15/a_history_of_lockpicking_from_99_percent_invisible_and_roman_mars.html): tiene que ver con lock picking y se puede leer en un{os} 6 minuto{s}
-   [Cheatsheet para SQLMap](http://www.securitybydefault.com/2014/05/cheatsheet-para-sqlmap.html): tiene que ver con security, sqlmap y se puede leer en un{os} 0 minuto{s}
-   [Acoustical Attacks against Hard Drives](https://www.schneier.com/blog/archives/2017/12/acoustical_atta.html): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [Securing Docker & Kubernetes](https://darumatic.com/blog/securing_docker_and_kubernetes): tiene que ver con docker, kubernetes, security y se puede leer en un{os} 24 minuto{s}
-   [Script to fully automate Terraform import of Github Org (teams, users, and repos)](https://www.reddit.com/r/devops/comments/7mx23t/script_to_fully_automate_terraform_import_of/): tiene que ver con terraform, git y se puede leer en un{os} 1 minuto{s}
-   [NSA-proof your e-mail in 2 hours](http://sealedabstract.com/code/nsa-proof-your-e-mail-in-2-hours/): tiene que ver con email, hardening y se puede leer en un{os} 31 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Microservices: Rearchitecting Your Stack With Tarantool](https://dzone.com/articles/microservices-re-architecting-your-stack-with-tara): tiene que ver con microservicios, bbdd y se puede leer en un{os} 3 minuto{s}
-   [The wasted talent in the meeting room](http://juanreyero.com/article/technology/meetings.html): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [Why you should be hacking your work environment](http://juanreyero.com/article/technology/talisman.html): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [OrgMode - A third approach to goal setting & tracking](http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=odOKnNDKSIs&format=xml): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [Lovecraft el astrónomo: la peculiar obsesión por la ciencia y el universo del genio literario](https://magnet.xataka.com/idolos-de-hoy-y-siempre/lovecraft-el-astronomo-la-peculiar-obsesion-por-la-ciencia-y-el-universo-del-genio-literario): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [Master the Art of Reading with Lewis Carroll’s Four Rules of Learning](https://lifehacker.com/master-the-art-of-reading-with-lewis-carroll-s-four-rul-1591658273): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [ninrod/evil-string-inflection](https://github.com/ninrod/evil-string-inflection/blob/master/readme.org): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [OrgMode E07S05: Goal setting & Goal Tracking](http://www.youtube.com/oembed?format=xml&url=https://www.youtube.com/watch?v=APhhHCBI8xc&list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE): tiene que ver con org-mode, video y se puede leer en un{os} 0 minuto{s}
-   [OrgMode E05S04: Priorities](http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=kPr0KMR1YdQ&list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE&format=xml): tiene que ver con org-mode, video y se puede leer en un{os} 0 minuto{s}
-   [OrgMode E04S02: Timers](http://www.youtube.com/oembed?format=xml&url=https://www.youtube.com/watch?v=lxkPeJS6keY&list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE): tiene que ver con org-mode, video y se puede leer en un{os} 0 minuto{s}
-   [Orgmode E04S03: Clocking (aka time tracking)](http://www.youtube.com/oembed?format=xml&url=https://www.youtube.com/watch?v=uVv49htxuS8&list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE): tiene que ver con org-mode, video y se puede leer en un{os} 0 minuto{s}
-   [OrgMode E04S04: Column view](http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=BfEAiWe3uvc&list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE&format=xml): tiene que ver con org-mode, video y se puede leer en un{os} 0 minuto{s}
-   [OrgMode E04S05: Effort estimates](http://www.youtube.com/oembed?format=xml&url=https://www.youtube.com/watch?v=BeAtCVZpHCg&list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE): tiene que ver con org-mode, video y se puede leer en un{os} 0 minuto{s}
-   [An Introduction to DevSecOps](https://dzone.com/articles/devsecops-overview): tiene que ver con devsecops, devops y se puede leer en un{os} 1 minuto{s}
