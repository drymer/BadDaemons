+++
title = "Semana 1 del año 2018"
author = ["drymer"]
publishDate = 2018-01-07T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Edward Snowden’s New App Uses Your Smartphone to Physically Guard Your Laptop](https://theintercept.com/2017/12/22/snowdens-new-app-uses-your-smartphone-to-physically-guard-your-laptop/): tiene que ver con seguridad, android y se puede leer en un{os} 9 minuto{s}
-   [Low Cost Non-Invasive Biomedical Imaging](https://media.ccc.de/v/34c3-8948-low_cost_non-invasive_biomedical_imaging): tiene que ver con video, ccc y se puede leer en un{os} 0 minuto{s}
-   [Docker Security Tools: Audit and Vulnerability](https://community.alfresco.com/community/ecm/blog/2015/12/03/docker-security-tools-audit-and-vulnerability-assessment/): tiene que ver con docker, seguridad y se puede leer en un{os} 11 minuto{s}
-   [Sysdig | 20 Docker security tools compared](https://sysdig.com/blog/20-docker-security-tools/): tiene que ver con docker, seguridad y se puede leer en un{os} 14 minuto{s}
-   [The Why and How of Kubernetes Namespaces - DZone Cloud](https://dzone.com/articles/the-why-and-how-of-kubernetes-namespaces): tiene que ver con kubernetes y se puede leer en un{os} 3 minuto{s}
-   [How To: Use mitmproxy to read and modify HTTPS traffic - Philipp's Tech Blog](https://blog.heckel.xyz/2013/07/01/how-to-use-mitmproxy-to-read-and-modify-https-traffic-of-your-phone/): tiene que ver con sysadmin, seguridad, mitm y se puede leer en un{os} 10 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Understanding Microservices Communication and Service Mesh](https://dzone.com/articles/understanding-microservices-communication-and-serv): tiene que ver con microservicios y se puede leer en un{os} 4 minuto{s}
-   [DevOps Glossary](https://www.reddit.com/r/devops/comments/7oeqqy/devops_glossary/): tiene que ver con devops y se puede leer en un{os} 1 minuto{s}
-   [DevSecOps for Government: Safer Software Sooner](https://dzone.com/articles/devsecops-for-government-safer-software-sooner): tiene que ver con devops y se puede leer en un{os} 1 minuto{s}
-   [The Top Continuous Testing Resources for 2018](https://dzone.com/articles/the-top-continuous-testing-resources-for-2018-1?platform=hootsuite): tiene que ver con nubecitas y se puede leer en un{os} 3 minuto{s}
-   [The Serverless Spectrum A Cloud Guru](https://read.acloud.guru/the-serverless-spectrum-147b02cb2292?gi=48908351ce69): tiene que ver con nubecitas y se puede leer en un{os} 7 minuto{s}
-   [How to Become a Conference Speaker, and Why You Should](https://dzone.com/articles/why-and-how-to-become-a-conference-speaker): tiene que ver con nubecitas y se puede leer en un{os} 7 minuto{s}
-   [Jakub Staš - Java Technology Enthusiast](https://jakubstas.com/): tiene que ver con java y se puede leer en un{os} 0 minuto{s}
-   [Java, Integration and the virtues of source](https://sourcevirtues.com/): tiene que ver con java y se puede leer en un{os} 8 minuto{s}
-   [The Emacs Guru Guide to Key Bindings](http://www.wilfred.me.uk/blog/2018/01/06/the-emacs-guru-guide-to-key-bindings/): tiene que ver con emacs y se puede leer en un{os} 4 minuto{s}
-   [Suggestions for making an Emacs talk engaging](https://www.reddit.com/r/emacs/comments/7nvd3x/suggestions_for_making_an_emacs_talk_engaging/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [Wikipedia articles invented by a neural network](http://aiweirdness.com/post/169309161212/wikipedia-articles-invented-by-a-neural-network): tiene que ver con ia y se puede leer en un{os} 2 minuto{s}
-   [Why Raspberry Pi isn't vulnerable to Spectre or Meltdown - Raspberry Pi](https://www.raspberrypi.org/blog/why-raspberry-pi-isnt-vulnerable-to-spectre-or-meltdown/): tiene que ver con nada en general y se puede leer en un{os} 11 minuto{s}
-   [Secrets in source code: How do you manage?](https://www.reddit.com/r/devops/comments/7oeueo/secrets_in_source_code_how_do_you_manage/): tiene que ver con devops y se puede leer en un{os} 1 minuto{s}
-   [Podcast: Tiago Forte on Productivity, Provocation, and Layering Knowledge](https://blog.evernote.com/blog/2017/06/05/podcast-tiago-forte-productivity-part-2/): tiene que ver con nada en general y se puede leer en un{os} 12 minuto{s}
-   [Podcast: Tiago Forte’s Approach to Productivity](https://blog.evernote.com/blog/2017/05/22/podcast-productivity-tiago-forte/): tiene que ver con nada en general y se puede leer en un{os} 11 minuto{s}
-   [O ANN OrgStruct is dead. Long live Orgalist.](https://lists.gnu.org/archive/html/emacs-orgmode/2017-12/msg00619.html): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [How to Learn: Lewis Carroll’s Four Rules for Digesting Information and Mastering the Art of Reading](https://www.brainpickings.org/index.php/2014/06/13/how-to-learn-lewis-carroll/): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [Science is broken](https://media.ccc.de/v/34c3-9055-science_is_broken): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [Why Do We Anthropomorphize Computers?...](https://media.ccc.de/v/34c3-9296-why_do_we_anthropomorphize_computers): tiene que ver con video, ccc y se puede leer en un{os} 0 minuto{s}
-   [Unleash your smart-home devices: Vacuum Cleaning Robot Hacking](https://media.ccc.de/v/34c3-9147-unleash_your_smart-home_devices_vacuum_cleaning_robot_hacking): tiene que ver con video, ccc y se puede leer en un{os} 2 minuto{s}
-   [The Discipline of Chaos Engineering](https://www.gremlin.com/the-discipline-of-chaos-engineering/): tiene que ver con video, ccc y se puede leer en un{os} 0 minuto{s}
-   [Tracking Transience](https://media.ccc.de/v/34c3-9281-tracking_transience): tiene que ver con video, ccc y se puede leer en un{os} 1 minuto{s}
-   [SCADA - Gateway to (s)hell](https://media.ccc.de/v/34c3-8956-scada_-_gateway_to_s_hell): tiene que ver con video, ccc y se puede leer en un{os} 1 minuto{s}
-   [Dude, you broke the Future!](https://media.ccc.de/v/34c3-9270-dude_you_broke_the_future): tiene que ver con video, ccc y se puede leer en un{os} 1 minuto{s}
-   [The Pragmatic Integrator](https://pragmaticintegrator.wordpress.com/): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}
