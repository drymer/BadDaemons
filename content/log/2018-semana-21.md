+++
title = "Semana 21 del año 2018"
author = ["drymer"]
publishDate = 2018-05-27T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Chaos Monkey for Spring Boot Microservices](https://dzone.com/articles/chaos-monkey-for-spring-boot-microservices): tiene que ver con microservicios y se puede leer en un{os} 8 minuto{s}
-   [Lessons learned from rolling out feature teams  Hacker Noon](https://hackernoon.com/lessons-learned-from-rolling-out-feature-teams-729b941027e5?source=rss----3a8144eabfe3---4&gi=96cd0d04fc67): tiene que ver con organizacion y se puede leer en un{os} 9 minuto{s}
-   [Christopher Lemmer Webber on MediaGoblin and ActivityPub](https://medium.com/we-distribute/faces-of-the-federation-christopher-allan-webber-on-mediagoblin-and-activitypub-24bbe212867e): tiene que ver con redes libres y se puede leer en un{os} 17 minuto{s}
-   [Adding a layer of security with the help of API gateways | kreuzwerker GmbH, Berlin](https://kreuzwerker.de/blog/posts/adding-a-layer-of-security-with-the-help-of-api-gateways): tiene que ver con api gateway y se puede leer en un{os} 8 minuto{s}
-   [El tinglado de la alimentación infantil](https://www.elsaltodiario.com/consumo-que-suma/mi-primer-veneno-alimentacion-infantil): tiene que ver con alimentacion y se puede leer en un{os} 5 minuto{s}
-   [Which Programming Languages Use the Least Electricity? - The New Stack](https://thenewstack.io/which-programming-languages-use-the-least-electricity/): tiene que ver con programacion y se puede leer en un{os} 8 minuto{s}
-   [Literate programming with python doctests](http://kitchingroup.cheme.cmu.edu/blog/2018/05/17/Literate-programming-with-python-doctests/): tiene que ver con python y se puede leer en un{os} 3 minuto{s}
-   [Emacs as a Musical Instrument](http://blog.josephwilk.net/art/emacs-as-a-musical-instrument.html): tiene que ver con emacs y se puede leer en un{os} 8 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Microservices Issues: Managing Damage Control](https://dzone.com/articles/microservices-issues-managing-damage-control): tiene que ver con microservicios y se puede leer en un{os} 3 minuto{s}
-   [KubeCon + CloudNativeCon 2018 - Copenhagen - YouTube](https://www.youtube.com/playlist?list=PLj6h78yzYM2N8GdbjmhVU65KYm_68qBmo&app=desktop): tiene que ver con kubernetets y se puede leer en un{os} 8 minuto{s}
-   [Kubernetes Security Best Practices](https://dzone.com/articles/kubernetes-security-best-practices): tiene que ver con kubernetes y se puede leer en un{os} 7 minuto{s}
-   [API Gateway Anywhere: Gateway on Kubernetes in Alibaba Cloud](https://dzone.com/articles/api-gateway-anywhere-gateway-on-kubernetes-in-alib): tiene que ver con kubernetes, api gateway y se puede leer en un{os} 5 minuto{s}
-   [Announcing Drone Autoscale](https://blog.drone.io/drone-autoscale/): tiene que ver con drone y se puede leer en un{os} 0 minuto{s}
-   [Corriendo con Ciber-lobas](https://elbinario.net/2018/04/19/corriendo-con-ciber-lobas/): tiene que ver con feminismo y se puede leer en un{os} 5 minuto{s}
-   [Mark Zuckerberg Played Parliament for Fools and They’re Pissed](http://www.viportal.co/tech/news/mark-zuckerberg-played-parliament-for-fools-and-theyre-pissed/): tiene que ver con privacidad y se puede leer en un{os} 0 minuto{s}
-   [Prometheus: The Good, the Bad, and the Ugly](https://timber.io/blog/prometheus-the-good-the-bad-and-the-ugly/): tiene que ver con prometheus y se puede leer en un{os} 5 minuto{s}
-   [API Gateway as a Node and Moving Beyond Backend and Front-end](https://dzone.com/articles/api-gateway-as-a-node-and-moving-beyond-backend-an): tiene que ver con api gateway y se puede leer en un{os} 2 minuto{s}
-   [r/productivity - TRAIN Your BRAIN To Focus](https://www.reddit.com/r/productivity/comments/8l3b9g/train_your_brain_to_focus/): tiene que ver con productividad y se puede leer en un{os} 1 minuto{s}
-   [¿Qué desmontamos? Primero tú](https://elbinario.net/2018/05/21/que-desmontamos-primero-tu/): tiene que ver con cuidados y se puede leer en un{os} 4 minuto{s}
-   [api gateway swagger terraform - Buscar con Google](https://www.google.com/search?q=api%20gateway%20swagger%20terraform&ie=utf-8&oe=utf-8&client=firefox-b-ab): tiene que ver con terraform, aws y se puede leer en un{os} 0 minuto{s}
-   [Monitoring Using Spring Boot 2.0, Prometheus and Grafana (Part 2 — Exposing Metrics)](https://dzone.com/articles/monitoring-using-spring-boot-2-prometheus-and-graf): tiene que ver con prometheus y se puede leer en un{os} 2 minuto{s}
