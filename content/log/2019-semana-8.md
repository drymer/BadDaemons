+++
title = "Semana 8 del año 2019"
author = ["drymer"]
publishDate = 2019-02-23T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Selling 911 Location Data](https://irreal.org/blog/?p=7844): tiene que ver con privacidad y se puede leer un{os} 1 minuto{s}
-   [Secret Design Docs: Multi-Tenant Orchestrator](https://blog.jessfraz.com/post/secret-design-docs-multi-tenant-orchestrator/): tiene que ver con orquestadores y se puede leer un{os} 7 minuto{s}
-   [Working with log files in Emacs](https://writequit.org/articles/working-with-logs-in-emacs.html): tiene que ver con emacs y se puede leer un{os} 11 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Using a logbook to improve your programming](https://routley.io/tech/2017/11/23/logbook.html): tiene que ver con programacion y se puede leer en un{os} 2 minuto{s}
-   [10 LDFLAGS I Love](https://blog.jessfraz.com/post/top-10-favorite-ldflags/): tiene que ver con linux y se puede leer en un{os} 2 minuto{s}
-   [Why I Gave Up on Extreme Minimalism](http://www.janafadness.com/blog/no-extreme-minimalism/): tiene que ver con la vida y se puede leer en un{os} 8 minuto{s}
-   [Infrastructure diagrams as code](https://dev.to//raoulmeyer/infrastructure-diagrams-as-code-3f3j): tiene que ver con infrastructura como codigo y se puede leer en un{os} 2 minuto{s}
-   [Getting Organized with an Engineering Logbook](https://www.viget.com/articles/getting-organized-with-an-engineering-logbook/): tiene que ver con productividad y se puede leer en un{os} 2 minuto{s}
-   [LD\_PRELOAD: The Hero We Need and Deserve](https://blog.jessfraz.com/post/ld_preload/): tiene que ver con linux y se puede leer en un{os} 2 minuto{s}
-   [Org Mode To Google Docs and Beyond](https://levlaz.org/org-mode-to-google-docs-and-beyond/): tiene que ver con org-mod y se puede leer en un{os} 4 minuto{s}
-   [The Firmware and Hardware Rabbit Hole](https://blog.jessfraz.com/post/the-firmware-rabbit-hole/): tiene que ver con seguridad y se puede leer en un{os} 7 minuto{s}
-   [PoC de explotación de CVE de runc](https://github.com/feexd/pocs/tree/master/CVE-2019-5736): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [r/emacs - Org-Kungfu: Editing Confluence pages with Org-mode](https://www.reddit.com/r/emacs/comments/aqecha/orgkungfu_editing_confluence_pages_with_orgmode/): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Sharing Data Among Org-mode Blocks](https://irreal.org/blog/?p=7836): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [emacs-lsp](https://github.com/emacs-lsp): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
