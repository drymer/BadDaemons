+++
title = "Semana 46 del año 2018"
author = ["drymer"]
publishDate = 2018-11-18T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [GitLab's Secret to Multi-Million-Dollar Success: All 350 Employees Work Remotely](https://www.inc.com/cameron-albert-deitch/2018-inc5000-gitlab.html): tiene que ver con git, agilismo y se puede leer un{os} 4 minuto{s}
-   [Zorras por el mundo: Errekaleor](https://ondahostil.wordpress.com/2018/11/11/zorras-por-el-mundo-errekaleor/): tiene que ver con privacidad, autogestion y se puede leer un{os} 8 minuto{s}
-   [No podéis incluir a las mujeres trans en el feminismo](http://www.pikaramagazine.com/2018/11/comunicacion-transinclusiva/): tiene que ver con feminismo y se puede leer un{os} 6 minuto{s}
-   [A Win for Open Access](http://irreal.org/blog/?p=7596): tiene que ver con nada en general y se puede leer un{os} 1 minuto{s}
-   [The Beginner’s Guide to the CNCF Landscape - Cloud Native Computing Foundation](https://www.cncf.io/blog/2018/11/05/34097/): tiene que ver con nubecitas, devops y se puede leer un{os} 28 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Impostor syndrome strikes men just as hard as women… and other findings from thousands of technical interviews](http://blog.interviewing.io/impostor-syndrome-strikes-men-just-as-hard-as-women-and-other-findings-from-thousands-of-technical-interviews/): tiene que ver con la vida, feminismo y se puede leer un{os} 13 minuto{s}
-   [Medium is a poor choice for blogging](https://medium.com/@nikitonsky/medium-is-a-poor-choice-for-blogging-bb0048d19133): tiene que ver con privaciad y se puede leer en un{os} 4 minuto{s}
-   [Things Nobody Told Me About Being a Software Engineer](https://anaulin.org/blog/things-nobody-told-me-about-being-a-software-engineer/): tiene que ver con engineria y se puede leer en un{os} 3 minuto{s}
-   [L’Espai Feminista de Sants llama a movilizarse contra la represión al movimiento feminista](http://www.pikaramagazine.com/2018/11/8mjotambetallo-lespai-feminista-de-sants-llama-a-movilizarse-contra-la-represion-al-movimiento-feminista/): tiene que ver con feminismo y se puede leer en un{os} 4 minuto{s}
-   [Proof Reading with Text-To-Speech](http://irreal.org/blog/?p=7611): tiene que ver con productividad y se puede leer en un{os} 0 minuto{s}
-   [Agile won the war but lost the peace](https://www.allankellyassociates.co.uk/archives/2762/agile-won-the-war-but-lost-the-peace/): tiene que ver con agilismo y se puede leer en un{os} 4 minuto{s}
-   [Troy Hunt on Why Passwords Aren’t Going Away](http://irreal.org/blog/?p=7598): tiene que ver con seguridad y se puede leer en un{os} 1 minuto{s}
-   [Recomendaciones para hablar con menores cuando necesitan ayuda](https://notxor.nueva-actitud.org/blog/2018/11/05/recomendaciones-para-hablar-con-menores-cuando-necesitan-ayuda/): tiene que ver con la vida y se puede leer en un{os} 2 minuto{s}
-   [October 21 post-incident analysis](https://blog.github.com/2018-10-30-oct21-post-incident-analysis/): tiene que ver con gestion de desastres y se puede leer en un{os} 14 minuto{s}
-   [Oreo, las galletas vinculadas a la extinción del orangután](https://www.elsaltodiario.com/biodiversidad/oreo-galletas-orangutan-extincion-aceite-palma): tiene que ver con ecologia y se puede leer en un{os} 3 minuto{s}
-   [How GitLab makes money](https://about.gitlab.com/2018/11/09/monetizing-and-being-open-source/): tiene que ver con git y se puede leer en un{os} 3 minuto{s}
-   [NO SOLO DUELEN LOS GOLPES](https://nosoloduelenlosgolpes.wordpress.com/): tiene que ver con feminismo y se puede leer en un{os} 2 minuto{s}
