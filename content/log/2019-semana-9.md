+++
title = "Semana 9 del año 2019"
author = ["drymer"]
publishDate = 2019-03-02T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Xenofeminismo: la tecnología como promesa de emancipación](https://www.pikaramagazine.com/2019/02/xenofeminismo-tecnologia-emancipacion/): tiene que ver con feminismo y se puede leer un{os} 10 minuto{s}
-   [Kubernetes networking](https://morioh.com/p/ecb38c8342ba/an-illustrated-guide-to-kubernetes-networking): tiene que ver con kubernetes y se puede leer un{os} 0 minuto{s}
-   [Breaking out of Docker via runC - Explaining CVE-2019-5736](https://www.twistlock.com/labs-blog/breaking-docker-via-runc-explaining-cve-2019-5736/): tiene que ver con runc, seguridad y se puede leer un{os} 14 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [La culpa del hombre: el moralismo y la inmovilización masculina en el 8M](https://www.elsaltodiario.com/masculinidades/8m-huelga-culpa-hombre-moralismo-inmovilizacion-masculina): tiene que ver con feminismo y se puede leer en un{os} 7 minuto{s}
-   [Government. Medicine. Capitalism?](https://blog.jessfraz.com/post/government-medicine-capitalism/): tiene que ver con nada en general y se puede leer en un{os} 8 minuto{s}
-   [From the File Vault: Pharmy Tales](https://blog.jessfraz.com/post/pharmy-tales/): tiene que ver con nada en general y se puede leer en un{os} 4 minuto{s}
-   [Explaining Scrum story points](https://dev.to//anortef/explaining-scrum-story-points-3mn1): tiene que ver con productividad y se puede leer en un{os} 1 minuto{s}
