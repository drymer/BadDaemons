+++
title = "Semana 12 del año 2018"
author = ["drymer"]
publishDate = 2018-03-25T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Why Use Emacs  Mathew Robinson](https://medium.com/@chasinglogic/why-use-emacs-ede521733fda): tiene que ver con emacs y se puede leer en un{os} 11 minuto{s}
-   [The Basics of BDD in Testing](https://dzone.com/articles/the-basics-of-bdd-in-testing): tiene que ver con devops, bbdd y se puede leer en un{os} 3 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Evil Emacs term workflow](https://www.reddit.com/r/emacs/comments/86sktb/evil_emacs_term_workflow/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [What workflows have you implemented within Org-mode?](https://www.reddit.com/r/emacs/comments/85f8mp/what_workflows_have_you_implemented_within_orgmode/): tiene que ver con emacs y se puede leer en un{os} 6 minuto{s}
-   [Marcin Borkowski: 2018-03-18 My Org-mode hydra](http://mbork.pl/2018-03-18_My_Org-mode_hydra): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [Zettelkasten Method for taking notes and The Archive software](https://www.reddit.com/r/productivity/comments/872sqy/zettelkasten_method_for_taking_notes_and_the/): tiene que ver con productividad y se puede leer en un{os} 1 minuto{s}
-   [6 Ways People Try to Manipulate You and How to Defend Yourself](https://conquer.today/defend-against-manipulation/): tiene que ver con cuidados y se puede leer en un{os} 4 minuto{s}
-   [Try Daily Templates Instead of Daily Routines for Better Results](https://conquer.today/daily-templates-over-routines/): tiene que ver con productividad y se puede leer en un{os} 4 minuto{s}
-   [Learn to Meditate in Two Minutes Using The Perfect Ten Method](https://conquer.today/learn-to-meditate/): tiene que ver con cuidados y se puede leer en un{os} 3 minuto{s}
-   [A little golang way | AeroFS](https://www.aerofs.com/a-little-golang-way-md/): tiene que ver con programacion y se puede leer en un{os} 5 minuto{s}
-   [What is your calendar setup?](https://www.reddit.com/r/emacs/comments/86xh8c/what_is_your_calendar_setup/): tiene que ver con productividad y se puede leer en un{os} 3 minuto{s}
-   [This week in Usability & Productivity, part 11](https://www.reddit.com/r/linux/comments/86yt73/this_week_in_usability_productivity_part_11/): tiene que ver con productividad y se puede leer en un{os} 2 minuto{s}
-   [Kibana Hacks: 5 Tips and Tricks - DZone Big Data](https://dzone.com/articles/kibana-hacks-5-tips-and-tricks): tiene que ver con kibana y se puede leer en un{os} 4 minuto{s}
-   [Tienda Online 1 unids cjmcu-teclado virtual badusb USB TF memoria teclado ATMEGA32U4](https://m.es.aliexpress.com/item/32815828963.html?trace=wwwdetail2mobilesitedetail&productId=32815828963&productSubject=CJMCU-Virtual-Keyboard-Badusb-USB-TF-Memory-Keyboard-ATMEGA32U4&spm=a2g0s.9042311.0.0.mhzoBn): tiene que ver con seguridad y se puede leer en un{os} 2 minuto{s}
-   [Oda a la tristeza: Kufungisisa o el susto de pensar mucho.](http://unaantropologaenlaluna.blogspot.com.es/2018/03/oda-la-tristeza-el-susto-de-pensar-mucho.html): tiene que ver con cuidadso y se puede leer en un{os} 7 minuto{s}
-   [Qué era secreto y qué es lo que siempre hemos sabido del escándalo de Facebook y su influencia en las elecciones](https://www.eldiario.es/tecnologia/secreto-escandalo-Facebook-influencia-elecciones_0_752075814.html): tiene que ver con privacidad y se puede leer en un{os} 3 minuto{s}
-   [abo-abo/define-word](https://github.com/abo-abo/define-word/blob/master/define-word.el): tiene que ver con emacs y se puede leer en un{os} 3 minuto{s}
-   [Ghub 2.0 and Glab 2.0 released](https://www.reddit.com/r/emacs/comments/85voqd/ghub_20_and_glab_20_released/): tiene que ver con git y se puede leer en un{os} 1 minuto{s}
-   [Need advice for discipline](https://www.reddit.com/r/productivity/comments/85vyu9/need_advice_for_discipline/): tiene que ver con productividad y se puede leer en un{os} 2 minuto{s}
-   [How to get better battery life in linux?](https://www.reddit.com/r/linux/comments/85wxqx/how_to_get_better_battery_life_in_linux/): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}
-   [Journals and Note taking](https://www.reddit.com/r/productivity/comments/85z2da/journals_and_note_taking/): tiene que ver con productividad y se puede leer en un{os} 2 minuto{s}
-   [How do you manage your personal social messages (instant messages, texts, whatsapp, facebook, etc.)?](https://www.reddit.com/r/productivity/comments/85f3y2/how_do_you_manage_your_personal_social_messages/): tiene que ver con productividad y se puede leer en un{os} 2 minuto{s}
-   [Run Your Daily Template for A Week and Monitor Success](https://conquer.today/run-daily-template-week-monitor-success/): tiene que ver con productividad y se puede leer en un{os} 0 minuto{s}
