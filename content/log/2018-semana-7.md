+++
title = "Semana 7 del año 2018"
author = ["drymer"]
publishDate = 2018-02-18T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Seven years at Docker](http://jpetazzo.github.io/2018/02/17/seven-years-at-docker/): tiene que ver con docker, cuidados y se puede leer en un{os} 8 minuto{s}
-   [Comida real y fibromialgia. Tratamiento dietético y nutricional.](http://realfooding.com/fibromialgia/): tiene que ver con nutricion y se puede leer en un{os} 16 minuto{s}
-   ["Si estoy rodeada de amigas violadas, lógicamente también tengo amigos violadores"](http://www.eldiario.es/cultura/libros/Virginie-Despentes-amigos-violadores_0_740176218.html): tiene que ver con feminismo y se puede leer en un{os} 10 minuto{s}
-   ['Cierto feminismo y cierta izquierda han comprado la idea de que el sexo es lo peor'](http://www.publico.es/culturas/entrevista-virginie-despentes-feminismo-izquierda-han-comprado-idea-sexo-peor.html): tiene que ver con feminismo y se puede leer en un{os} 8 minuto{s}
-   [“Armas Marca España, el oscuro negocio de la guerra”](http://www.nuevatribuna.es/articulo/sociedad/greenpeace-denuncia-impunidad-tolerada-gobierno-espanol-venta-armas-paises-conflicto/20180213181101148518.html): tiene que ver con nada en general y se puede leer en un{os} 4 minuto{s}
-   [Piotr Limanowski Reading for Programmers](https://codearsonist.com/reading-for-programmers): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [Hackers, ciberfeminismo y revolución](https://www.pikaramagazine.com/2018/02/hackers-ciberfeminismo-y-revolucion/): tiene que ver con activismo y se puede leer en un{os} 3 minuto{s}
-   [Refactoring power-ups](http://manuel-uberti.github.io/emacs/2018/02/10/occur/): tiene que ver con programacion, refactorizacion y se puede leer en un{os} 1 minuto{s}
-   [Benchmarking Message Queue Latency](https://bravenewgeek.com/benchmarking-message-queue-latency/): tiene que ver con colas y se puede leer en un{os} 12 minuto{s}
-   [Ten email commandments](http://timharford.com/2013/09/3180/): tiene que ver con productividad y se puede leer en un{os} 16 minuto{s}
-   [Exploiting SQL Injection: A Hands-on Example](https://dzone.com/articles/exploiting-sql-injection-a-hands-on-example): tiene que ver con seguridad y se puede leer en un{os} 10 minuto{s}
-   [GitOps: High-Velocity CI/CD for Kubernetes](https://dzone.com/articles/gitops-high-velocity-cicd-for-kubernetes): tiene que ver con git, kubernetes, ci/cd, devops y se puede leer en un{os} 10 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Announcing Spring Cloud GCP—integrating your favorite Java framework with Google Cloud](https://cloudplatform.googleblog.com/2018/02/announcing-Spring-Cloud-GCP-integrating-your-favorite-Java-framework-with-Google-Cloud.html?m=1): tiene que ver con java y se puede leer en un{os} 2 minuto{s}
-   [6 Meeting Hacks (and 1 Weird Tip) That Build Trust in the Room](https://dzone.com/articles/6-meeting-hacks-and-1-weird-tip-that-build-trust-i): tiene que ver con productividad y se puede leer en un{os} 5 minuto{s}
-   [13 Signs of a Toxic Team Culture](https://dzone.com/articles/13-signs-of-a-toxic-team-culture?oid=twitter): tiene que ver con productividad y se puede leer en un{os} 3 minuto{s}
-   [KISS Kubernetes: Building Your First Container Management Cluster - DZone Cloud](https://dzone.com/articles/kiss-kubernetes-building-your-first-container-mana): tiene que ver con kubernetes y se puede leer en un{os} 7 minuto{s}
-   [Structures in Emacs Lisp](http://irreal.org/blog/?p=6960): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [What Has Emacs Replaced Your Workflow](http://irreal.org/blog/?p=6958): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Emacs Tip: find-file-at-point](https://www.reddit.com/r/emacs/comments/7wjyhy/emacs_tip_findfileatpoint/): tiene que ver con emacs y se puede leer en un{os} 3 minuto{s}
-   [More emacs configuration tweaks (multiple-cursor on click, minimap, code folding, ensime eval overlays)](http://www.mostlymaths.net/2016/09/more-emacs-configuration-tweaks.html?m=1): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [What has emacs replaced in your life?](https://www.reddit.com/r/emacs/comments/7ww01t/what_has_emacs_replaced_in_your_life/): tiene que ver con emacs y se puede leer en un{os} 4 minuto{s}
-   [Maslach Burnout Inventory](https://en.wikipedia.org/wiki/Maslach_Burnout_Inventory): tiene que ver con cuidados y se puede leer en un{os} 9 minuto{s}
-   [Un doctor aprovechado tras un aborto: antes de irte "dame un beso"](http://www.eldiario.es/micromachismos/doctor-aprovechado-aborto_6_740235990.html): tiene que ver con feminismo y se puede leer en un{os} 3 minuto{s}
-   [El autismo, la esquizofrenia, el trastorno bipolar y la depresión tienen patrones superpuestos de actividad genética](http://web-salud.blogspot.com.es/2018/02/autismo-esquizofrenia-trastorno-bipolar-depresion-genetica.html): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [Design Patterns](https://dzone.com/refcardz/design-patterns?chapter=1): tiene que ver con microservicios y se puede leer en un{os} 16 minuto{s}
-   [Bill Murray: Identity Politics Have Made Comedy "Harder and Harder"](https://www.realclearpolitics.com/video/2018/02/13/bill_murray_comedy_harder_and_harder_to_do_because_people_trying_to_win_their_point_of_view.html?_escaped_fragment_=): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [Por qué las periodistas feministas vamos a  hacer huelga](https://www.pikaramagazine.com/2018/02/por-que-las-periodistas-feministas-vamos-a-hacer-huelga/): tiene que ver con feminismo y se puede leer en un{os} 4 minuto{s}
-   [How to design your own orgmode clock reports](https://www.reddit.com/r/emacs/comments/7xgzbm/how_to_design_your_own_orgmode_clock_reports/): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [How to (seriously) read a scientific paper](http://www.sciencemag.org/careers/2016/03/how-seriously-read-scientific-paper): tiene que ver con nada en general y se puede leer en un{os} 16 minuto{s}
-   [Suggestions on making slides (for presentations)](https://www.reddit.com/r/emacs/comments/7voel1/suggestions_on_making_slides_for_presentations/): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [Marcin Borkowski: 2018-02-12 append-next-kill](http://mbork.pl/2018-02-12_append-next-kill): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [Using FZF instead of DMENU  njiuko](https://medium.com/njiuko/using-fzf-instead-of-dmenu-2780d184753f): tiene que ver con cli y se puede leer en un{os} 5 minuto{s}
-   [Making Things Easier](http://irreal.org/blog/?p=6924): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [What makes the difference between a master lock and an ordinary one?](https://www.reddit.com/r/lockpicking/comments/7wrnly/what_makes_the_difference_between_a_master_lock/): tiene que ver con lockpicking y se puede leer en un{os} 0 minuto{s}
-   [¿Sabes qué pasa en tú nube? Forensic version](http://www.hackplayers.com/2018/02/sabes-que-pasa-en-tu-nube-forensic-version.html): tiene que ver con nubecitas y se puede leer en un{os} 1 minuto{s}
-   [Key bindings for git with fzf / junegunn.kr](https://junegunn.kr/2016/07/fzf-git/): tiene que ver con git y se puede leer en un{os} 3 minuto{s}
