+++
title = "Semana 3 del año 2018"
author = ["drymer"]
publishDate = 2018-01-21T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [What is "Immutable Infrastructure"?](http://blog.armory.io/what-is-immutable-infrastructure/): tiene que ver con nubecitas y se puede leer en un{os} 6 minuto{s}
-   [HashiCorp on Recovering from Failures](https://dzone.com/articles/hashicorp-on-recovering-from-failures): tiene que ver con nubecitas y se puede leer en un{os} 6 minuto{s}
-   [Please consider changing the name of PetSet before General Availability](https://github.com/kubernetes/kubernetes/issues/27430): tiene que ver con kubernetes, veganismo y se puede leer en un{os} 0 minuto{s}
-   [My Raspberry Pi Kubernetes Cluster](https://dzone.com/articles/my-raspberry-pi-kubernetes-cluster): tiene que ver con kubernetes, raspberrypi y se puede leer en un{os} 7 minuto{s}
-   [OAuth 2.0 in a Nutshell](https://dzone.com/articles/oauth-20-in-a-nutshell): tiene que ver con oauth y se puede leer en un{os} 9 minuto{s}
-   [The Cross-Site Scripting (XSS) Vulnerability](https://dzone.com/articles/the-cross-site-scripting-xss-vulnerability-definit): tiene que ver con seguridad, xss y se puede leer en un{os} 6 minuto{s}
-   [How Spinnaker fits into the Continuous Delivery puzzle](http://blog.armory.io/how-spinnaker-fits-into-the-continuous-delivery-puzzle/): tiene que ver con despliegues, spinnaker y se puede leer en un{os} 12 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Hershell, una shell reversa escrita en Go](http://www.hackplayers.com/2018/01/hershell-una-shell-reversa-escrita-en-go.html): tiene que ver con seguridad, shell inversa y se puede leer en un{os} 4 minuto{s}
-   [Top 10 Things to Know About Alibaba Cloud RDS - DZone Cloud](https://dzone.com/articles/top-10-things-to-know-about-alibaba-cloud-rds-1): tiene que ver con nubecitas y se puede leer en un{os} 10 minuto{s}
-   [Example of a CI/CD pipeline with Kubernetes, Gitlab CI, and Helm](https://www.reddit.com/r/devops/comments/7qzopb/example_of_a_cicd_pipeline_with_kubernetes_gitlab/): tiene que ver con git, kubernetes, ci/cd y se puede leer en un{os} 1 minuto{s}
-   [Manage your Kubernetes clusters with Terraform on Google Cloud](https://www.reddit.com/r/devops/comments/7qml1w/manage_your_kubernetes_clusters_with_terraform_on/): tiene que ver con terraform, kubernetes y se puede leer en un{os} 1 minuto{s}
-   [Philip K. Dick and the Fake Humans](http://bostonreview.net/literature-culture/henry-farrell-philip-k-dick-and-fake-humans): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [W O R D S](https://garbados.github.io/my-blog/distributed-datastructures.html): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [How I Learned to Stop Worrying and Love DTrace](http://nullprogram.com/blog/2018/01/17/): tiene que ver con nada en general y se puede leer en un{os} 11 minuto{s}
-   [predictive-mode; question about predictive-major-mode-alist; path to dictionaries](https://www.reddit.com/r/emacs/comments/7r7vur/predictivemode_question_about/): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [Code Analysis Part 2: Analyzing Code with SonarQube](https://dzone.com/articles/code-analysis-part-2-analyzing-code-with-sonarqube): tiene que ver con nada en general y se puede leer en un{os} 3 minuto{s}
-   [My Infosec Blog List](https://gist.github.com/0x9090/18db21f501bdab325ae4b242789a83c6): tiene que ver con nada en general y se puede leer en un{os} 32 minuto{s}
-   [In defence of swap: common misconceptions](https://chrisdown.name/2018/01/02/in-defence-of-swap.html): tiene que ver con nada en general y se puede leer en un{os} 18 minuto{s}
-   [Perturbación Feminista en la Fuerza](http://www.eldiario.es/tribunaabierta/Perturbacion-Feminista-Fuerza_6_729737031.html): tiene que ver con feminismo y se puede leer en un{os} 6 minuto{s}
-   [nEXT Browser](https://next-browser.github.io/): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Org Clock Table Formatting](https://www.reddit.com/r/emacs/comments/7qfr5x/org_clock_table_formatting/): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [Subscribe! Announcements for the borg package manager](https://www.reddit.com/r/emacs/comments/7qk3qw/subscribe_announcements_for_the_borg_package/): tiene que ver con contenedores y se puede leer en un{os} 0 minuto{s}
-   [5 Tips to Secure Docker Containers for Early Adopters](https://dzone.com/articles/5-tips-to-secure-docker-containers-for-early-adopt): tiene que ver con docker, nubecitas y se puede leer en un{os} 3 minuto{s}
-   [What You Didn’t Know About HTTPS](http://cybosol.com/blog/what-you-didnt-know-about-https/): tiene que ver con sysadmin y se puede leer en un{os} 4 minuto{s}
-   [Container Monitoring: Prometheus and Grafana Vs. Sysdig and Sysdig Monitor](https://dzone.com/articles/container-monitoring-prometheus-and-grafana-vs-sys): tiene que ver con contenedores y se puede leer en un{os} 5 minuto{s}
-   [How to build a CI/CD pipeline using Kubernetes, Gitlab CI, and Helm](http://artemstar.com/2018/01/15/cicd-with-kubernetes-and-gitlab/): tiene que ver con git, kubernetes, ci/cd, devops y se puede leer en un{os} 5 minuto{s}
