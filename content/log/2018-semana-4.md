+++
title = "Semana 4 del año 2018"
author = ["drymer"]
publishDate = 2018-01-28T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Functional Programming Principles Every Imperative Programmer Should Use - DZone Java](https://dzone.com/articles/functional-programming-principles-every-imperative): tiene que ver con java, Programacion funcional y se puede leer en un{os} 9 minuto{s}
-   [Microservices With Spring Boot - Using Ribbon for Load Balancing](https://dzone.com/articles/microservices-with-spring-boot-part-4-using-ribbon): tiene que ver con microservicios, springboot y se puede leer en un{os} 2 minuto{s}
-   [Microservices With Spring Boot - Creating a Forex Microservice](https://dzone.com/articles/microservices-with-spring-boot-part-2-creating-a-f): tiene que ver con microservicios, springboot y se puede leer en un{os} 10 minuto{s}
-   [Fun with Multi-stage Dockerfiles Koki](https://medium.com/kokster/fun-with-multi-stage-dockerfiles-7da7f11403d2): tiene que ver con docker y se puede leer en un{os} 4 minuto{s}
-   [U.S. border guards can search your phone: here are some details on how](http://www.cbc.ca/news/technology/usa-border-phones-search-1.4494371): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [Tech Workers of the World, Unite!](http://inthesetimes.com/working/entry/20836/tech-workers-union-labor-movement-silicon-valley-facebook-google): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}
-   [Functional Programming: Recursion](https://dzone.com/articles/functional-programming-recursion): tiene que ver con programacion y se puede leer en un{os} 3 minuto{s}
-   [Reporting Errors from Control Plane to Applications Using Kubernetes Events](http://blog.kubernetes.io/2018/01/reporting-errors-using-kubernetes-events.html): tiene que ver con kubernetes y se puede leer en un{os} 5 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Using Netflix Eureka With Spring Cloud/Spring Boot Microservices (Part 1)](https://dzone.com/articles/using-netflix-eureka-with-spring-cloudspring-boot): tiene que ver con microservicios y se puede leer en un{os} 2 minuto{s}
-   [Microservices With Spring Boot - Getting Started](https://dzone.com/articles/microservices-with-spring-boot-part-1-getting-star): tiene que ver con microservicios y se puede leer en un{os} 8 minuto{s}
-   [Learning Docker Networking (Part1 & Part2)](https://www.reddit.com/r/devops/comments/7srugg/learning_docker_networking_part1_part2/): tiene que ver con docker y se puede leer en un{os} 0 minuto{s}
-   [Better autoscaling with Prometheus and the Kubernetes Metrics APIs](https://coreos.com/blog/autoscaling-with-prometheus-and-kubernetes-metrics-apis): tiene que ver con kubernetes y se puede leer en un{os} 4 minuto{s}
-   [Using Kubernetes Part 1  Linux Academy Blog](https://linuxacademy.com/blog/devops/using-kubernetes-part-1/): tiene que ver con kubernetes y se puede leer en un{os} 3 minuto{s}
-   [Spring Boot 2: Migrating From Dropwizard Metrics to Micrometer - DZone Java](https://dzone.com/articles/spring-boot-2-migrating-from-dropwizard-to-micrometer): tiene que ver con java y se puede leer en un{os} 2 minuto{s}
-   [Literate programming best practices: flpping between source editing and org-mode editing](https://www.reddit.com/r/emacs/comments/7sxyfa/literate_programming_best_practices_flpping/): tiene que ver con emacs y se puede leer en un{os} 10 minuto{s}
-   [Comparing Doom Emacs, Spacemacs, and Vanilla Emacs](http://irreal.org/blog/?p=6914): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [Manage daily todo files on Emacs](https://gist.github.com/prathik/ae2899ae2c432dcb0cfe966aa3683eb3): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Ansible Run Analysis](https://dzone.com/articles/ansible-run-analysis): tiene que ver con ansible, devops y se puede leer en un{os} 2 minuto{s}
-   [PDF](http://www.lysator.liu.se/mit-guide/MITLockGuide.pdf): tiene que ver con lock picking y se puede leer en un{os} 0 minuto{s}
-   [Externalizing fine-grained security](https://gitanjali548936168.wordpress.com/2018/01/03/externalizing-fine-grained-security/): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}
-   [danielside.nom.es | Amazon Telescreen](http://danielside.nom.es/2018/01/22/amazon-telescreen/): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [New Zealand Football website hacked with fake news of CEO Andy Martin's resignation](http://www.ibtimes.co.uk/new-zealand-football-website-hacked-fake-news-ceo-andy-martins-resignation-1655003): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [The death of microservice madness in 2018](https://www.reddit.com/r/devops/comments/7s23du/the_death_of_microservice_madness_in_2018/): tiene que ver con microservicios y se puede leer en un{os} 8 minuto{s}
-   [Research on Misconfigured Jenkins Servers - emtunc's Blog](https://www.reddit.com/r/netsec/comments/7rijkf/research_on_misconfigured_jenkins_servers_emtuncs/): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}
-   [How Slack Stays Secure During Hyper Growth](https://www.reddit.com/r/netsec/comments/7rkgzc/how_slack_stays_secure_during_hyper_growth/): tiene que ver con seguridad y se puede leer en un{os} 8 minuto{s}
-   [One-Lin3r: herramienta para generar comandos de una sóla línea para pentesting](http://www.hackplayers.com/2018/01/one-lin3r-comandos-pentesting.html): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [abatchy's blog | Kernel Exploitation 2: Payloads](http://www.abatchy.com/2018/01/kernel-exploitation-2): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [Monitoring Your Wireless Interfaces](https://dzone.com/articles/monitoring-your-wireless-interfaces): tiene que ver con nada en general y se puede leer en un{os} 4 minuto{s}
-   [.gitignore Generated by gitignore.io](https://www.reddit.com/r/emacs/comments/7sk5tp/gitignore_generated_by_gitignoreio/): tiene que ver con git y se puede leer en un{os} 0 minuto{s}
