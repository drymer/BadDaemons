+++
title = "Semana 8 del año 2018"
author = ["drymer"]
publishDate = 2018-02-25T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [“El ecofeminismo plantea un cambio de conciencia, pensarnos más allá del especismo”](https://www.pikaramagazine.com/2018/02/maria-jose-guerra-palmero/): tiene que ver con feminismo, racismo, veganismo, capitalismo, interseccionalidad y se puede leer en un{os} 19 minuto{s}
-   [Optimizando tu aplicación Java en Docker](https://overclockyourself.wordpress.com/2018/02/14/optimizando-tu-aplicacion-java-en-docker/): tiene que ver con docker, java y se puede leer en un{os} 28 minuto{s}
-   [Emacs Lisp Lambda Expressions Are Not Self-Evaluating « null program](http://nullprogram.com/blog/2018/02/22/): tiene que ver con emacs y se puede leer en un{os} 9 minuto{s}
-   [A/B Testing With Kubernetes and Istio](https://dzone.com/articles/ab-testing-with-kubernetes-and-istio): tiene que ver con kubernetes, microservicios, service mesh y se puede leer en un{os} 2 minuto{s}
-   [Emacs for Devops](http://irreal.org/blog/?p=6966): tiene que ver con emacs, devops y se puede leer en un{os} 1 minuto{s}
-   [Top 5 OWASP Resources No Developer Should Be Without](https://dzone.com/articles/top-5-owasp-resources-no-developer-should-be-witho-1): tiene que ver con seguridad y se puede leer en un{os} 6 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Microservices Authentication and Authorization Using API Gateway](https://dzone.com/articles/security-in-microservices): tiene que ver con microservicios y se puede leer en un{os} 3 minuto{s}
-   [IBM/microservices-traffic-management-using-istio](https://github.com/IBM/microservices-traffic-management-using-istio): tiene que ver con microservicios y se puede leer en un{os} 20 minuto{s}
-   [Amazon AWS Servers Might Soon Be Held for Ransom, Similar to MongoDB](https://www.bleepingcomputer.com/news/security/amazon-aws-servers-might-soon-be-held-for-ransom-similar-to-mongodb/): tiene que ver con mongo, aws y se puede leer en un{os} 7 minuto{s}
-   [Released! Learning Kubernetes and K8s: Native Tools](https://theagileadmin.com/2018/01/26/released-learning-kubernetes-and-k8s-native-tools/): tiene que ver con kubernetes y se puede leer en un{os} 1 minuto{s}
-   [Best Remote Development Setup with Spacemacs](https://www.reddit.com/r/emacs/comments/7zozh9/best_remote_development_setup_with_spacemacs/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [Why I still use Emacs](https://coffeeandcode.neocities.org/why-i-still-use-emacs.html): tiene que ver con emacs y se puede leer en un{os} 6 minuto{s}
-   [To Luke Smith: Why bother with Emacs? Part 1](http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=EsAkPl3On3E&format=xml): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Why Emacs is Awesome](http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=KuWVHcBCB78&format=xml): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Options for Structured Data in Emacs Lisp « null program](http://nullprogram.com/blog/2018/02/14/): tiene que ver con emacs y se puede leer en un{os} 8 minuto{s}
-   [Kubernetes Monitoring: Best Practices, Methods, and Existing Solutions - DZone Performance](https://dzone.com/articles/kubernetes-monitoring-best-practices-methods-and-e): tiene que ver con kubernetes y se puede leer en un{os} 11 minuto{s}
-   [DevOps Foundations: Lean and Agile](https://theagileadmin.com/2017/10/09/devops-foundations-lean-and-agile/): tiene que ver con devops y se puede leer en un{os} 2 minuto{s}
-   [DevSecOps: How Security Teams Can Better Support Their Developer Counterparts - DevOps.com](https://devops.com/how-security-teams-can-better-support-their-developer-counterparts/): tiene que ver con devops, seguridada y se puede leer en un{os} 2 minuto{s}
-   [20 DevSecOps Reference Architectures to Help](https://dzone.com/articles/20-devsecops-reference-architectures-to-help): tiene que ver con devops y se puede leer en un{os} 2 minuto{s}
-   [Women in DevOps: Meera Subbarao](https://dzone.com/articles/women-in-devops-meera-subbarao): tiene que ver con devops y se puede leer en un{os} 6 minuto{s}
-   [Cloud Comic Serverless and Everything Else](https://dzone.com/articles/cloud-comic-serverless-and-everything-else): tiene que ver con nubecitas y se puede leer en un{os} 0 minuto{s}
-   [Helm Chart Patterns I - Vic Iglesias, Google](http://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=WugC_mbbiWU&format=xml): tiene que ver con kubernetes, video, helm y se puede leer en un{os} 0 minuto{s}
-   [Docker Compose and Kubernetes with Docker for Desktop - Docker Blog](https://blog.docker.com/2018/02/docker-compose-kubernetes-docker-desktop/): tiene que ver con docker, kubernetes y se puede leer en un{os} 4 minuto{s}
-   [Why We Moved From NoSQL MongoDB to PostgreSQL - DZone Database](https://dzone.com/articles/why-we-moved-from-nosql-mongodb-to-postgresql): tiene que ver con bbdd, mongo y se puede leer en un{os} 5 minuto{s}
-   [MongoDB's Drive to Multi-Document Transactions - DZone Database](https://dzone.com/articles/mongodbs-drive-to-multi-document-transactions): tiene que ver con bbdd, mongo y se puede leer en un{os} 6 minuto{s}
-   [Reproducible Builds in Java - DZone Java](https://dzone.com/articles/reproducible-builds-in-java): tiene que ver con java y se puede leer en un{os} 5 minuto{s}
-   [Docker 101](https://theagileadmin.com/2017/10/14/docker-101/): tiene que ver con docker y se puede leer en un{os} 1 minuto{s}
-   [Docker Blog - News and versions will be announced here](https://blog.docker.com/): tiene que ver con docker y se puede leer en un{os} 0 minuto{s}
-   [¿Se puede ser feminista y pro-sex?](https://www.pikaramagazine.com/2018/02/se-puede-ser-feminista-y-pro-sex/): tiene que ver con feminismo y se puede leer en un{os} 7 minuto{s}
-   [¿Galantería? No, gracias](https://www.pikaramagazine.com/2018/02/galanteria-no-gracias/): tiene que ver con feminismo y se puede leer en un{os} 3 minuto{s}
-   [Share your Org Capture Templates!](https://www.reddit.com/r/emacs/comments/7zqc7b/share_your_org_capture_templates/): tiene que ver con emacs y se puede leer en un{os} 4 minuto{s}
-   [What kind of scripts do y’all have to use org mode for literate programming?](https://www.reddit.com/r/emacs/comments/7zbwqh/what_kind_of_scripts_do_yall_have_to_use_org_mode/): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [Martxoak 8, nosotras paramos](https://www.pikaramagazine.com/2018/02/nosotras-paramos-halabedi/): tiene que ver con nada en general y se puede leer en un{os} 1 minuto{s}
-   [Conectar Linux con AWS](https://tangelov.me/posts/conectar-linux-con-aws.html): tiene que ver con aws y se puede leer en un{os} 2 minuto{s}
-   [Vuela Ekai](https://www.pikaramagazine.com/2018/02/vuela-ekai/): tiene que ver con cuidados y se puede leer en un{os} 4 minuto{s}
-   [Evil-Mode Integration](https://www.reddit.com/r/emacs/comments/7yqfvx/evilmode_integration/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [Occupational burnout](https://en.wikipedia.org/wiki/Occupational_burnout): tiene que ver con cuidados y se puede leer en un{os} 16 minuto{s}
-   [Common Lisp Runtime Redefinition](http://tiborsimko.org/common-lisp-runtime-redefinition.html): tiene que ver con emacs y se puede leer en un{os} 6 minuto{s}
-   [Lisp Runtime Redefinition](http://irreal.org/blog/?p=6483): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [Helm and Charts: 2017 In Review](https://codeengineered.com/blog/2018/helm-review-2017/): tiene que ver con kubernetes y se puede leer en un{os} 6 minuto{s}
-   [bitnami-labs/sealed-secrets](https://github.com/bitnami-labs/sealed-secrets): tiene que ver con kubernetes y se puede leer en un{os} 7 minuto{s}
-   [Monitoring and Observability](https://theagileadmin.com/2018/02/16/monitoring-and-observability/): tiene que ver con monitorizacion y se puede leer en un{os} 3 minuto{s}
-   [Exterminate Magit buffers](http://manuel-uberti.github.io/emacs/2018/02/17/magit-bury-buffer/): tiene que ver con emacs y se puede leer en un{os} 2 minuto{s}
-   [Zamansky 42: Git Gutter and Git Time Machine](http://irreal.org/blog/?p=6968): tiene que ver con git, emacs y se puede leer en un{os} 1 minuto{s}
