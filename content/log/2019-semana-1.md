+++
title = "Semana 1 del año 2019"
author = ["drymer"]
publishDate = 2019-01-12T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Getting Towards Real Sandbox Containers](https://blog.jessfraz.com/post/getting-towards-real-sandbox-containers/): tiene que ver con contenedores y se puede leer un{os} 4 minuto{s}
-   [Site Reliability Engineering Principles](https://medium.com/@alexbmeng/site-reliability-engineering-principals-fd52229bfcd6): tiene que ver con devops y se puede leer un{os} 6 minuto{s}
-   [Please do not attempt to simplify this code](https://news.ycombinator.com/item?id=18772873): tiene que ver con programacion y se puede leer un{os} 5 minuto{s}
-   [Autogestión sanitaria de los ‘street medic’ ante la violencia policial en Francia](https://www.elsaltodiario.com/chalecos-amarillos/autogestion-sanitaria-street-medic-violencia-policial-francia-chalecos.amarillos): tiene que ver con libertad y se puede leer un{os} 9 minuto{s}
-   [Ofendiditas](https://www.eldiario.es/barbijaputa/barbijaputa-ofendiditas-campofrio_6_848375178.html): tiene que ver con feminismo y se puede leer un{os} 5 minuto{s}
-   [CDNs aren't just for caching](https://jvns.ca/blog/2016/04/29/cdns-arent-just-for-caching/): tiene que ver con sysadmin y se puede leer un{os} 7 minuto{s}
-   [An Anarchist’s Perspective of the #YellowVests](https://enoughisenough14.org/2018/12/14/an-anarchists-perspective-of-the-yellowvests/): tiene que ver con libertad, anarquismo y se puede leer un{os} 8 minuto{s}
-   [Los chalecos amarillos no se tiñen de morado](http://www.pikaramagazine.com/2018/12/los-chalecos-amarillos-no-se-tinen-de-morado/): tiene que ver con feminismo y se puede leer un{os} 8 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Using S3 As a Helm Repository](https://hackernoon.com/using-s3-as-a-helm-repository-a76b504d494e?source=rss----3a8144eabfe3---4&gi=4d26a2b55ed): tiene que ver con kubernetes y se puede leer en un{os} 3 minuto{s}
-   [Dos 'hackers' burlan el sistema biométrico de reconocimiento de venas con una mano hecha de cera](https://www.20minutos.es/noticia/3527730/0/hackeo-reconocimiento-biometrico-venas-mano/): tiene que ver con seguridad y se puede leer en un{os} 1 minuto{s}
-   [How to Bullet Journal](http://www.youtube.com/oembed?format=xml&url=https://www.youtube.com/watch?v=fm15cmYU0IM): tiene que ver con productividad y se puede leer en un{os} 0 minuto{s}
-   [One notebook could replace all the productivity apps that have failed you](https://www.popsci.com/bullet-journal-guide): tiene que ver con productividad y se puede leer en un{os} 0 minuto{s}
-   [¿Tan difícil es protegernos -a las mujeres- sin socavar principios generales del derecho?](https://confilegal.com/20181227-tan-dificil-es-protegernos-a-las-mujeres-sin-socavar-principios-generales-del-derecho/): tiene que ver con feminismo? y se puede leer en un{os} 5 minuto{s}
-   [Violencia de Género (I): Introducción](https://www.meneame.net/story/violencia-genero-i-introduccion): tiene que ver con feminismo y se puede leer en un{os} 9 minuto{s}
-   [dbcli/mycli](https://github.com/dbcli/mycli): tiene que ver con cli, mysql y se puede leer en un{os} 4 minuto{s}
-   [oflatt/esonify](https://github.com/oflatt/esonify): tiene que ver con emacs y se puede leer en un{os} 0 minuto{s}
-   [Gwendolyn’s birthday and some thoughts about learning](https://revanthrameshkumar.github.io/gwurdblog/thoughts/about/learning/2018/12/21/first_post_thoughts_about_learning.html): tiene que ver con aprender y se puede leer en un{os} 6 minuto{s}
