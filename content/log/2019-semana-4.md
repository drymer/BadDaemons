+++
title = "Semana 4 del año 2019"
author = ["drymer"]
publishDate = 2019-01-26T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [ActivityPub - Final thoughts, one year later](https://schub.io/blog/2019/01/13/activitypub-final-thoughts-one-year-later.html): tiene que ver con fediverso y se puede leer un{os} 21 minuto{s}
-   [Why I track time](https://github.com/Elergy/notes/blob/master/articles/en/time-tracking.md): tiene que ver con productividad y se puede leer un{os} 5 minuto{s}
-   [El 90% de los que nacen pobres mueren pobres, el 90% de los que nacen ricos mueren ricos"](http://www.gamba.cl/2018/03/premio-nobel-de-economia-el-90-de-los-que-nacen-pobres-mueren-pobres-por-mas-esfuerzo-que-hagan-el-90-de-los-que-nacen-ricos-mueren-ricos-independientemente-de-que-hagan-o-no-merito-para-ello/): tiene que ver con la vida y se puede leer un{os} 8 minuto{s}
-   [Datos: qué ayudas reciben los inmigrantes en España](https://newtral.es/actualidad/datos-que-ayudas-reciben-los-inmigrantes-en-espana/): tiene que ver con racismo y se puede leer un{os} 6 minuto{s}
-   [Parentheses, Elisp, and Other Things That Aren’t Going Away](https://irreal.org/blog/?p=7772): tiene que ver con emacs y se puede leer un{os} 1 minuto{s}
-   [OWASP ServerlessGoat: Learn Serverless Security By Hacking and Defending - DZone Cloud](https://dzone.com/articles/owasp-serverlessgoat-learn-serverless-security-by): tiene que ver con nubecitas, seguridad y se puede leer un{os} 2 minuto{s}
