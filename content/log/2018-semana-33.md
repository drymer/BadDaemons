+++
title = "Semana 33 del año 2018"
author = ["drymer"]
publishDate = 2018-08-19T00:00:00+02:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Behavior-driven development](https://en.wikipedia.org/wiki/Behavior-driven_development): tiene que ver con bdd, wikipedia y se puede leer en un{os} 18 minuto{s}
-   [microctfs: pequeños retos en contenedores docker para practicar](https://www.hackplayers.com/2018/08/microctfs-pequenos-retos-en-docker.html): tiene que ver con docker, ctf y se puede leer en un{os} 1 minuto{s}
-   [CLI Love Inside](https://dev.to/mamyn0va/cli-love-inside-4lgl): tiene que ver con cli y se puede leer en un{os} 6 minuto{s}
-   [VerbalExpressions - RegularExpression made easy](https://dev.to/bachnxhedspi/verbalexpressions---regularexpression-made-easy-27a8): tiene que ver con regex y se puede leer en un{os} 3 minuto{s}
-   [Kubernetes Networking: How to Write Your Own CNI Plug-in with Bash | Altoros](https://www.altoros.com/blog/kubernetes-networking-writing-your-own-simple-cni-plug-in-with-bash/): tiene que ver con kubernetes, redes y se puede leer en un{os} 0 minuto{s}
-   [Decorators in Python: What you need to know  Hacker Noon](https://hackernoon.com/decorators-in-python-what-you-need-to-know-72c81cc65c94?source=rss----3a8144eabfe3---4&gi=e558dfd04672): tiene que ver con python y se puede leer en un{os} 8 minuto{s}
-   [Matar a la Bruja Avería. Así rompió el PSOE 'La bola de cristal'](https://www.elconfidencial.com/cultura/2018-08-12/bola-cristal-rtve-psoe-felipe-gonzalez-pilar-miro_1603243/): tiene que ver con nada en general y se puede leer en un{os} 14 minuto{s}
-   [Adopting org-gcal.el](http://puntoblogspot.blogspot.com/2018/08/adopting-org-gcalel.html?m=1): tiene que ver con org-mode y se puede leer en un{os} 0 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Two Years With Emacs as a CEO (and now CTO)](https://blog.fugue.co/2018-08-09-two-years-with-emacs-as-a-cto.html): tiene que ver con emacs y se puede leer en un{os} 11 minuto{s}
-   [Sprint Planning as Self Care](https://dev.to/kathryngrayson/sprint-planning-as-self-care-j59): tiene que ver con cuidados y se puede leer en un{os} 3 minuto{s}
-   [Managing Helm releases the GitOps way](https://www.weave.works/blog/managing-helm-releases-the-gitops-way): tiene que ver con git, kubernetes, helm, weave y se puede leer en un{os} 13 minuto{s}
-   [calebmadrigal/trackerjacker](https://github.com/calebmadrigal/trackerjacker): tiene que ver con redes y se puede leer en un{os} 8 minuto{s}
