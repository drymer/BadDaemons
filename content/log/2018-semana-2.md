+++
title = "Semana 2 del año 2018"
author = ["drymer"]
publishDate = 2018-01-14T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Kubernetes: Twelve Key Features](https://dzone.com/articles/kubernetes-twelve-key-features): tiene que ver con kubernetes y se puede leer en un{os} 9 minuto{s}
-   [Detección de micrófonos ocultos con Salamandra](http://www.hackplayers.com/2018/01/deteccion-de-microfonos-ocultos-con.html): tiene que ver con seguridad, privacidad y se puede leer en un{os} 2 minuto{s}
-   [El agua que buscábamos: el suelo de Marte oculta columnas de hielo de hasta 170 metros](https://www.elconfidencial.com/tecnologia/ciencia/2018-01-11/hielo-marte-descubrimiento-secretos-geologia_1504594/): tiene que ver con nada en general y se puede leer en un{os} 2 minuto{s}
-   [Susan Landau's New Book: Listening In](https://www.schneier.com/blog/archives/2018/01/susan_landaus_n.html): tiene que ver con seguridad y se puede leer en un{os} 0 minuto{s}
-   [Developer Tip: Keep Your Commits "Atomic"](https://www.freshconsulting.com/atomic-commits/): tiene que ver con git y se puede leer en un{os} 3 minuto{s}
-   [How to write a good commit message](http://blog.indrek.io/articles/how-to-write-a-good-commit-message/): tiene que ver con git y se puede leer en un{os} 9 minuto{s}
-   [Recovering the productivity stolen by depression with kanban and emoji](http://jpetazzo.github.io/2017/12/24/productivity-depression-kanban-emoji/): tiene que ver con cuidados y se puede leer en un{os} 16 minuto{s}
-   [Too Big To Fail: Lessons Learnt from Google and HealthCare.gov](https://www.infoq.com/news/2015/06/too-big-to-fail): tiene que ver con chaos y se puede leer en un{os} 4 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Risk Analytics in a Microservices Architecture: Part 1](https://dzone.com/articles/risk-analytics-in-a-microservices-architecture-par): tiene que ver con microservicios y se puede leer en un{os} 2 minuto{s}
-   [Top 5 Recommendations for a Successful DevOps Initiative](https://dzone.com/articles/top-5-recommendations-for-successful-devops-initia): tiene que ver con devops y se puede leer en un{os} 3 minuto{s}
-   [GitHub Repo Sharing: kubernetes-scripts](https://dzone.com/articles/github-repo-sharing-kubernetes-scripts): tiene que ver con git, kubernetes, devops y se puede leer en un{os} 0 minuto{s}
-   [Speed Up Ansible](https://dzone.com/articles/speed-up-ansible): tiene que ver con ansible y se puede leer en un{os} 8 minuto{s}
-   [python2t3 slides](https://avara1986.github.io/python2to3/): tiene que ver con python y se puede leer en un{os} 8 minuto{s}
-   [deploying kubernetes applications with helm](https://www.reddit.com/r/devops/comments/7pzr9n/deploying_kubernetes_applications_with_helm/): tiene que ver con kubernetes y se puede leer en un{os} 1 minuto{s}
-   [Yet Another Post About Java Heap Space > performant code\_](http://performantcode.com/gc/gc-explained-heap/): tiene que ver con java y se puede leer en un{os} 2 minuto{s}
-   [Emacs org-mode examples and cookbook](http://ehneilsen.net/notebook/orgExamples/org-examples.html): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [La clase obrera ha abandonado a la izquierda](http://lareplica.es/la-clase-obrera-ha-abandonado-la-izquierda/): tiene que ver con nada en general y se puede leer en un{os} 5 minuto{s}
-   [How to deal with Confluence?](https://www.reddit.com/r/emacs/comments/7pytxd/how_to_deal_with_confluence/): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}
-   [Un artículo crítico de un profesor del IESE desata la cólera del Banco de España](https://www.lainformacion.com/economia-negocios-y-finanzas/el-informe-sobre-la-crisis-financiera-que-enfurecio-a-la-cupula-del-banco-de-espana/6339975): tiene que ver con nada en general y se puede leer en un{os} 6 minuto{s}
-   [On commit messages](https://who-t.blogspot.com.es/2009/12/on-commit-messages.html): tiene que ver con nada en general y se puede leer en un{os} 7 minuto{s}
-   [GC Algorithms > performant code\_](http://performantcode.com/gc/gc-explained-algorithms/): tiene que ver con java y se puede leer en un{os} 2 minuto{s}
-   [When Throughput Matters  Parallel GC > performant code\_](http://performantcode.com/gc/gc-explained-parallel-collector/): tiene que ver con java y se puede leer en un{os} 4 minuto{s}
-   [Garbage Collectors Overview > performant code](http://performantcode.com/gc/gc-explained-collectors-overview/): tiene que ver con java y se puede leer en un{os} 4 minuto{s}
