+++
title = "Semana 2 del año 2019"
author = ["drymer"]
publishDate = 2019-01-13T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}
-   [Creating containers - Part 1](http://crosbymichael.com/creating-containers-part-1.html): tiene que ver con contenedores y se puede leer un{os} 20 minuto{s}
-   [Monorepos: Please don’t!  Matt Klein  Medium](https://medium.com/@mattklein123/monorepos-please-dont-e9a279be011b): tiene que ver con git, metodologia y se puede leer un{os} 11 minuto{s}
-   [Monorepo: please do!  Adam Jacob  Medium](https://medium.com/@adamhjk/monorepo-please-do-3657e08a4b70): tiene que ver con git, metodologia y se puede leer en un{os} 8 minuto{s}
-   [Follow Up: Container Scanning Comparison](https://kubedex.com/follow-up-container-scanning-comparison/): tiene que ver con seguridad y se puede leer un{os} 13 minuto{s}
-   [Relación de sitios web para descargar libros gratis de forma legal (Actualizado)](https://www.internautas.org/html/7180.html): tiene que ver con conocimiento libre y se puede leer un{os} 4 minuto{s}
-   [Keyless SSL: The Nitty Gritty Technical Details](https://blog.cloudflare.com/keyless-ssl-the-nitty-gritty-technical-details/): tiene que ver con sysadmin, ssl y se puede leer un{os} 21 minuto{s}


## Articulos leídos {#articulos-leídos}
-   [The Emacs Year in Review · Emacs Redux](https://emacsredux.com/blog/2019/01/10/the-emacs-year-in-review/): tiene que ver con emacs, productividad y se puede leer en un{os} 7 minuto{s}
-   [Marie Kondo's Advice Praised by Scientists: "Clutter Is Not a Good Thing"](https://www.inverse.com/article/52319-marie-kondo-tidying-up-clutter): tiene que ver con productividad y se puede leer en un{os} 3 minuto{s}
-   [Container Scanning](https://kubedex.com/container-scanning/): tiene que ver con seguridad y se puede leer en un{os} 8 minuto{s}
-   [User namespaces have arrived in Docker!](https://integratedcode.us/2015/10/13/user-namespaces-have-arrived-in-docker/): tiene que ver con docker y se puede leer en un{os} 5 minuto{s}
-   [2018-12-11: It's Magit! - John Wiegley](http://www.youtube.com/oembed?format=xml&url=https://www.youtube.com/watch?v=j-k-lkilbEs): tiene que ver con git y se puede leer en un{os} 0 minuto{s}
