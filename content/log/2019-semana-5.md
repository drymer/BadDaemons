+++
title = "Semana 5 del año 2019"
author = ["drymer"]
publishDate = 2019-02-02T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [CSI, Casi Siempre Inocentes (El efecto CSI)](https://naukas.com/2019/01/28/csi-casi-siempre-inocentes-el-efecto-csi/): tiene que ver con nada en general y se puede leer un{os} 16 minuto{s}
-   [“La nueva guía de alimentación de Canadá”](https://www.instagram.com/p/BtBSTHMFxch/&igshid=1undpqes1ssej): tiene que ver con alimentación y se puede leer un{os} 0 minuto{s}
-   [r/emacs - Is there a detailed "under the hood" writeup of emacs?](https://www.reddit.com/r/emacs/comments/ajeolq/is_there_a_detailed_under_the_hood_writeup_of/): tiene que ver con emacs y se puede leer un{os} 0 minuto{s}
-   ["A los hombres con discapacidad intelectual no se les esteriliza, sino que se les lleva de putas"](https://www.eldiario.es/cultura/libros/Morales-discapacidad-intelectual-esteriliza-putas_0_860564361.html): tiene que ver con capacitismo y se puede leer un{os} 10 minuto{s}
-   [Feedback and Control - an Essential GitOps Component](https://www.weave.works/blog/feedback-and-control-an-essential-gitops-component): tiene que ver con git, kubernetes, observabilidad, gitops y se puede leer un{os} 4 minuto{s}
-   [An ActivityPub Philosophy](https://cjslep.com/c/blog/an-activitypub-philosophy): tiene que ver con fediverso y se puede leer un{os} 8 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [Git the Princess!](https://toggl.com/programming-princess/): tiene que ver con git y se puede leer en un{os} 1 minuto{s}
-   [AlphaStar: Mastering the Real-Time Strategy Game StarCraft II ](https://deepmind.com/blog/alphastar-mastering-real-time-strategy-game-starcraft-ii/): tiene que ver con IA y se puede leer en un{os} 2 minuto{s}
-   [A small notebook for a system administrator](https://m.habr.com/en/post/437912/): tiene que ver con sysadmin y se puede leer en un{os} 31 minuto{s}
-   [Facebook pays teens to install VPN that spies on them](https://techcrunch.com/2019/01/29/facebook-project-atlas/): tiene que ver con privacidad y se puede leer en un{os} 12 minuto{s}
-   [Formatting JSON](http://puntoblogspot.blogspot.com/2019/01/formatting-json.html?m=1): tiene que ver con emacs y se puede leer en un{os} 1 minuto{s}
-   [r/orgmode - org-grasp: browser extension for org-capture](https://www.reddit.com/r/orgmode/comments/akazos/orggrasp_browser_extension_for_orgcapture/): tiene que ver con org-mode y se puede leer en un{os} 1 minuto{s}
-   [Can You Trust Your C Compiler?](https://hackaday.com/2019/01/24/can-you-trust-your-c-compiler/): tiene que ver con programacion y se puede leer en un{os} 2 minuto{s}
-   [Why I use old hardware](https://drewdevault.com/2019/01/23/Why-I-use-old-hardware.html): tiene que ver con programacion y se puede leer en un{os} 2 minuto{s}
-   [The Evolution of Darknets](https://www.schneier.com/blog/archives/2019/01/the_evolution_o.html): tiene que ver con darknets y se puede leer en un{os} 4 minuto{s}
