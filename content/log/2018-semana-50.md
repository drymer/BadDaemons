+++
title = "Semana 50 del año 2018"
author = ["drymer"]
publishDate = 2018-12-16T00:00:00+01:00
draft = false
+++

## Articulos leídos {#articulos-leídos}

-   [What we learned from 3 years of bra engineering, and what's next](https://bratheory.com/what-we-learned-and-whats-next/): tiene que ver con nada en general y se puede leer en un{os} 0 minuto{s}
-   [Next](https://next.atlas.engineer/): tiene que ver con navegadores y se puede leer en un{os} 0 minuto{s}
-   [nyyManni/ejira](https://github.com/nyyManni/ejira/blob/master/README.org): tiene que ver con agilismo, emacs y se puede leer en un{os} 5 minuto{s}
