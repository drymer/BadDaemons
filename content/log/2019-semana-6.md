+++
title = "Semana 6 del año 2019"
author = ["drymer"]
publishDate = 2019-02-09T00:00:00+01:00
draft = false
+++

## Articulos favoritos {#articulos-favoritos}

-   [Sysdig | Enable Kubernetes Pod Security Policy with kube-psp-advisor](https://sysdig.com/blog/enable-kubernetes-pod-security-policy/): tiene que ver con kubernetes y se puede leer un{os} 7 minuto{s}
-   [git reset vs git revert](https://www.pixelstech.net/article/1549115148-git-reset-vs-git-revert): tiene que ver con git y se puede leer un{os} 2 minuto{s}
-   [¿Quién quiere romper el movimiento feminista?](https://ctxt.es/es/20190206/Firmas/24296/Nuria-Alabao-Maria-Perez-Colina-conflicto-movimiento-feminista-abolicionistas-PSOE.htm?fbclid=IwAR1OyoegYp0JNfakv8_gIY9E6B5e1mGVdjb3LvMXo6sQr3pKjx6szvIygbQ): tiene que ver con feminismo y se puede leer en un{os} 9 minuto{s}


## Articulos leídos {#articulos-leídos}

-   [BASH tricks: The dir stack.](https://dev.to/cpu/bash-tricks-the-dir-stack-4mei): tiene que ver con bash y se puede leer en un{os} 5 minuto{s}
-   [Getting Boxes Done](http://howardism.org/Technical/Emacs/getting-boxes-done.html): tiene que ver con org-mode y se puede leer en un{os} 1 minuto{s}
-   [Getting Boxes Done, the Code](http://www.howardism.org/Technical/Emacs/getting-more-boxes-done.html): tiene que ver con org-mode y se puede leer en un{os} 4 minuto{s}
-   [A Perfect DNS Storm](https://www.adammargherio.com/a-perfect-dns-storm/): tiene que ver con postmortem, kubernetes y se puede leer en un{os} 5 minuto{s}
-   [Outages? Downtime?](https://sethmccombs.github.io/work/2018/12/03/Outages.html): tiene que ver con postmortem, kubernetes y se puede leer en un{os} 11 minuto{s}
-   [Using ed](https://irreal.org/blog/?p=7805): tiene que ver con sysadmin y se puede leer en un{os} 2 minuto{s}
-   [Why I chose Flask to build sr.ht’s mini-services](https://drewdevault.com/2019/01/30/Why-I-built-sr.ht-with-Flask.html): tiene que ver con programacion y se puede leer en un{os} 3 minuto{s}
-   [The Feynman Technique  The Habitual Programmer  Medium](https://medium.com/the-habitual-programmer/the-feynman-technique-a65666a5d980): tiene que ver con aprender y se puede leer en un{os} 4 minuto{s}
-   [A few early marketing thoughts](https://jvns.ca/blog/2019/01/29/marketing-thoughts/): tiene que ver con nada en general y se puede leer en un{os} 8 minuto{s}
-   [Systemd as tragedy](https://lwn.net/SubscriberLink/777595/a71362cc65b1c271/): tiene que ver con sysadmin y se puede leer en un{os} 9 minuto{s}
-   [Our First Kubernetes Outage  Saltside Engineering](https://engineering.saltside.se/our-first-kubernetes-outage-c6b9249cfd3a?gi=e2205218b8ff): tiene que ver con kubernetes, postmortem y se puede leer en un{os} 6 minuto{s}
-   [AirMap Platform Kubernetes Incident Report](https://www.airmap.com/incident-180719/): tiene que ver con kubernetes, postmortem y se puede leer en un{os} 2 minuto{s}
-   [Maximize learnings from a Kubernetes cluster failure](https://dev.to/tbeijen/maximize-learnings-from-a-kubernetes-cluster-failure-3p53): tiene que ver con kubernetes, postmortem y se puede leer en un{os} 10 minuto{s}
-   [NRE Labs Outage Post-Mortem · Keeping It Classless](https://keepingitclassless.net/2018/12/december-4---nre-labs-outage-post-mortem/): tiene que ver con kubernetes, postmortem y se puede leer en un{os} 10 minuto{s}
-   [Backups & redundancy at sr.ht](https://drewdevault.com/2019/01/13/Backups-and-redundancy-at-sr.ht.html): tiene que ver con sysadmin y se puede leer en un{os} 5 minuto{s}
-   [Kubernetes Load Balancer Configuration](https://www.devops-hof.de/kubernetes-load-balancer-konfiguration-beware-when-draining-nodes/): tiene que ver con kubernetes, postmortem y se puede leer en un{os} 2 minuto{s}
-   [A GitOps RDS Migration Story](https://www.weave.works/blog/gitops-rds-migration-story): tiene que ver con gitops y se puede leer en un{os} 4 minuto{s}
-   [Scanning Docker images with CoreOS Clair](https://werner-dijkerman.nl/2019/01/28/scanning-docker-images-with-coreos-clair/): tiene que ver con docker, seguridad y se puede leer en un{os} 9 minuto{s}
